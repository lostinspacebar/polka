<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="6.6.0">
<drawing>
<settings>
<setting alwaysvectorfont="yes"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="1" fill="3" visible="no" active="no"/>
<layer number="3" name="Route3" color="4" fill="3" visible="no" active="no"/>
<layer number="14" name="Route14" color="1" fill="6" visible="no" active="no"/>
<layer number="15" name="Route15" color="4" fill="6" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="9" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="1" fill="9" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="57" name="tCAD" color="7" fill="1" visible="no" active="no"/>
<layer number="59" name="tCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="60" name="bCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="90" name="Modules" color="5" fill="1" visible="no" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="7" fill="1" visible="yes" active="yes"/>
<layer number="100" name="Muster" color="7" fill="1" visible="no" active="no"/>
<layer number="101" name="Patch_Top" color="12" fill="4" visible="yes" active="yes"/>
<layer number="102" name="Vscore" color="7" fill="1" visible="yes" active="yes"/>
<layer number="103" name="tMap" color="7" fill="1" visible="yes" active="yes"/>
<layer number="104" name="Name" color="16" fill="1" visible="yes" active="yes"/>
<layer number="105" name="tPlate" color="7" fill="1" visible="yes" active="yes"/>
<layer number="106" name="bPlate" color="7" fill="1" visible="yes" active="yes"/>
<layer number="107" name="Crop" color="7" fill="1" visible="yes" active="yes"/>
<layer number="108" name="tplace-old" color="10" fill="1" visible="yes" active="yes"/>
<layer number="109" name="ref-old" color="11" fill="1" visible="yes" active="yes"/>
<layer number="110" name="fp0" color="7" fill="1" visible="yes" active="yes"/>
<layer number="111" name="LPC17xx" color="7" fill="1" visible="yes" active="yes"/>
<layer number="112" name="tSilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="113" name="IDFDebug" color="7" fill="1" visible="no" active="yes"/>
<layer number="116" name="Patch_BOT" color="9" fill="4" visible="yes" active="yes"/>
<layer number="118" name="Rect_Pads" color="7" fill="1" visible="no" active="yes"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="126" name="_bNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="127" name="_tValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="128" name="_bValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="129" name="Mask" color="7" fill="1" visible="yes" active="yes"/>
<layer number="131" name="tAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="132" name="bAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="144" name="Drill_legend" color="7" fill="1" visible="yes" active="yes"/>
<layer number="150" name="Notes" color="7" fill="1" visible="yes" active="yes"/>
<layer number="151" name="HeatSink" color="7" fill="1" visible="yes" active="yes"/>
<layer number="152" name="_bDocu" color="7" fill="1" visible="yes" active="yes"/>
<layer number="153" name="FabDoc1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="154" name="FabDoc2" color="7" fill="1" visible="yes" active="yes"/>
<layer number="155" name="FabDoc3" color="7" fill="1" visible="yes" active="yes"/>
<layer number="199" name="Contour" color="7" fill="1" visible="yes" active="yes"/>
<layer number="200" name="200bmp" color="1" fill="10" visible="yes" active="yes"/>
<layer number="201" name="201bmp" color="2" fill="10" visible="yes" active="yes"/>
<layer number="202" name="202bmp" color="3" fill="10" visible="yes" active="yes"/>
<layer number="203" name="203bmp" color="4" fill="10" visible="yes" active="yes"/>
<layer number="204" name="204bmp" color="5" fill="10" visible="yes" active="yes"/>
<layer number="205" name="205bmp" color="6" fill="10" visible="yes" active="yes"/>
<layer number="206" name="206bmp" color="7" fill="10" visible="yes" active="yes"/>
<layer number="207" name="207bmp" color="8" fill="10" visible="yes" active="yes"/>
<layer number="208" name="208bmp" color="9" fill="10" visible="yes" active="yes"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="211" name="211bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="217" name="217bmp" color="18" fill="1" visible="no" active="no"/>
<layer number="218" name="218bmp" color="19" fill="1" visible="no" active="no"/>
<layer number="219" name="219bmp" color="20" fill="1" visible="no" active="no"/>
<layer number="220" name="220bmp" color="21" fill="1" visible="no" active="no"/>
<layer number="221" name="221bmp" color="22" fill="1" visible="no" active="no"/>
<layer number="222" name="222bmp" color="23" fill="1" visible="no" active="no"/>
<layer number="223" name="223bmp" color="24" fill="1" visible="no" active="no"/>
<layer number="224" name="224bmp" color="25" fill="1" visible="no" active="no"/>
<layer number="225" name="225bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="226" name="226bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="227" name="227bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="228" name="228bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="229" name="229bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="230" name="230bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="231" name="Eagle3D_PG1" color="7" fill="1" visible="no" active="yes"/>
<layer number="232" name="Eagle3D_PG2" color="7" fill="1" visible="no" active="yes"/>
<layer number="233" name="Eagle3D_PG3" color="7" fill="1" visible="no" active="yes"/>
<layer number="247" name="wrappinf" color="7" fill="1" visible="no" active="yes"/>
<layer number="248" name="Housing" color="7" fill="1" visible="yes" active="yes"/>
<layer number="249" name="Edge" color="7" fill="1" visible="yes" active="yes"/>
<layer number="250" name="Descript" color="3" fill="1" visible="no" active="no"/>
<layer number="251" name="SMDround" color="12" fill="11" visible="no" active="no"/>
<layer number="252" name="BR-BS" color="7" fill="1" visible="no" active="yes"/>
<layer number="253" name="BR-LS" color="7" fill="1" visible="no" active="yes"/>
<layer number="254" name="cooling" color="7" fill="1" visible="yes" active="yes"/>
<layer number="255" name="ZchnBlatt" color="7" fill="1" visible="no" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="aditya_main">
<description>Common anode RGB matrix</description>
<packages>
<package name="LED_WS2812B">
<wire x1="2.5" y1="2.5" x2="-2.5" y2="2.5" width="0.127" layer="21"/>
<wire x1="-2.5" y1="2.5" x2="-2.5" y2="-2.5" width="0.127" layer="21"/>
<wire x1="-2.5" y1="-2.5" x2="2.5" y2="-2.5" width="0.127" layer="21"/>
<wire x1="2.5" y1="-2.5" x2="2.5" y2="2.5" width="0.127" layer="21"/>
<smd name="P3" x="2.25" y="-1.65" dx="1" dy="1" layer="1" roundness="20"/>
<smd name="P2" x="-2.25" y="-1.65" dx="1" dy="1" layer="1" roundness="20"/>
<smd name="P1" x="-2.25" y="1.65" dx="1" dy="1" layer="1" roundness="20"/>
<smd name="P4" x="2.25" y="1.65" dx="1" dy="1" layer="1" roundness="20"/>
<text x="-2" y="-0.5" size="0.9" layer="21" ratio="15">&gt;NAME</text>
</package>
<package name="4POS_254MM_1R_THRU">
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="-1.27" width="0.127" layer="21"/>
<wire x1="-1.27" y1="-1.27" x2="8.89" y2="-1.27" width="0.127" layer="21"/>
<wire x1="8.89" y1="-1.27" x2="8.89" y2="1.27" width="0.127" layer="21"/>
<wire x1="8.89" y1="1.27" x2="-1.27" y2="1.27" width="0.127" layer="21"/>
<pad name="P$1" x="0" y="0" drill="1.016" diameter="1.905" shape="square"/>
<pad name="P$2" x="2.54" y="0" drill="1.016" diameter="1.905"/>
<pad name="P$3" x="5.08" y="0" drill="1.016" diameter="1.905"/>
<pad name="P$4" x="7.62" y="0" drill="1.016" diameter="1.905"/>
<text x="-1.27" y="1.27" size="1.27" layer="21">&gt;NAME</text>
</package>
<package name="4POS_127MM_1R_SMD">
<smd name="P1" x="0" y="0" dx="0.75" dy="0.5" layer="1" roundness="20" rot="R90"/>
<smd name="P2" x="1.27" y="0" dx="0.75" dy="0.5" layer="1" roundness="20" rot="R90"/>
<smd name="P3" x="2.54" y="0" dx="0.75" dy="0.5" layer="1" roundness="20" rot="R90"/>
<smd name="P4" x="3.81" y="0" dx="0.75" dy="0.5" layer="1" roundness="20" rot="R90"/>
<wire x1="-0.5" y1="0.6" x2="-0.5" y2="-0.6" width="0.05" layer="49"/>
<wire x1="-0.5" y1="-0.6" x2="4.3" y2="-0.6" width="0.05" layer="49"/>
<wire x1="4.3" y1="-0.6" x2="4.3" y2="0.6" width="0.05" layer="49"/>
<wire x1="4.3" y1="0.6" x2="-0.5" y2="0.6" width="0.05" layer="49"/>
<text x="-0.1" y="-0.3" size="0.5" layer="49">&gt;NAME</text>
</package>
<package name="4POS_250MM_MOLEX53426">
<pad name="P$1" x="0" y="0" drill="1.1" diameter="1.9" shape="square"/>
<pad name="P$2" x="2.5" y="0" drill="1.1" diameter="1.9"/>
<wire x1="-2.45" y1="0" x2="-2.45" y2="-9" width="0.127" layer="21"/>
<wire x1="-2.45" y1="-9" x2="9.95" y2="-9" width="0.127" layer="21"/>
<wire x1="9.95" y1="-9" x2="9.95" y2="0" width="0.127" layer="21"/>
<wire x1="9.95" y1="0" x2="-2.45" y2="0" width="0.127" layer="21"/>
<wire x1="-2.45" y1="0" x2="-2.45" y2="2.5" width="0.127" layer="21"/>
<wire x1="-2.45" y1="2.5" x2="9.95" y2="2.5" width="0.127" layer="21"/>
<wire x1="9.95" y1="2.5" x2="9.95" y2="0" width="0.127" layer="21"/>
<pad name="P$3" x="5" y="0" drill="1.1" diameter="1.9"/>
<pad name="P$4" x="7.5" y="0" drill="1.1" diameter="1.9"/>
<text x="-2.54" y="2.794" size="1.778" layer="21">&gt;NAME</text>
</package>
<package name="4POS_150MM_1R_SMD_WIDE">
<smd name="P$1" x="0" y="0" dx="1" dy="1.25" layer="1" rot="R180"/>
<smd name="P$2" x="1.5" y="0" dx="1.25" dy="1" layer="1" rot="R90"/>
<smd name="P$3" x="3" y="0" dx="1.25" dy="1" layer="1" rot="R90"/>
<smd name="P$4" x="4.5" y="0" dx="1.25" dy="1" layer="1" rot="R90"/>
<wire x1="-0.8" y1="1" x2="-0.8" y2="-1" width="0.127" layer="21"/>
<wire x1="-0.8" y1="-1" x2="5.3" y2="-1" width="0.127" layer="21"/>
<wire x1="5.3" y1="-1" x2="5.3" y2="1" width="0.127" layer="21"/>
<wire x1="5.3" y1="1" x2="-0.8" y2="1" width="0.127" layer="21"/>
<rectangle x1="-1.5" y1="-0.9" x2="-1" y2="0.8" layer="21"/>
</package>
<package name="4POS_0.5MM_FFC_PADS">
<smd name="P3" x="0.25" y="0" dx="2" dy="0.3" layer="1" roundness="20" rot="R90"/>
<smd name="P4" x="0.75" y="0" dx="2" dy="0.3" layer="1" roundness="20" rot="R90"/>
<smd name="P2" x="-0.25" y="0" dx="2" dy="0.3" layer="1" roundness="20" rot="R90"/>
<smd name="P1" x="-0.75" y="0" dx="2" dy="0.3" layer="1" roundness="20" rot="R90"/>
<rectangle x1="-1" y1="1.25" x2="-0.5" y2="1.5" layer="21"/>
<rectangle x1="-1.25" y1="-1" x2="-1" y2="0.25" layer="21"/>
</package>
<package name="4POS_254MM_2R_SMT">
<smd name="P1" x="-1.27" y="-2.7" dx="3.1" dy="1" layer="1" roundness="20" rot="R90"/>
<smd name="P2" x="1.27" y="-2.7" dx="3.1" dy="1" layer="1" roundness="20" rot="R90"/>
<smd name="P3" x="1.27" y="2.7" dx="3.1" dy="1" layer="1" roundness="20" rot="R90"/>
<smd name="P4" x="-1.27" y="2.7" dx="3.1" dy="1" layer="1" roundness="20" rot="R90"/>
<wire x1="-2.54" y1="2.54" x2="-2.54" y2="-2.54" width="0.127" layer="21"/>
<wire x1="-2.54" y1="-2.54" x2="2.54" y2="-2.54" width="0.127" layer="21"/>
<wire x1="2.54" y1="-2.54" x2="2.54" y2="2.54" width="0.127" layer="21"/>
<wire x1="2.54" y1="2.54" x2="-2.54" y2="2.54" width="0.127" layer="21"/>
<text x="-2" y="-0.5" size="0.9" layer="21" ratio="15">&gt;NAME</text>
</package>
<package name="0805">
<smd name="1" x="-0.9" y="0" dx="1.15" dy="1.45" layer="1" roundness="15"/>
<smd name="2" x="0.9" y="0" dx="1.15" dy="1.45" layer="1" roundness="15"/>
<text x="-1.9" y="1.3" size="0.9" layer="21" ratio="15">&gt;NAME</text>
<wire x1="-1.8" y1="1.05" x2="-1.8" y2="-1.05" width="0.2032" layer="21"/>
<wire x1="-1.8" y1="-1.05" x2="1.8" y2="-1.05" width="0.2032" layer="21"/>
<wire x1="1.8" y1="-1.05" x2="1.8" y2="1.05" width="0.2032" layer="21"/>
<wire x1="1.8" y1="1.05" x2="-1.8" y2="1.05" width="0.2032" layer="21"/>
<rectangle x1="-1.8" y1="-1.1" x2="-0.75" y2="1.05" layer="21"/>
</package>
<package name="1206">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<smd name="2" x="1.5" y="0" dx="1.15" dy="1.8" layer="1" roundness="20"/>
<smd name="1" x="-1.5" y="0" dx="1.15" dy="1.8" layer="1" roundness="20"/>
<rectangle x1="-0.3" y1="-0.7" x2="0.3" y2="0.7" layer="35"/>
<wire x1="2.35" y1="1.15" x2="2.35" y2="-1.15" width="0.05" layer="49"/>
<wire x1="2.35" y1="-1.15" x2="-2.35" y2="-1.15" width="0.05" layer="49"/>
<wire x1="-2.35" y1="-1.15" x2="-2.35" y2="1.15" width="0.05" layer="49"/>
<wire x1="-2.35" y1="1.15" x2="2.35" y2="1.15" width="0.05" layer="49"/>
<text x="-1.8" y="-0.25" size="0.5" layer="49" ratio="6">&gt;NAME</text>
</package>
<package name="CAP-PTH-SMALL">
<wire x1="1.27" y1="0.635" x2="1.27" y2="-0.635" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0" drill="0.7" diameter="1.651"/>
<pad name="2" x="2.54" y="0" drill="0.7" diameter="1.651"/>
<text x="-0.508" y="1.27" size="0.762" layer="25">&gt;Name</text>
<text x="-0.508" y="-1.778" size="0.762" layer="27">&gt;Value</text>
</package>
<package name="0201">
<description>&lt;b&gt;RESISTOR&lt;/b&gt; chip&lt;p&gt;
Source: http://www.vishay.com/docs/20008/dcrcw.pdf</description>
<smd name="1" x="-0.33" y="0" dx="0.42" dy="0.46" layer="1" roundness="10"/>
<smd name="2" x="0.33" y="0" dx="0.42" dy="0.46" layer="1" roundness="10"/>
<wire x1="-0.7" y1="0.4" x2="-0.7" y2="-0.4" width="0.05" layer="49"/>
<wire x1="-0.7" y1="-0.4" x2="0.7" y2="-0.4" width="0.05" layer="49"/>
<wire x1="0.7" y1="-0.4" x2="0.7" y2="0.4" width="0.05" layer="49"/>
<wire x1="0.7" y1="0.4" x2="-0.7" y2="0.4" width="0.05" layer="49"/>
<text x="-0.55" y="-0.15" size="0.3" layer="49" font="vector">&gt;NAME</text>
<rectangle x1="-0.2" y1="0.3" x2="0.2" y2="0.4" layer="21"/>
<rectangle x1="-0.2" y1="-0.4" x2="0.2" y2="-0.3" layer="21"/>
</package>
<package name="SUPERCAP_PHV_32X21MM">
<wire x1="-10.5" y1="3" x2="-10.5" y2="35" width="0.127" layer="21"/>
<wire x1="-10.5" y1="35" x2="10.5" y2="35" width="0.127" layer="21"/>
<wire x1="10.5" y1="35" x2="10.5" y2="3" width="0.127" layer="21"/>
<wire x1="10.5" y1="3" x2="-10.5" y2="3" width="0.127" layer="21"/>
<pad name="P1" x="-2.54" y="0" drill="1.143" diameter="2.54" shape="square" thermals="no"/>
<pad name="P2" x="2.54" y="0" drill="1.143" diameter="2.54" thermals="no"/>
<text x="-3.81" y="3.81" size="2.54" layer="21" ratio="15">+</text>
<text x="1.27" y="3.81" size="2.54" layer="21" ratio="15">-</text>
<text x="-8.5" y="5" size="1" layer="21" rot="R90">32x21MM</text>
<text x="-9.5" y="32" size="2.032" layer="21">&gt;NAME</text>
<text x="-9.5" y="29" size="2.032" layer="21">&gt;VALUE</text>
</package>
<package name="SUPERCAP_DCK-3R3E224U-E">
<circle x="0" y="0" radius="3.4" width="0.127" layer="21"/>
<smd name="P2" x="-0.75" y="-4.75" dx="2.5" dy="0.5" layer="1" rot="R90"/>
<smd name="P1" x="0.75" y="-4.75" dx="2.5" dy="0.5" layer="1" rot="R90"/>
<text x="0.3" y="-3.35" size="1.27" layer="21" ratio="15">+</text>
<text x="-1.2" y="-3.35" size="1.27" layer="21" ratio="15">-</text>
</package>
<package name="4POS_0.5MM_FFC_FH33">
<smd name="P1" x="-0.75" y="0.25" dx="1" dy="0.3" layer="1" roundness="20" rot="R90"/>
<smd name="P2" x="-0.25" y="0.25" dx="1" dy="0.3" layer="1" roundness="20" rot="R90"/>
<smd name="P3" x="0.25" y="0.25" dx="1" dy="0.3" layer="1" roundness="20" rot="R90"/>
<smd name="P4" x="0.75" y="0.25" dx="1" dy="0.3" layer="1" roundness="20" rot="R90"/>
<wire x1="-2" y1="0" x2="-2" y2="-2.15" width="0.127" layer="21"/>
<wire x1="-2" y1="-2.15" x2="2" y2="-2.15" width="0.127" layer="21"/>
<wire x1="2" y1="-2.15" x2="2" y2="0" width="0.127" layer="21"/>
<wire x1="2" y1="0" x2="-2" y2="0" width="0.127" layer="21"/>
<smd name="P$7" x="1.95" y="-1.8" dx="0.75" dy="0.5" layer="1" roundness="20" rot="R90"/>
<smd name="P$8" x="-1.95" y="-1.8" dx="0.75" dy="0.5" layer="1" roundness="20" rot="R90"/>
<wire x1="-1.8" y1="-0.6" x2="-1.8" y2="-0.2" width="0.127" layer="21"/>
<wire x1="-1.8" y1="-0.2" x2="-1.4" y2="-0.2" width="0.127" layer="21"/>
<rectangle x1="-1" y1="-1" x2="-0.5" y2="-0.5" layer="21"/>
<text x="-1.8" y="-3.1" size="0.8" layer="21" ratio="15">&gt;NAME</text>
<wire x1="-2" y1="0" x2="-2" y2="0.35" width="0.127" layer="21"/>
<wire x1="-2" y1="0.35" x2="2" y2="0.35" width="0.127" layer="21"/>
<wire x1="2" y1="0.35" x2="2" y2="0" width="0.127" layer="21"/>
</package>
<package name="0402">
<smd name="1" x="-0.49" y="0" dx="0.58" dy="0.66" layer="1" roundness="10"/>
<smd name="2" x="0.49" y="0" dx="0.58" dy="0.66" layer="1" roundness="10"/>
<text x="-1.15" y="0.8" size="0.75" layer="25" ratio="15">&gt;NAME</text>
<rectangle x1="-0.0729" y1="-0.4389" x2="0.3269" y2="0.1611" layer="35"/>
<wire x1="-1.05" y1="0.6" x2="1.05" y2="0.6" width="0.2032" layer="21"/>
<wire x1="1.05" y1="0.6" x2="1.05" y2="-0.6" width="0.2032" layer="21"/>
<wire x1="1.05" y1="-0.6" x2="-1.05" y2="-0.6" width="0.2032" layer="21"/>
<wire x1="-1.05" y1="-0.6" x2="-1.05" y2="0.6" width="0.2032" layer="21"/>
<rectangle x1="-0.95" y1="-0.5" x2="-0.45" y2="0.5" layer="21"/>
</package>
<package name="0603">
<smd name="1" x="-0.8" y="0" dx="0.95" dy="1" layer="1" roundness="15"/>
<smd name="2" x="0.8" y="0" dx="0.95" dy="1" layer="1" roundness="15"/>
<text x="-1.7" y="1.2" size="0.9" layer="21" font="vector" ratio="15">&gt;NAME</text>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
<wire x1="-1.65" y1="0.85" x2="-1.65" y2="-0.85" width="0.2032" layer="21"/>
<wire x1="-1.65" y1="-0.85" x2="1.65" y2="-0.85" width="0.2032" layer="21"/>
<wire x1="1.65" y1="-0.85" x2="1.65" y2="0.85" width="0.2032" layer="21"/>
<wire x1="1.65" y1="0.85" x2="-1.65" y2="0.85" width="0.2032" layer="21"/>
<rectangle x1="-1.55" y1="-0.75" x2="-0.8" y2="0.75" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="LED_WS2812B">
<pin name="1.VDD" x="-12.7" y="5.08" length="short"/>
<pin name="3.VSS" x="-12.7" y="-5.08" length="short"/>
<pin name="4.DIN" x="-12.7" y="0" length="short"/>
<pin name="3.DOUT" x="12.7" y="0" length="short" rot="R180"/>
<wire x1="-10.16" y1="7.62" x2="-10.16" y2="-7.62" width="0.254" layer="94"/>
<wire x1="-10.16" y1="-7.62" x2="10.16" y2="-7.62" width="0.254" layer="94"/>
<wire x1="10.16" y1="-7.62" x2="10.16" y2="7.62" width="0.254" layer="94"/>
<wire x1="10.16" y1="7.62" x2="-10.16" y2="7.62" width="0.254" layer="94"/>
<text x="-10.16" y="7.62" size="1.778" layer="95">&gt;NAME</text>
<text x="-2.54" y="-10.16" size="1.778" layer="94">WS2812B</text>
</symbol>
<symbol name="GND">
<wire x1="-1.905" y1="0" x2="1.905" y2="0" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
<symbol name="+VBATT">
<wire x1="1.27" y1="0.635" x2="0" y2="2.54" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="-1.27" y2="0.635" width="0.254" layer="94"/>
<text x="0" y="3.81" size="1.778" layer="96" rot="R180" align="center">&gt;VALUE</text>
<pin name="+VBATT" x="0" y="0" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
<symbol name="A3L-LOC">
<wire x1="0" y1="0" x2="50.8" y2="0" width="0.1016" layer="94"/>
<wire x1="50.8" y1="0" x2="98.425" y2="0" width="0.1016" layer="94"/>
<wire x1="98.425" y1="0" x2="146.05" y2="0" width="0.1016" layer="94"/>
<wire x1="146.05" y1="0" x2="193.675" y2="0" width="0.1016" layer="94"/>
<wire x1="193.675" y1="0" x2="241.3" y2="0" width="0.1016" layer="94"/>
<wire x1="241.3" y1="0" x2="288.925" y2="0" width="0.1016" layer="94"/>
<wire x1="288.925" y1="0" x2="336.55" y2="0" width="0.1016" layer="94"/>
<wire x1="336.55" y1="0" x2="387.35" y2="0" width="0.1016" layer="94"/>
<wire x1="387.35" y1="0" x2="387.35" y2="53.975" width="0.1016" layer="94"/>
<wire x1="387.35" y1="53.975" x2="387.35" y2="104.775" width="0.1016" layer="94"/>
<wire x1="387.35" y1="104.775" x2="387.35" y2="155.575" width="0.1016" layer="94"/>
<wire x1="387.35" y1="155.575" x2="387.35" y2="206.375" width="0.1016" layer="94"/>
<wire x1="387.35" y1="206.375" x2="387.35" y2="260.35" width="0.1016" layer="94"/>
<wire x1="146.05" y1="260.35" x2="98.425" y2="260.35" width="0.1016" layer="94"/>
<wire x1="98.425" y1="260.35" x2="50.8" y2="260.35" width="0.1016" layer="94"/>
<wire x1="50.8" y1="260.35" x2="0" y2="260.35" width="0.1016" layer="94"/>
<wire x1="0" y1="260.35" x2="0" y2="206.375" width="0.1016" layer="94"/>
<wire x1="0" y1="206.375" x2="0" y2="155.575" width="0.1016" layer="94"/>
<wire x1="0" y1="155.575" x2="0" y2="104.775" width="0.1016" layer="94"/>
<wire x1="0" y1="104.775" x2="0" y2="53.975" width="0.1016" layer="94"/>
<wire x1="0" y1="53.975" x2="0" y2="0" width="0.1016" layer="94"/>
<wire x1="3.81" y1="3.81" x2="50.8" y2="3.81" width="0.1016" layer="94"/>
<wire x1="50.8" y1="3.81" x2="98.425" y2="3.81" width="0.1016" layer="94"/>
<wire x1="98.425" y1="3.81" x2="146.05" y2="3.81" width="0.1016" layer="94"/>
<wire x1="146.05" y1="3.81" x2="193.675" y2="3.81" width="0.1016" layer="94"/>
<wire x1="193.675" y1="3.81" x2="241.3" y2="3.81" width="0.1016" layer="94"/>
<wire x1="241.3" y1="3.81" x2="288.925" y2="3.81" width="0.1016" layer="94"/>
<wire x1="336.55" y1="3.81" x2="326.39" y2="3.81" width="0.1016" layer="94"/>
<wire x1="326.39" y1="3.81" x2="288.925" y2="3.81" width="0.1016" layer="94"/>
<wire x1="336.55" y1="3.81" x2="369.57" y2="3.81" width="0.1016" layer="94"/>
<wire x1="369.57" y1="3.81" x2="383.54" y2="3.81" width="0.1016" layer="94"/>
<wire x1="383.54" y1="3.81" x2="383.54" y2="8.89" width="0.1016" layer="94"/>
<wire x1="383.54" y1="8.89" x2="383.54" y2="13.97" width="0.1016" layer="94"/>
<wire x1="383.54" y1="13.97" x2="383.54" y2="19.05" width="0.1016" layer="94"/>
<wire x1="383.54" y1="19.05" x2="383.54" y2="24.13" width="0.1016" layer="94"/>
<wire x1="383.54" y1="24.13" x2="383.54" y2="53.975" width="0.1016" layer="94"/>
<wire x1="383.54" y1="53.975" x2="383.54" y2="104.775" width="0.1016" layer="94"/>
<wire x1="383.54" y1="104.775" x2="383.54" y2="155.575" width="0.1016" layer="94"/>
<wire x1="383.54" y1="155.575" x2="383.54" y2="206.375" width="0.1016" layer="94"/>
<wire x1="383.54" y1="206.375" x2="383.54" y2="256.54" width="0.1016" layer="94"/>
<wire x1="383.54" y1="256.54" x2="336.55" y2="256.54" width="0.1016" layer="94"/>
<wire x1="336.55" y1="256.54" x2="288.925" y2="256.54" width="0.1016" layer="94"/>
<wire x1="288.925" y1="256.54" x2="241.3" y2="256.54" width="0.1016" layer="94"/>
<wire x1="241.3" y1="256.54" x2="193.675" y2="256.54" width="0.1016" layer="94"/>
<wire x1="193.675" y1="256.54" x2="146.05" y2="256.54" width="0.1016" layer="94"/>
<wire x1="146.05" y1="256.54" x2="98.425" y2="256.54" width="0.1016" layer="94"/>
<wire x1="98.425" y1="256.54" x2="50.8" y2="256.54" width="0.1016" layer="94"/>
<wire x1="50.8" y1="256.54" x2="3.81" y2="256.54" width="0.1016" layer="94"/>
<wire x1="3.81" y1="256.54" x2="3.81" y2="206.375" width="0.1016" layer="94"/>
<wire x1="3.81" y1="206.375" x2="3.81" y2="155.575" width="0.1016" layer="94"/>
<wire x1="3.81" y1="155.575" x2="3.81" y2="104.775" width="0.1016" layer="94"/>
<wire x1="3.81" y1="104.775" x2="3.81" y2="53.975" width="0.1016" layer="94"/>
<wire x1="3.81" y1="53.975" x2="3.81" y2="3.81" width="0.1016" layer="94"/>
<wire x1="387.35" y1="260.35" x2="336.55" y2="260.35" width="0.1016" layer="94"/>
<wire x1="336.55" y1="260.35" x2="288.925" y2="260.35" width="0.1016" layer="94"/>
<wire x1="288.925" y1="260.35" x2="241.3" y2="260.35" width="0.1016" layer="94"/>
<wire x1="241.3" y1="260.35" x2="193.675" y2="260.35" width="0.1016" layer="94"/>
<wire x1="193.675" y1="260.35" x2="146.05" y2="260.35" width="0.1016" layer="94"/>
<wire x1="193.675" y1="260.35" x2="193.675" y2="256.54" width="0.1016" layer="94"/>
<wire x1="193.675" y1="3.81" x2="193.675" y2="0" width="0.1016" layer="94"/>
<wire x1="0" y1="104.775" x2="3.81" y2="104.775" width="0.1016" layer="94"/>
<wire x1="383.54" y1="155.575" x2="387.35" y2="155.575" width="0.1016" layer="94"/>
<wire x1="98.425" y1="256.54" x2="98.425" y2="260.35" width="0.1016" layer="94"/>
<wire x1="98.425" y1="3.81" x2="98.425" y2="0" width="0.1016" layer="94"/>
<wire x1="288.925" y1="260.35" x2="288.925" y2="256.54" width="0.1016" layer="94"/>
<wire x1="288.925" y1="3.81" x2="288.925" y2="0" width="0.1016" layer="94"/>
<wire x1="0" y1="53.975" x2="3.81" y2="53.975" width="0.1016" layer="94"/>
<wire x1="383.54" y1="104.775" x2="387.35" y2="104.775" width="0.1016" layer="94"/>
<wire x1="0" y1="155.575" x2="3.81" y2="155.575" width="0.1016" layer="94"/>
<wire x1="383.54" y1="206.375" x2="387.35" y2="206.375" width="0.1016" layer="94"/>
<wire x1="50.8" y1="256.54" x2="50.8" y2="260.35" width="0.1016" layer="94"/>
<wire x1="0" y1="206.375" x2="3.81" y2="206.375" width="0.1016" layer="94"/>
<wire x1="383.54" y1="53.975" x2="387.35" y2="53.975" width="0.1016" layer="94"/>
<wire x1="146.05" y1="256.54" x2="146.05" y2="260.35" width="0.1016" layer="94"/>
<wire x1="241.3" y1="260.35" x2="241.3" y2="256.54" width="0.1016" layer="94"/>
<wire x1="336.55" y1="260.35" x2="336.55" y2="256.54" width="0.1016" layer="94"/>
<wire x1="336.55" y1="3.81" x2="336.55" y2="0" width="0.1016" layer="94"/>
<wire x1="241.3" y1="3.81" x2="241.3" y2="0" width="0.1016" layer="94"/>
<wire x1="146.05" y1="3.81" x2="146.05" y2="0" width="0.1016" layer="94"/>
<wire x1="50.8" y1="0" x2="50.8" y2="3.81" width="0.1016" layer="94"/>
<text x="24.384" y="0.254" size="2.54" layer="94" font="vector">A</text>
<text x="74.422" y="0.254" size="2.54" layer="94" font="vector">B</text>
<text x="121.158" y="0.254" size="2.54" layer="94" font="vector">C</text>
<text x="169.418" y="0.254" size="2.54" layer="94" font="vector">D</text>
<text x="216.916" y="0.254" size="2.54" layer="94" font="vector">E</text>
<text x="263.652" y="0.254" size="2.54" layer="94" font="vector">F</text>
<text x="310.642" y="0.254" size="2.54" layer="94" font="vector">G</text>
<text x="360.934" y="0.254" size="2.54" layer="94" font="vector">H</text>
<text x="385.064" y="28.702" size="2.54" layer="94" font="vector">1</text>
<text x="384.81" y="79.502" size="2.54" layer="94" font="vector">2</text>
<text x="384.81" y="130.302" size="2.54" layer="94" font="vector">3</text>
<text x="384.81" y="181.864" size="2.54" layer="94" font="vector">4</text>
<text x="384.81" y="231.14" size="2.54" layer="94" font="vector">5</text>
<text x="361.188" y="257.556" size="2.54" layer="94" font="vector">H</text>
<text x="311.404" y="257.556" size="2.54" layer="94" font="vector">G</text>
<text x="262.89" y="257.556" size="2.54" layer="94" font="vector">F</text>
<text x="215.9" y="257.556" size="2.54" layer="94" font="vector">E</text>
<text x="168.148" y="257.556" size="2.54" layer="94" font="vector">D</text>
<text x="120.904" y="257.556" size="2.54" layer="94" font="vector">C</text>
<text x="72.898" y="257.556" size="2.54" layer="94" font="vector">B</text>
<text x="24.384" y="257.556" size="2.54" layer="94" font="vector">A</text>
<text x="0.762" y="231.14" size="2.54" layer="94" font="vector">5</text>
<text x="0.762" y="181.61" size="2.54" layer="94" font="vector">4</text>
<text x="0.762" y="130.302" size="2.54" layer="94" font="vector">3</text>
<text x="0.762" y="79.248" size="2.54" layer="94" font="vector">2</text>
<text x="1.016" y="26.67" size="2.54" layer="94" font="vector">1</text>
<text x="327.66" y="5.08" size="1.778" layer="94">&gt;DRAWING_NAME</text>
<text x="372.11" y="5.08" size="1.778" layer="94">&gt;SHEET</text>
<wire x1="326.39" y1="3.81" x2="326.39" y2="8.89" width="0.127" layer="94"/>
<wire x1="326.39" y1="8.89" x2="369.57" y2="8.89" width="0.127" layer="94"/>
<wire x1="369.57" y1="8.89" x2="369.57" y2="3.81" width="0.127" layer="94"/>
<wire x1="369.57" y1="8.89" x2="383.54" y2="8.89" width="0.127" layer="94"/>
<text x="327.66" y="10.16" size="1.778" layer="94">$rev: $</text>
<wire x1="326.39" y1="8.89" x2="326.39" y2="13.97" width="0.127" layer="94"/>
<wire x1="326.39" y1="13.97" x2="383.54" y2="13.97" width="0.127" layer="94"/>
<wire x1="326.39" y1="13.97" x2="326.39" y2="19.05" width="0.127" layer="94"/>
<wire x1="326.39" y1="19.05" x2="326.39" y2="24.13" width="0.127" layer="94"/>
<wire x1="326.39" y1="19.05" x2="383.54" y2="19.05" width="0.127" layer="94"/>
<wire x1="326.39" y1="24.13" x2="383.54" y2="24.13" width="0.127" layer="94"/>
</symbol>
<symbol name="4POS_TERMINAL">
<wire x1="0" y1="2.54" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="12.7" y2="0" width="0.254" layer="94"/>
<wire x1="12.7" y1="0" x2="12.7" y2="2.54" width="0.254" layer="94"/>
<wire x1="12.7" y1="2.54" x2="0" y2="2.54" width="0.254" layer="94"/>
<wire x1="2.54" y1="0" x2="2.54" y2="2.54" width="0.254" layer="94"/>
<text x="12.7" y="0" size="1.27" layer="95" rot="R180">&gt;NAME</text>
<pin name="P$1" x="2.54" y="7.62" visible="off" length="middle" rot="R270"/>
<pin name="P$2" x="5.08" y="7.62" visible="off" length="middle" rot="R270"/>
<pin name="P$3" x="7.62" y="7.62" visible="off" length="middle" rot="R270"/>
<pin name="P$4" x="10.16" y="7.62" visible="off" length="middle" rot="R270"/>
</symbol>
<symbol name="C">
<wire x1="0" y1="2.54" x2="0" y2="2.032" width="0.1524" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="0.508" width="0.1524" layer="94"/>
<text x="0.5" y="-1" size="1.25" layer="95" ratio="20">&gt;NAME</text>
<text x="0.5" y="-2.1" size="1" layer="96" ratio="10">&gt;VALUE</text>
<text x="-1.27" y="2.54" size="1.27" layer="94">+</text>
<rectangle x1="-2.032" y1="0.508" x2="2.032" y2="1.016" layer="94"/>
<rectangle x1="-2.032" y1="1.524" x2="2.032" y2="2.032" layer="94"/>
<pin name="1" x="0" y="5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="2" x="0" y="-2.54" visible="off" length="short" direction="pas" swaplevel="1" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="LED_WS2812B">
<gates>
<gate name="G$1" symbol="LED_WS2812B" x="0" y="0"/>
</gates>
<devices>
<device name="" package="LED_WS2812B">
<connects>
<connect gate="G$1" pin="1.VDD" pad="P1"/>
<connect gate="G$1" pin="3.DOUT" pad="P2"/>
<connect gate="G$1" pin="3.VSS" pad="P3"/>
<connect gate="G$1" pin="4.DIN" pad="P4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="GND" prefix="GND">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="+VBATT">
<gates>
<gate name="G$1" symbol="+VBATT" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="FRAME-A3" prefix="FRAME">
<description>&lt;b&gt;Schematic Frame&lt;/b&gt;&lt;p&gt;
A3 Larger Frame</description>
<gates>
<gate name="G$1" symbol="A3L-LOC" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="4POS_TERMINAL">
<gates>
<gate name="G$1" symbol="4POS_TERMINAL" x="0" y="0"/>
</gates>
<devices>
<device name="_254MM_THRU" package="4POS_254MM_1R_THRU">
<connects>
<connect gate="G$1" pin="P$1" pad="P$1"/>
<connect gate="G$1" pin="P$2" pad="P$2"/>
<connect gate="G$1" pin="P$3" pad="P$3"/>
<connect gate="G$1" pin="P$4" pad="P$4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_127MM_SMD" package="4POS_127MM_1R_SMD">
<connects>
<connect gate="G$1" pin="P$1" pad="P1"/>
<connect gate="G$1" pin="P$2" pad="P2"/>
<connect gate="G$1" pin="P$3" pad="P3"/>
<connect gate="G$1" pin="P$4" pad="P4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_254_MOLEX53426" package="4POS_250MM_MOLEX53426">
<connects>
<connect gate="G$1" pin="P$1" pad="P$1"/>
<connect gate="G$1" pin="P$2" pad="P$2"/>
<connect gate="G$1" pin="P$3" pad="P$3"/>
<connect gate="G$1" pin="P$4" pad="P$4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_150MM_SMD_WIDE" package="4POS_150MM_1R_SMD_WIDE">
<connects>
<connect gate="G$1" pin="P$1" pad="P$1"/>
<connect gate="G$1" pin="P$2" pad="P$2"/>
<connect gate="G$1" pin="P$3" pad="P$3"/>
<connect gate="G$1" pin="P$4" pad="P$4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_0.5MM_FH33" package="4POS_0.5MM_FFC_FH33">
<connects>
<connect gate="G$1" pin="P$1" pad="P1"/>
<connect gate="G$1" pin="P$2" pad="P2"/>
<connect gate="G$1" pin="P$3" pad="P3"/>
<connect gate="G$1" pin="P$4" pad="P4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_0.5MM_PADS" package="4POS_0.5MM_FFC_PADS">
<connects>
<connect gate="G$1" pin="P$1" pad="P1"/>
<connect gate="G$1" pin="P$2" pad="P2"/>
<connect gate="G$1" pin="P$3" pad="P3"/>
<connect gate="G$1" pin="P$4" pad="P4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_254MM_2R_SMT" package="4POS_254MM_2R_SMT">
<connects>
<connect gate="G$1" pin="P$1" pad="P1"/>
<connect gate="G$1" pin="P$2" pad="P2"/>
<connect gate="G$1" pin="P$3" pad="P3"/>
<connect gate="G$1" pin="P$4" pad="P4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="C" prefix="C" uservalue="yes">
<description>&lt;b&gt;Capacitor&lt;/b&gt;
Standard 0603 ceramic capacitor, and 0.1" leaded capacitor.</description>
<gates>
<gate name="G$1" symbol="C" x="0" y="0"/>
</gates>
<devices>
<device name="0805" package="0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0402" package="0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0603" package="0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1206" package="1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_TH_SMALL" package="CAP-PTH-SMALL">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0201" package="0201">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_SUPERCAP_PHV_32X21MM" package="SUPERCAP_PHV_32X21MM">
<connects>
<connect gate="G$1" pin="1" pad="P1"/>
<connect gate="G$1" pin="2" pad="P2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_SUPERCAP_DCK-3R3E224U-E" package="SUPERCAP_DCK-3R3E224U-E">
<connects>
<connect gate="G$1" pin="1" pad="P1"/>
<connect gate="G$1" pin="2" pad="P2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="LED1" library="aditya_main" deviceset="LED_WS2812B" device=""/>
<part name="LED2" library="aditya_main" deviceset="LED_WS2812B" device=""/>
<part name="LED3" library="aditya_main" deviceset="LED_WS2812B" device=""/>
<part name="LED4" library="aditya_main" deviceset="LED_WS2812B" device=""/>
<part name="LED5" library="aditya_main" deviceset="LED_WS2812B" device=""/>
<part name="LED6" library="aditya_main" deviceset="LED_WS2812B" device=""/>
<part name="LED7" library="aditya_main" deviceset="LED_WS2812B" device=""/>
<part name="LED8" library="aditya_main" deviceset="LED_WS2812B" device=""/>
<part name="GND1" library="aditya_main" deviceset="GND" device=""/>
<part name="GND2" library="aditya_main" deviceset="GND" device=""/>
<part name="GND3" library="aditya_main" deviceset="GND" device=""/>
<part name="GND4" library="aditya_main" deviceset="GND" device=""/>
<part name="GND5" library="aditya_main" deviceset="GND" device=""/>
<part name="GND6" library="aditya_main" deviceset="GND" device=""/>
<part name="GND7" library="aditya_main" deviceset="GND" device=""/>
<part name="GND8" library="aditya_main" deviceset="GND" device=""/>
<part name="U$9" library="aditya_main" deviceset="+VBATT" device=""/>
<part name="U$10" library="aditya_main" deviceset="+VBATT" device=""/>
<part name="U$11" library="aditya_main" deviceset="+VBATT" device=""/>
<part name="U$12" library="aditya_main" deviceset="+VBATT" device=""/>
<part name="U$13" library="aditya_main" deviceset="+VBATT" device=""/>
<part name="U$14" library="aditya_main" deviceset="+VBATT" device=""/>
<part name="U$15" library="aditya_main" deviceset="+VBATT" device=""/>
<part name="U$16" library="aditya_main" deviceset="+VBATT" device=""/>
<part name="LED9" library="aditya_main" deviceset="LED_WS2812B" device=""/>
<part name="LED10" library="aditya_main" deviceset="LED_WS2812B" device=""/>
<part name="LED11" library="aditya_main" deviceset="LED_WS2812B" device=""/>
<part name="LED12" library="aditya_main" deviceset="LED_WS2812B" device=""/>
<part name="LED13" library="aditya_main" deviceset="LED_WS2812B" device=""/>
<part name="LED14" library="aditya_main" deviceset="LED_WS2812B" device=""/>
<part name="LED15" library="aditya_main" deviceset="LED_WS2812B" device=""/>
<part name="LED16" library="aditya_main" deviceset="LED_WS2812B" device=""/>
<part name="GND9" library="aditya_main" deviceset="GND" device=""/>
<part name="GND10" library="aditya_main" deviceset="GND" device=""/>
<part name="GND11" library="aditya_main" deviceset="GND" device=""/>
<part name="GND12" library="aditya_main" deviceset="GND" device=""/>
<part name="GND13" library="aditya_main" deviceset="GND" device=""/>
<part name="GND14" library="aditya_main" deviceset="GND" device=""/>
<part name="GND15" library="aditya_main" deviceset="GND" device=""/>
<part name="GND16" library="aditya_main" deviceset="GND" device=""/>
<part name="U$1" library="aditya_main" deviceset="+VBATT" device=""/>
<part name="U$2" library="aditya_main" deviceset="+VBATT" device=""/>
<part name="U$3" library="aditya_main" deviceset="+VBATT" device=""/>
<part name="U$4" library="aditya_main" deviceset="+VBATT" device=""/>
<part name="U$5" library="aditya_main" deviceset="+VBATT" device=""/>
<part name="U$6" library="aditya_main" deviceset="+VBATT" device=""/>
<part name="U$7" library="aditya_main" deviceset="+VBATT" device=""/>
<part name="U$8" library="aditya_main" deviceset="+VBATT" device=""/>
<part name="LED17" library="aditya_main" deviceset="LED_WS2812B" device=""/>
<part name="LED18" library="aditya_main" deviceset="LED_WS2812B" device=""/>
<part name="LED19" library="aditya_main" deviceset="LED_WS2812B" device=""/>
<part name="LED20" library="aditya_main" deviceset="LED_WS2812B" device=""/>
<part name="LED21" library="aditya_main" deviceset="LED_WS2812B" device=""/>
<part name="LED22" library="aditya_main" deviceset="LED_WS2812B" device=""/>
<part name="LED23" library="aditya_main" deviceset="LED_WS2812B" device=""/>
<part name="LED24" library="aditya_main" deviceset="LED_WS2812B" device=""/>
<part name="GND17" library="aditya_main" deviceset="GND" device=""/>
<part name="GND18" library="aditya_main" deviceset="GND" device=""/>
<part name="GND19" library="aditya_main" deviceset="GND" device=""/>
<part name="GND20" library="aditya_main" deviceset="GND" device=""/>
<part name="GND21" library="aditya_main" deviceset="GND" device=""/>
<part name="GND22" library="aditya_main" deviceset="GND" device=""/>
<part name="GND23" library="aditya_main" deviceset="GND" device=""/>
<part name="GND24" library="aditya_main" deviceset="GND" device=""/>
<part name="U$17" library="aditya_main" deviceset="+VBATT" device=""/>
<part name="U$18" library="aditya_main" deviceset="+VBATT" device=""/>
<part name="U$19" library="aditya_main" deviceset="+VBATT" device=""/>
<part name="U$20" library="aditya_main" deviceset="+VBATT" device=""/>
<part name="U$21" library="aditya_main" deviceset="+VBATT" device=""/>
<part name="U$22" library="aditya_main" deviceset="+VBATT" device=""/>
<part name="U$23" library="aditya_main" deviceset="+VBATT" device=""/>
<part name="U$24" library="aditya_main" deviceset="+VBATT" device=""/>
<part name="LED25" library="aditya_main" deviceset="LED_WS2812B" device=""/>
<part name="LED26" library="aditya_main" deviceset="LED_WS2812B" device=""/>
<part name="LED27" library="aditya_main" deviceset="LED_WS2812B" device=""/>
<part name="LED28" library="aditya_main" deviceset="LED_WS2812B" device=""/>
<part name="LED29" library="aditya_main" deviceset="LED_WS2812B" device=""/>
<part name="LED30" library="aditya_main" deviceset="LED_WS2812B" device=""/>
<part name="LED31" library="aditya_main" deviceset="LED_WS2812B" device=""/>
<part name="LED32" library="aditya_main" deviceset="LED_WS2812B" device=""/>
<part name="GND25" library="aditya_main" deviceset="GND" device=""/>
<part name="GND26" library="aditya_main" deviceset="GND" device=""/>
<part name="GND27" library="aditya_main" deviceset="GND" device=""/>
<part name="GND28" library="aditya_main" deviceset="GND" device=""/>
<part name="GND29" library="aditya_main" deviceset="GND" device=""/>
<part name="GND30" library="aditya_main" deviceset="GND" device=""/>
<part name="GND31" library="aditya_main" deviceset="GND" device=""/>
<part name="GND32" library="aditya_main" deviceset="GND" device=""/>
<part name="U$25" library="aditya_main" deviceset="+VBATT" device=""/>
<part name="U$26" library="aditya_main" deviceset="+VBATT" device=""/>
<part name="U$27" library="aditya_main" deviceset="+VBATT" device=""/>
<part name="U$28" library="aditya_main" deviceset="+VBATT" device=""/>
<part name="U$29" library="aditya_main" deviceset="+VBATT" device=""/>
<part name="U$30" library="aditya_main" deviceset="+VBATT" device=""/>
<part name="U$31" library="aditya_main" deviceset="+VBATT" device=""/>
<part name="U$32" library="aditya_main" deviceset="+VBATT" device=""/>
<part name="LED33" library="aditya_main" deviceset="LED_WS2812B" device=""/>
<part name="LED34" library="aditya_main" deviceset="LED_WS2812B" device=""/>
<part name="LED35" library="aditya_main" deviceset="LED_WS2812B" device=""/>
<part name="LED36" library="aditya_main" deviceset="LED_WS2812B" device=""/>
<part name="LED37" library="aditya_main" deviceset="LED_WS2812B" device=""/>
<part name="LED38" library="aditya_main" deviceset="LED_WS2812B" device=""/>
<part name="LED39" library="aditya_main" deviceset="LED_WS2812B" device=""/>
<part name="LED40" library="aditya_main" deviceset="LED_WS2812B" device=""/>
<part name="GND33" library="aditya_main" deviceset="GND" device=""/>
<part name="GND34" library="aditya_main" deviceset="GND" device=""/>
<part name="GND35" library="aditya_main" deviceset="GND" device=""/>
<part name="GND36" library="aditya_main" deviceset="GND" device=""/>
<part name="GND37" library="aditya_main" deviceset="GND" device=""/>
<part name="GND38" library="aditya_main" deviceset="GND" device=""/>
<part name="GND39" library="aditya_main" deviceset="GND" device=""/>
<part name="GND40" library="aditya_main" deviceset="GND" device=""/>
<part name="U$33" library="aditya_main" deviceset="+VBATT" device=""/>
<part name="U$34" library="aditya_main" deviceset="+VBATT" device=""/>
<part name="U$35" library="aditya_main" deviceset="+VBATT" device=""/>
<part name="U$36" library="aditya_main" deviceset="+VBATT" device=""/>
<part name="U$37" library="aditya_main" deviceset="+VBATT" device=""/>
<part name="U$38" library="aditya_main" deviceset="+VBATT" device=""/>
<part name="U$39" library="aditya_main" deviceset="+VBATT" device=""/>
<part name="U$40" library="aditya_main" deviceset="+VBATT" device=""/>
<part name="LED41" library="aditya_main" deviceset="LED_WS2812B" device=""/>
<part name="LED42" library="aditya_main" deviceset="LED_WS2812B" device=""/>
<part name="LED43" library="aditya_main" deviceset="LED_WS2812B" device=""/>
<part name="LED44" library="aditya_main" deviceset="LED_WS2812B" device=""/>
<part name="LED45" library="aditya_main" deviceset="LED_WS2812B" device=""/>
<part name="LED46" library="aditya_main" deviceset="LED_WS2812B" device=""/>
<part name="LED47" library="aditya_main" deviceset="LED_WS2812B" device=""/>
<part name="LED48" library="aditya_main" deviceset="LED_WS2812B" device=""/>
<part name="GND41" library="aditya_main" deviceset="GND" device=""/>
<part name="GND42" library="aditya_main" deviceset="GND" device=""/>
<part name="GND43" library="aditya_main" deviceset="GND" device=""/>
<part name="GND44" library="aditya_main" deviceset="GND" device=""/>
<part name="GND45" library="aditya_main" deviceset="GND" device=""/>
<part name="GND46" library="aditya_main" deviceset="GND" device=""/>
<part name="GND47" library="aditya_main" deviceset="GND" device=""/>
<part name="GND48" library="aditya_main" deviceset="GND" device=""/>
<part name="U$41" library="aditya_main" deviceset="+VBATT" device=""/>
<part name="U$42" library="aditya_main" deviceset="+VBATT" device=""/>
<part name="U$43" library="aditya_main" deviceset="+VBATT" device=""/>
<part name="U$44" library="aditya_main" deviceset="+VBATT" device=""/>
<part name="U$45" library="aditya_main" deviceset="+VBATT" device=""/>
<part name="U$46" library="aditya_main" deviceset="+VBATT" device=""/>
<part name="U$47" library="aditya_main" deviceset="+VBATT" device=""/>
<part name="U$48" library="aditya_main" deviceset="+VBATT" device=""/>
<part name="LED49" library="aditya_main" deviceset="LED_WS2812B" device=""/>
<part name="LED50" library="aditya_main" deviceset="LED_WS2812B" device=""/>
<part name="LED51" library="aditya_main" deviceset="LED_WS2812B" device=""/>
<part name="LED52" library="aditya_main" deviceset="LED_WS2812B" device=""/>
<part name="LED53" library="aditya_main" deviceset="LED_WS2812B" device=""/>
<part name="LED54" library="aditya_main" deviceset="LED_WS2812B" device=""/>
<part name="LED55" library="aditya_main" deviceset="LED_WS2812B" device=""/>
<part name="LED56" library="aditya_main" deviceset="LED_WS2812B" device=""/>
<part name="GND49" library="aditya_main" deviceset="GND" device=""/>
<part name="GND50" library="aditya_main" deviceset="GND" device=""/>
<part name="GND51" library="aditya_main" deviceset="GND" device=""/>
<part name="GND52" library="aditya_main" deviceset="GND" device=""/>
<part name="GND53" library="aditya_main" deviceset="GND" device=""/>
<part name="GND54" library="aditya_main" deviceset="GND" device=""/>
<part name="GND55" library="aditya_main" deviceset="GND" device=""/>
<part name="GND56" library="aditya_main" deviceset="GND" device=""/>
<part name="U$49" library="aditya_main" deviceset="+VBATT" device=""/>
<part name="U$50" library="aditya_main" deviceset="+VBATT" device=""/>
<part name="U$51" library="aditya_main" deviceset="+VBATT" device=""/>
<part name="U$52" library="aditya_main" deviceset="+VBATT" device=""/>
<part name="U$53" library="aditya_main" deviceset="+VBATT" device=""/>
<part name="U$54" library="aditya_main" deviceset="+VBATT" device=""/>
<part name="U$55" library="aditya_main" deviceset="+VBATT" device=""/>
<part name="U$56" library="aditya_main" deviceset="+VBATT" device=""/>
<part name="LED57" library="aditya_main" deviceset="LED_WS2812B" device=""/>
<part name="LED58" library="aditya_main" deviceset="LED_WS2812B" device=""/>
<part name="LED59" library="aditya_main" deviceset="LED_WS2812B" device=""/>
<part name="LED60" library="aditya_main" deviceset="LED_WS2812B" device=""/>
<part name="LED61" library="aditya_main" deviceset="LED_WS2812B" device=""/>
<part name="LED62" library="aditya_main" deviceset="LED_WS2812B" device=""/>
<part name="LED63" library="aditya_main" deviceset="LED_WS2812B" device=""/>
<part name="LED64" library="aditya_main" deviceset="LED_WS2812B" device=""/>
<part name="GND57" library="aditya_main" deviceset="GND" device=""/>
<part name="GND58" library="aditya_main" deviceset="GND" device=""/>
<part name="GND59" library="aditya_main" deviceset="GND" device=""/>
<part name="GND60" library="aditya_main" deviceset="GND" device=""/>
<part name="GND61" library="aditya_main" deviceset="GND" device=""/>
<part name="GND62" library="aditya_main" deviceset="GND" device=""/>
<part name="GND63" library="aditya_main" deviceset="GND" device=""/>
<part name="GND64" library="aditya_main" deviceset="GND" device=""/>
<part name="U$57" library="aditya_main" deviceset="+VBATT" device=""/>
<part name="U$58" library="aditya_main" deviceset="+VBATT" device=""/>
<part name="U$59" library="aditya_main" deviceset="+VBATT" device=""/>
<part name="U$60" library="aditya_main" deviceset="+VBATT" device=""/>
<part name="U$61" library="aditya_main" deviceset="+VBATT" device=""/>
<part name="U$62" library="aditya_main" deviceset="+VBATT" device=""/>
<part name="U$63" library="aditya_main" deviceset="+VBATT" device=""/>
<part name="U$64" library="aditya_main" deviceset="+VBATT" device=""/>
<part name="FRAME1" library="aditya_main" deviceset="FRAME-A3" device=""/>
<part name="CONN" library="aditya_main" deviceset="4POS_TERMINAL" device="_254MM_2R_SMT"/>
<part name="U$65" library="aditya_main" deviceset="+VBATT" device=""/>
<part name="GND65" library="aditya_main" deviceset="GND" device=""/>
<part name="C1" library="aditya_main" deviceset="C" device="0805" value="10uF"/>
<part name="C2" library="aditya_main" deviceset="C" device="0805" value="10uF"/>
<part name="C3" library="aditya_main" deviceset="C" device="0805" value="10uF"/>
<part name="C4" library="aditya_main" deviceset="C" device="0805" value="10uF"/>
<part name="U$66" library="aditya_main" deviceset="+VBATT" device=""/>
<part name="GND66" library="aditya_main" deviceset="GND" device=""/>
</parts>
<sheets>
<sheet>
<plain>
</plain>
<instances>
<instance part="LED1" gate="G$1" x="33.02" y="22.86"/>
<instance part="LED2" gate="G$1" x="63.5" y="22.86"/>
<instance part="LED3" gate="G$1" x="93.98" y="22.86"/>
<instance part="LED4" gate="G$1" x="124.46" y="22.86"/>
<instance part="LED5" gate="G$1" x="154.94" y="22.86"/>
<instance part="LED6" gate="G$1" x="185.42" y="22.86"/>
<instance part="LED7" gate="G$1" x="215.9" y="22.86"/>
<instance part="LED8" gate="G$1" x="246.38" y="22.86"/>
<instance part="GND1" gate="1" x="17.78" y="12.7"/>
<instance part="GND2" gate="1" x="48.26" y="12.7"/>
<instance part="GND3" gate="1" x="78.74" y="12.7"/>
<instance part="GND4" gate="1" x="109.22" y="12.7"/>
<instance part="GND5" gate="1" x="139.7" y="12.7"/>
<instance part="GND6" gate="1" x="170.18" y="12.7"/>
<instance part="GND7" gate="1" x="200.66" y="12.7"/>
<instance part="GND8" gate="1" x="231.14" y="12.7"/>
<instance part="U$9" gate="G$1" x="17.78" y="30.48"/>
<instance part="U$10" gate="G$1" x="48.26" y="30.48"/>
<instance part="U$11" gate="G$1" x="78.74" y="30.48"/>
<instance part="U$12" gate="G$1" x="109.22" y="30.48"/>
<instance part="U$13" gate="G$1" x="139.7" y="30.48"/>
<instance part="U$14" gate="G$1" x="170.18" y="30.48"/>
<instance part="U$15" gate="G$1" x="200.66" y="30.48"/>
<instance part="U$16" gate="G$1" x="231.14" y="30.48"/>
<instance part="LED9" gate="G$1" x="33.02" y="53.34"/>
<instance part="LED10" gate="G$1" x="63.5" y="53.34"/>
<instance part="LED11" gate="G$1" x="93.98" y="53.34"/>
<instance part="LED12" gate="G$1" x="124.46" y="53.34"/>
<instance part="LED13" gate="G$1" x="154.94" y="53.34"/>
<instance part="LED14" gate="G$1" x="185.42" y="53.34"/>
<instance part="LED15" gate="G$1" x="215.9" y="53.34"/>
<instance part="LED16" gate="G$1" x="246.38" y="53.34"/>
<instance part="GND9" gate="1" x="17.78" y="43.18"/>
<instance part="GND10" gate="1" x="48.26" y="43.18"/>
<instance part="GND11" gate="1" x="78.74" y="43.18"/>
<instance part="GND12" gate="1" x="109.22" y="43.18"/>
<instance part="GND13" gate="1" x="139.7" y="43.18"/>
<instance part="GND14" gate="1" x="170.18" y="43.18"/>
<instance part="GND15" gate="1" x="200.66" y="43.18"/>
<instance part="GND16" gate="1" x="231.14" y="43.18"/>
<instance part="U$1" gate="G$1" x="17.78" y="60.96"/>
<instance part="U$2" gate="G$1" x="48.26" y="60.96"/>
<instance part="U$3" gate="G$1" x="78.74" y="60.96"/>
<instance part="U$4" gate="G$1" x="109.22" y="60.96"/>
<instance part="U$5" gate="G$1" x="139.7" y="60.96"/>
<instance part="U$6" gate="G$1" x="170.18" y="60.96"/>
<instance part="U$7" gate="G$1" x="200.66" y="60.96"/>
<instance part="U$8" gate="G$1" x="231.14" y="60.96"/>
<instance part="LED17" gate="G$1" x="33.02" y="83.82"/>
<instance part="LED18" gate="G$1" x="63.5" y="83.82"/>
<instance part="LED19" gate="G$1" x="93.98" y="83.82"/>
<instance part="LED20" gate="G$1" x="124.46" y="83.82"/>
<instance part="LED21" gate="G$1" x="154.94" y="83.82"/>
<instance part="LED22" gate="G$1" x="185.42" y="83.82"/>
<instance part="LED23" gate="G$1" x="215.9" y="83.82"/>
<instance part="LED24" gate="G$1" x="246.38" y="83.82"/>
<instance part="GND17" gate="1" x="17.78" y="73.66"/>
<instance part="GND18" gate="1" x="48.26" y="73.66"/>
<instance part="GND19" gate="1" x="78.74" y="73.66"/>
<instance part="GND20" gate="1" x="109.22" y="73.66"/>
<instance part="GND21" gate="1" x="139.7" y="73.66"/>
<instance part="GND22" gate="1" x="170.18" y="73.66"/>
<instance part="GND23" gate="1" x="200.66" y="73.66"/>
<instance part="GND24" gate="1" x="231.14" y="73.66"/>
<instance part="U$17" gate="G$1" x="17.78" y="91.44"/>
<instance part="U$18" gate="G$1" x="48.26" y="91.44"/>
<instance part="U$19" gate="G$1" x="78.74" y="91.44"/>
<instance part="U$20" gate="G$1" x="109.22" y="91.44"/>
<instance part="U$21" gate="G$1" x="139.7" y="91.44"/>
<instance part="U$22" gate="G$1" x="170.18" y="91.44"/>
<instance part="U$23" gate="G$1" x="200.66" y="91.44"/>
<instance part="U$24" gate="G$1" x="231.14" y="91.44"/>
<instance part="LED25" gate="G$1" x="33.02" y="114.3"/>
<instance part="LED26" gate="G$1" x="63.5" y="114.3"/>
<instance part="LED27" gate="G$1" x="93.98" y="114.3"/>
<instance part="LED28" gate="G$1" x="124.46" y="114.3"/>
<instance part="LED29" gate="G$1" x="154.94" y="114.3"/>
<instance part="LED30" gate="G$1" x="185.42" y="114.3"/>
<instance part="LED31" gate="G$1" x="215.9" y="114.3"/>
<instance part="LED32" gate="G$1" x="246.38" y="114.3"/>
<instance part="GND25" gate="1" x="17.78" y="104.14"/>
<instance part="GND26" gate="1" x="48.26" y="104.14"/>
<instance part="GND27" gate="1" x="78.74" y="104.14"/>
<instance part="GND28" gate="1" x="109.22" y="104.14"/>
<instance part="GND29" gate="1" x="139.7" y="104.14"/>
<instance part="GND30" gate="1" x="170.18" y="104.14"/>
<instance part="GND31" gate="1" x="200.66" y="104.14"/>
<instance part="GND32" gate="1" x="231.14" y="104.14"/>
<instance part="U$25" gate="G$1" x="17.78" y="121.92"/>
<instance part="U$26" gate="G$1" x="48.26" y="121.92"/>
<instance part="U$27" gate="G$1" x="78.74" y="121.92"/>
<instance part="U$28" gate="G$1" x="109.22" y="121.92"/>
<instance part="U$29" gate="G$1" x="139.7" y="121.92"/>
<instance part="U$30" gate="G$1" x="170.18" y="121.92"/>
<instance part="U$31" gate="G$1" x="200.66" y="121.92"/>
<instance part="U$32" gate="G$1" x="231.14" y="121.92"/>
<instance part="LED33" gate="G$1" x="33.02" y="144.78"/>
<instance part="LED34" gate="G$1" x="63.5" y="144.78"/>
<instance part="LED35" gate="G$1" x="93.98" y="144.78"/>
<instance part="LED36" gate="G$1" x="124.46" y="144.78"/>
<instance part="LED37" gate="G$1" x="154.94" y="144.78"/>
<instance part="LED38" gate="G$1" x="185.42" y="144.78"/>
<instance part="LED39" gate="G$1" x="215.9" y="144.78"/>
<instance part="LED40" gate="G$1" x="246.38" y="144.78"/>
<instance part="GND33" gate="1" x="17.78" y="134.62"/>
<instance part="GND34" gate="1" x="48.26" y="134.62"/>
<instance part="GND35" gate="1" x="78.74" y="134.62"/>
<instance part="GND36" gate="1" x="109.22" y="134.62"/>
<instance part="GND37" gate="1" x="139.7" y="134.62"/>
<instance part="GND38" gate="1" x="170.18" y="134.62"/>
<instance part="GND39" gate="1" x="200.66" y="134.62"/>
<instance part="GND40" gate="1" x="231.14" y="134.62"/>
<instance part="U$33" gate="G$1" x="17.78" y="152.4"/>
<instance part="U$34" gate="G$1" x="48.26" y="152.4"/>
<instance part="U$35" gate="G$1" x="78.74" y="152.4"/>
<instance part="U$36" gate="G$1" x="109.22" y="152.4"/>
<instance part="U$37" gate="G$1" x="139.7" y="152.4"/>
<instance part="U$38" gate="G$1" x="170.18" y="152.4"/>
<instance part="U$39" gate="G$1" x="200.66" y="152.4"/>
<instance part="U$40" gate="G$1" x="231.14" y="152.4"/>
<instance part="LED41" gate="G$1" x="33.02" y="175.26"/>
<instance part="LED42" gate="G$1" x="63.5" y="175.26"/>
<instance part="LED43" gate="G$1" x="93.98" y="175.26"/>
<instance part="LED44" gate="G$1" x="124.46" y="175.26"/>
<instance part="LED45" gate="G$1" x="154.94" y="175.26"/>
<instance part="LED46" gate="G$1" x="185.42" y="175.26"/>
<instance part="LED47" gate="G$1" x="215.9" y="175.26"/>
<instance part="LED48" gate="G$1" x="246.38" y="175.26"/>
<instance part="GND41" gate="1" x="17.78" y="165.1"/>
<instance part="GND42" gate="1" x="48.26" y="165.1"/>
<instance part="GND43" gate="1" x="78.74" y="165.1"/>
<instance part="GND44" gate="1" x="109.22" y="165.1"/>
<instance part="GND45" gate="1" x="139.7" y="165.1"/>
<instance part="GND46" gate="1" x="170.18" y="165.1"/>
<instance part="GND47" gate="1" x="200.66" y="165.1"/>
<instance part="GND48" gate="1" x="231.14" y="165.1"/>
<instance part="U$41" gate="G$1" x="17.78" y="182.88"/>
<instance part="U$42" gate="G$1" x="48.26" y="182.88"/>
<instance part="U$43" gate="G$1" x="78.74" y="182.88"/>
<instance part="U$44" gate="G$1" x="109.22" y="182.88"/>
<instance part="U$45" gate="G$1" x="139.7" y="182.88"/>
<instance part="U$46" gate="G$1" x="170.18" y="182.88"/>
<instance part="U$47" gate="G$1" x="200.66" y="182.88"/>
<instance part="U$48" gate="G$1" x="231.14" y="182.88"/>
<instance part="LED49" gate="G$1" x="33.02" y="205.74"/>
<instance part="LED50" gate="G$1" x="63.5" y="205.74"/>
<instance part="LED51" gate="G$1" x="93.98" y="205.74"/>
<instance part="LED52" gate="G$1" x="124.46" y="205.74"/>
<instance part="LED53" gate="G$1" x="154.94" y="205.74"/>
<instance part="LED54" gate="G$1" x="185.42" y="205.74"/>
<instance part="LED55" gate="G$1" x="215.9" y="205.74"/>
<instance part="LED56" gate="G$1" x="246.38" y="205.74"/>
<instance part="GND49" gate="1" x="17.78" y="195.58"/>
<instance part="GND50" gate="1" x="48.26" y="195.58"/>
<instance part="GND51" gate="1" x="78.74" y="195.58"/>
<instance part="GND52" gate="1" x="109.22" y="195.58"/>
<instance part="GND53" gate="1" x="139.7" y="195.58"/>
<instance part="GND54" gate="1" x="170.18" y="195.58"/>
<instance part="GND55" gate="1" x="200.66" y="195.58"/>
<instance part="GND56" gate="1" x="231.14" y="195.58"/>
<instance part="U$49" gate="G$1" x="17.78" y="213.36"/>
<instance part="U$50" gate="G$1" x="48.26" y="213.36"/>
<instance part="U$51" gate="G$1" x="78.74" y="213.36"/>
<instance part="U$52" gate="G$1" x="109.22" y="213.36"/>
<instance part="U$53" gate="G$1" x="139.7" y="213.36"/>
<instance part="U$54" gate="G$1" x="170.18" y="213.36"/>
<instance part="U$55" gate="G$1" x="200.66" y="213.36"/>
<instance part="U$56" gate="G$1" x="231.14" y="213.36"/>
<instance part="LED57" gate="G$1" x="33.02" y="236.22"/>
<instance part="LED58" gate="G$1" x="63.5" y="236.22"/>
<instance part="LED59" gate="G$1" x="93.98" y="236.22"/>
<instance part="LED60" gate="G$1" x="124.46" y="236.22"/>
<instance part="LED61" gate="G$1" x="154.94" y="236.22"/>
<instance part="LED62" gate="G$1" x="185.42" y="236.22"/>
<instance part="LED63" gate="G$1" x="215.9" y="236.22"/>
<instance part="LED64" gate="G$1" x="246.38" y="236.22"/>
<instance part="GND57" gate="1" x="17.78" y="226.06"/>
<instance part="GND58" gate="1" x="48.26" y="226.06"/>
<instance part="GND59" gate="1" x="78.74" y="226.06"/>
<instance part="GND60" gate="1" x="109.22" y="226.06"/>
<instance part="GND61" gate="1" x="139.7" y="226.06"/>
<instance part="GND62" gate="1" x="170.18" y="226.06"/>
<instance part="GND63" gate="1" x="200.66" y="226.06"/>
<instance part="GND64" gate="1" x="231.14" y="226.06"/>
<instance part="U$57" gate="G$1" x="17.78" y="243.84"/>
<instance part="U$58" gate="G$1" x="48.26" y="243.84"/>
<instance part="U$59" gate="G$1" x="78.74" y="243.84"/>
<instance part="U$60" gate="G$1" x="109.22" y="243.84"/>
<instance part="U$61" gate="G$1" x="139.7" y="243.84"/>
<instance part="U$62" gate="G$1" x="170.18" y="243.84"/>
<instance part="U$63" gate="G$1" x="200.66" y="243.84"/>
<instance part="U$64" gate="G$1" x="231.14" y="243.84"/>
<instance part="FRAME1" gate="G$1" x="0" y="0"/>
<instance part="CONN" gate="G$1" x="294.64" y="142.24" rot="R270"/>
<instance part="U$65" gate="G$1" x="309.88" y="144.78"/>
<instance part="GND65" gate="1" x="309.88" y="124.46"/>
<instance part="C1" gate="G$1" x="297.18" y="99.06"/>
<instance part="C2" gate="G$1" x="302.26" y="99.06"/>
<instance part="C3" gate="G$1" x="307.34" y="99.06"/>
<instance part="C4" gate="G$1" x="312.42" y="99.06"/>
<instance part="U$66" gate="G$1" x="297.18" y="111.76"/>
<instance part="GND66" gate="1" x="297.18" y="88.9"/>
</instances>
<busses>
</busses>
<nets>
<net name="N$1" class="0">
<segment>
<pinref part="LED1" gate="G$1" pin="3.DOUT"/>
<pinref part="LED2" gate="G$1" pin="4.DIN"/>
<wire x1="45.72" y1="22.86" x2="50.8" y2="22.86" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$2" class="0">
<segment>
<pinref part="LED2" gate="G$1" pin="3.DOUT"/>
<pinref part="LED3" gate="G$1" pin="4.DIN"/>
<wire x1="76.2" y1="22.86" x2="81.28" y2="22.86" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$3" class="0">
<segment>
<pinref part="LED3" gate="G$1" pin="3.DOUT"/>
<pinref part="LED4" gate="G$1" pin="4.DIN"/>
<wire x1="106.68" y1="22.86" x2="111.76" y2="22.86" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$4" class="0">
<segment>
<pinref part="LED4" gate="G$1" pin="3.DOUT"/>
<pinref part="LED5" gate="G$1" pin="4.DIN"/>
<wire x1="137.16" y1="22.86" x2="142.24" y2="22.86" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$5" class="0">
<segment>
<pinref part="LED5" gate="G$1" pin="3.DOUT"/>
<pinref part="LED6" gate="G$1" pin="4.DIN"/>
<wire x1="167.64" y1="22.86" x2="172.72" y2="22.86" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$6" class="0">
<segment>
<pinref part="LED6" gate="G$1" pin="3.DOUT"/>
<pinref part="LED7" gate="G$1" pin="4.DIN"/>
<wire x1="198.12" y1="22.86" x2="203.2" y2="22.86" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$7" class="0">
<segment>
<pinref part="LED7" gate="G$1" pin="3.DOUT"/>
<pinref part="LED8" gate="G$1" pin="4.DIN"/>
<wire x1="228.6" y1="22.86" x2="233.68" y2="22.86" width="0.1524" layer="91"/>
</segment>
</net>
<net name="GND" class="0">
<segment>
<pinref part="LED1" gate="G$1" pin="3.VSS"/>
<wire x1="20.32" y1="17.78" x2="17.78" y2="17.78" width="0.1524" layer="91"/>
<wire x1="17.78" y1="17.78" x2="17.78" y2="15.24" width="0.1524" layer="91"/>
<pinref part="GND1" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="LED2" gate="G$1" pin="3.VSS"/>
<wire x1="50.8" y1="17.78" x2="48.26" y2="17.78" width="0.1524" layer="91"/>
<wire x1="48.26" y1="17.78" x2="48.26" y2="15.24" width="0.1524" layer="91"/>
<pinref part="GND2" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="LED3" gate="G$1" pin="3.VSS"/>
<pinref part="GND3" gate="1" pin="GND"/>
<wire x1="81.28" y1="17.78" x2="78.74" y2="17.78" width="0.1524" layer="91"/>
<wire x1="78.74" y1="17.78" x2="78.74" y2="15.24" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="LED4" gate="G$1" pin="3.VSS"/>
<pinref part="GND4" gate="1" pin="GND"/>
<wire x1="111.76" y1="17.78" x2="109.22" y2="17.78" width="0.1524" layer="91"/>
<wire x1="109.22" y1="17.78" x2="109.22" y2="15.24" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="LED5" gate="G$1" pin="3.VSS"/>
<pinref part="GND5" gate="1" pin="GND"/>
<wire x1="142.24" y1="17.78" x2="139.7" y2="17.78" width="0.1524" layer="91"/>
<wire x1="139.7" y1="17.78" x2="139.7" y2="15.24" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="LED6" gate="G$1" pin="3.VSS"/>
<pinref part="GND6" gate="1" pin="GND"/>
<wire x1="172.72" y1="17.78" x2="170.18" y2="17.78" width="0.1524" layer="91"/>
<wire x1="170.18" y1="17.78" x2="170.18" y2="15.24" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="LED7" gate="G$1" pin="3.VSS"/>
<pinref part="GND7" gate="1" pin="GND"/>
<wire x1="203.2" y1="17.78" x2="200.66" y2="17.78" width="0.1524" layer="91"/>
<wire x1="200.66" y1="17.78" x2="200.66" y2="15.24" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="LED8" gate="G$1" pin="3.VSS"/>
<pinref part="GND8" gate="1" pin="GND"/>
<wire x1="233.68" y1="17.78" x2="231.14" y2="17.78" width="0.1524" layer="91"/>
<wire x1="231.14" y1="17.78" x2="231.14" y2="15.24" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="LED9" gate="G$1" pin="3.VSS"/>
<wire x1="20.32" y1="48.26" x2="17.78" y2="48.26" width="0.1524" layer="91"/>
<wire x1="17.78" y1="48.26" x2="17.78" y2="45.72" width="0.1524" layer="91"/>
<pinref part="GND9" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="LED10" gate="G$1" pin="3.VSS"/>
<wire x1="50.8" y1="48.26" x2="48.26" y2="48.26" width="0.1524" layer="91"/>
<wire x1="48.26" y1="48.26" x2="48.26" y2="45.72" width="0.1524" layer="91"/>
<pinref part="GND10" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="LED11" gate="G$1" pin="3.VSS"/>
<pinref part="GND11" gate="1" pin="GND"/>
<wire x1="81.28" y1="48.26" x2="78.74" y2="48.26" width="0.1524" layer="91"/>
<wire x1="78.74" y1="48.26" x2="78.74" y2="45.72" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="LED12" gate="G$1" pin="3.VSS"/>
<pinref part="GND12" gate="1" pin="GND"/>
<wire x1="111.76" y1="48.26" x2="109.22" y2="48.26" width="0.1524" layer="91"/>
<wire x1="109.22" y1="48.26" x2="109.22" y2="45.72" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="LED13" gate="G$1" pin="3.VSS"/>
<pinref part="GND13" gate="1" pin="GND"/>
<wire x1="142.24" y1="48.26" x2="139.7" y2="48.26" width="0.1524" layer="91"/>
<wire x1="139.7" y1="48.26" x2="139.7" y2="45.72" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="LED14" gate="G$1" pin="3.VSS"/>
<pinref part="GND14" gate="1" pin="GND"/>
<wire x1="172.72" y1="48.26" x2="170.18" y2="48.26" width="0.1524" layer="91"/>
<wire x1="170.18" y1="48.26" x2="170.18" y2="45.72" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="LED15" gate="G$1" pin="3.VSS"/>
<pinref part="GND15" gate="1" pin="GND"/>
<wire x1="203.2" y1="48.26" x2="200.66" y2="48.26" width="0.1524" layer="91"/>
<wire x1="200.66" y1="48.26" x2="200.66" y2="45.72" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="LED16" gate="G$1" pin="3.VSS"/>
<pinref part="GND16" gate="1" pin="GND"/>
<wire x1="233.68" y1="48.26" x2="231.14" y2="48.26" width="0.1524" layer="91"/>
<wire x1="231.14" y1="48.26" x2="231.14" y2="45.72" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="LED17" gate="G$1" pin="3.VSS"/>
<wire x1="20.32" y1="78.74" x2="17.78" y2="78.74" width="0.1524" layer="91"/>
<wire x1="17.78" y1="78.74" x2="17.78" y2="76.2" width="0.1524" layer="91"/>
<pinref part="GND17" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="LED18" gate="G$1" pin="3.VSS"/>
<wire x1="50.8" y1="78.74" x2="48.26" y2="78.74" width="0.1524" layer="91"/>
<wire x1="48.26" y1="78.74" x2="48.26" y2="76.2" width="0.1524" layer="91"/>
<pinref part="GND18" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="LED19" gate="G$1" pin="3.VSS"/>
<pinref part="GND19" gate="1" pin="GND"/>
<wire x1="81.28" y1="78.74" x2="78.74" y2="78.74" width="0.1524" layer="91"/>
<wire x1="78.74" y1="78.74" x2="78.74" y2="76.2" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="LED20" gate="G$1" pin="3.VSS"/>
<pinref part="GND20" gate="1" pin="GND"/>
<wire x1="111.76" y1="78.74" x2="109.22" y2="78.74" width="0.1524" layer="91"/>
<wire x1="109.22" y1="78.74" x2="109.22" y2="76.2" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="LED21" gate="G$1" pin="3.VSS"/>
<pinref part="GND21" gate="1" pin="GND"/>
<wire x1="142.24" y1="78.74" x2="139.7" y2="78.74" width="0.1524" layer="91"/>
<wire x1="139.7" y1="78.74" x2="139.7" y2="76.2" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="LED22" gate="G$1" pin="3.VSS"/>
<pinref part="GND22" gate="1" pin="GND"/>
<wire x1="172.72" y1="78.74" x2="170.18" y2="78.74" width="0.1524" layer="91"/>
<wire x1="170.18" y1="78.74" x2="170.18" y2="76.2" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="LED23" gate="G$1" pin="3.VSS"/>
<pinref part="GND23" gate="1" pin="GND"/>
<wire x1="203.2" y1="78.74" x2="200.66" y2="78.74" width="0.1524" layer="91"/>
<wire x1="200.66" y1="78.74" x2="200.66" y2="76.2" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="LED24" gate="G$1" pin="3.VSS"/>
<pinref part="GND24" gate="1" pin="GND"/>
<wire x1="233.68" y1="78.74" x2="231.14" y2="78.74" width="0.1524" layer="91"/>
<wire x1="231.14" y1="78.74" x2="231.14" y2="76.2" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="LED25" gate="G$1" pin="3.VSS"/>
<wire x1="20.32" y1="109.22" x2="17.78" y2="109.22" width="0.1524" layer="91"/>
<wire x1="17.78" y1="109.22" x2="17.78" y2="106.68" width="0.1524" layer="91"/>
<pinref part="GND25" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="LED26" gate="G$1" pin="3.VSS"/>
<wire x1="50.8" y1="109.22" x2="48.26" y2="109.22" width="0.1524" layer="91"/>
<wire x1="48.26" y1="109.22" x2="48.26" y2="106.68" width="0.1524" layer="91"/>
<pinref part="GND26" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="LED27" gate="G$1" pin="3.VSS"/>
<pinref part="GND27" gate="1" pin="GND"/>
<wire x1="81.28" y1="109.22" x2="78.74" y2="109.22" width="0.1524" layer="91"/>
<wire x1="78.74" y1="109.22" x2="78.74" y2="106.68" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="LED28" gate="G$1" pin="3.VSS"/>
<pinref part="GND28" gate="1" pin="GND"/>
<wire x1="111.76" y1="109.22" x2="109.22" y2="109.22" width="0.1524" layer="91"/>
<wire x1="109.22" y1="109.22" x2="109.22" y2="106.68" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="LED29" gate="G$1" pin="3.VSS"/>
<pinref part="GND29" gate="1" pin="GND"/>
<wire x1="142.24" y1="109.22" x2="139.7" y2="109.22" width="0.1524" layer="91"/>
<wire x1="139.7" y1="109.22" x2="139.7" y2="106.68" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="LED30" gate="G$1" pin="3.VSS"/>
<pinref part="GND30" gate="1" pin="GND"/>
<wire x1="172.72" y1="109.22" x2="170.18" y2="109.22" width="0.1524" layer="91"/>
<wire x1="170.18" y1="109.22" x2="170.18" y2="106.68" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="LED31" gate="G$1" pin="3.VSS"/>
<pinref part="GND31" gate="1" pin="GND"/>
<wire x1="203.2" y1="109.22" x2="200.66" y2="109.22" width="0.1524" layer="91"/>
<wire x1="200.66" y1="109.22" x2="200.66" y2="106.68" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="LED32" gate="G$1" pin="3.VSS"/>
<pinref part="GND32" gate="1" pin="GND"/>
<wire x1="233.68" y1="109.22" x2="231.14" y2="109.22" width="0.1524" layer="91"/>
<wire x1="231.14" y1="109.22" x2="231.14" y2="106.68" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="LED33" gate="G$1" pin="3.VSS"/>
<wire x1="20.32" y1="139.7" x2="17.78" y2="139.7" width="0.1524" layer="91"/>
<wire x1="17.78" y1="139.7" x2="17.78" y2="137.16" width="0.1524" layer="91"/>
<pinref part="GND33" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="LED34" gate="G$1" pin="3.VSS"/>
<wire x1="50.8" y1="139.7" x2="48.26" y2="139.7" width="0.1524" layer="91"/>
<wire x1="48.26" y1="139.7" x2="48.26" y2="137.16" width="0.1524" layer="91"/>
<pinref part="GND34" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="LED35" gate="G$1" pin="3.VSS"/>
<pinref part="GND35" gate="1" pin="GND"/>
<wire x1="81.28" y1="139.7" x2="78.74" y2="139.7" width="0.1524" layer="91"/>
<wire x1="78.74" y1="139.7" x2="78.74" y2="137.16" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="LED36" gate="G$1" pin="3.VSS"/>
<pinref part="GND36" gate="1" pin="GND"/>
<wire x1="111.76" y1="139.7" x2="109.22" y2="139.7" width="0.1524" layer="91"/>
<wire x1="109.22" y1="139.7" x2="109.22" y2="137.16" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="LED37" gate="G$1" pin="3.VSS"/>
<pinref part="GND37" gate="1" pin="GND"/>
<wire x1="142.24" y1="139.7" x2="139.7" y2="139.7" width="0.1524" layer="91"/>
<wire x1="139.7" y1="139.7" x2="139.7" y2="137.16" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="LED38" gate="G$1" pin="3.VSS"/>
<pinref part="GND38" gate="1" pin="GND"/>
<wire x1="172.72" y1="139.7" x2="170.18" y2="139.7" width="0.1524" layer="91"/>
<wire x1="170.18" y1="139.7" x2="170.18" y2="137.16" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="LED39" gate="G$1" pin="3.VSS"/>
<pinref part="GND39" gate="1" pin="GND"/>
<wire x1="203.2" y1="139.7" x2="200.66" y2="139.7" width="0.1524" layer="91"/>
<wire x1="200.66" y1="139.7" x2="200.66" y2="137.16" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="LED40" gate="G$1" pin="3.VSS"/>
<pinref part="GND40" gate="1" pin="GND"/>
<wire x1="233.68" y1="139.7" x2="231.14" y2="139.7" width="0.1524" layer="91"/>
<wire x1="231.14" y1="139.7" x2="231.14" y2="137.16" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="LED41" gate="G$1" pin="3.VSS"/>
<wire x1="20.32" y1="170.18" x2="17.78" y2="170.18" width="0.1524" layer="91"/>
<wire x1="17.78" y1="170.18" x2="17.78" y2="167.64" width="0.1524" layer="91"/>
<pinref part="GND41" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="LED42" gate="G$1" pin="3.VSS"/>
<wire x1="50.8" y1="170.18" x2="48.26" y2="170.18" width="0.1524" layer="91"/>
<wire x1="48.26" y1="170.18" x2="48.26" y2="167.64" width="0.1524" layer="91"/>
<pinref part="GND42" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="LED43" gate="G$1" pin="3.VSS"/>
<pinref part="GND43" gate="1" pin="GND"/>
<wire x1="81.28" y1="170.18" x2="78.74" y2="170.18" width="0.1524" layer="91"/>
<wire x1="78.74" y1="170.18" x2="78.74" y2="167.64" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="LED44" gate="G$1" pin="3.VSS"/>
<pinref part="GND44" gate="1" pin="GND"/>
<wire x1="111.76" y1="170.18" x2="109.22" y2="170.18" width="0.1524" layer="91"/>
<wire x1="109.22" y1="170.18" x2="109.22" y2="167.64" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="LED45" gate="G$1" pin="3.VSS"/>
<pinref part="GND45" gate="1" pin="GND"/>
<wire x1="142.24" y1="170.18" x2="139.7" y2="170.18" width="0.1524" layer="91"/>
<wire x1="139.7" y1="170.18" x2="139.7" y2="167.64" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="LED46" gate="G$1" pin="3.VSS"/>
<pinref part="GND46" gate="1" pin="GND"/>
<wire x1="172.72" y1="170.18" x2="170.18" y2="170.18" width="0.1524" layer="91"/>
<wire x1="170.18" y1="170.18" x2="170.18" y2="167.64" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="LED47" gate="G$1" pin="3.VSS"/>
<pinref part="GND47" gate="1" pin="GND"/>
<wire x1="203.2" y1="170.18" x2="200.66" y2="170.18" width="0.1524" layer="91"/>
<wire x1="200.66" y1="170.18" x2="200.66" y2="167.64" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="LED48" gate="G$1" pin="3.VSS"/>
<pinref part="GND48" gate="1" pin="GND"/>
<wire x1="233.68" y1="170.18" x2="231.14" y2="170.18" width="0.1524" layer="91"/>
<wire x1="231.14" y1="170.18" x2="231.14" y2="167.64" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="LED49" gate="G$1" pin="3.VSS"/>
<wire x1="20.32" y1="200.66" x2="17.78" y2="200.66" width="0.1524" layer="91"/>
<wire x1="17.78" y1="200.66" x2="17.78" y2="198.12" width="0.1524" layer="91"/>
<pinref part="GND49" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="LED50" gate="G$1" pin="3.VSS"/>
<wire x1="50.8" y1="200.66" x2="48.26" y2="200.66" width="0.1524" layer="91"/>
<wire x1="48.26" y1="200.66" x2="48.26" y2="198.12" width="0.1524" layer="91"/>
<pinref part="GND50" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="LED51" gate="G$1" pin="3.VSS"/>
<pinref part="GND51" gate="1" pin="GND"/>
<wire x1="81.28" y1="200.66" x2="78.74" y2="200.66" width="0.1524" layer="91"/>
<wire x1="78.74" y1="200.66" x2="78.74" y2="198.12" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="LED52" gate="G$1" pin="3.VSS"/>
<pinref part="GND52" gate="1" pin="GND"/>
<wire x1="111.76" y1="200.66" x2="109.22" y2="200.66" width="0.1524" layer="91"/>
<wire x1="109.22" y1="200.66" x2="109.22" y2="198.12" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="LED53" gate="G$1" pin="3.VSS"/>
<pinref part="GND53" gate="1" pin="GND"/>
<wire x1="142.24" y1="200.66" x2="139.7" y2="200.66" width="0.1524" layer="91"/>
<wire x1="139.7" y1="200.66" x2="139.7" y2="198.12" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="LED54" gate="G$1" pin="3.VSS"/>
<pinref part="GND54" gate="1" pin="GND"/>
<wire x1="172.72" y1="200.66" x2="170.18" y2="200.66" width="0.1524" layer="91"/>
<wire x1="170.18" y1="200.66" x2="170.18" y2="198.12" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="LED55" gate="G$1" pin="3.VSS"/>
<pinref part="GND55" gate="1" pin="GND"/>
<wire x1="203.2" y1="200.66" x2="200.66" y2="200.66" width="0.1524" layer="91"/>
<wire x1="200.66" y1="200.66" x2="200.66" y2="198.12" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="LED56" gate="G$1" pin="3.VSS"/>
<pinref part="GND56" gate="1" pin="GND"/>
<wire x1="233.68" y1="200.66" x2="231.14" y2="200.66" width="0.1524" layer="91"/>
<wire x1="231.14" y1="200.66" x2="231.14" y2="198.12" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="LED57" gate="G$1" pin="3.VSS"/>
<wire x1="20.32" y1="231.14" x2="17.78" y2="231.14" width="0.1524" layer="91"/>
<wire x1="17.78" y1="231.14" x2="17.78" y2="228.6" width="0.1524" layer="91"/>
<pinref part="GND57" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="LED58" gate="G$1" pin="3.VSS"/>
<wire x1="50.8" y1="231.14" x2="48.26" y2="231.14" width="0.1524" layer="91"/>
<wire x1="48.26" y1="231.14" x2="48.26" y2="228.6" width="0.1524" layer="91"/>
<pinref part="GND58" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="LED59" gate="G$1" pin="3.VSS"/>
<pinref part="GND59" gate="1" pin="GND"/>
<wire x1="81.28" y1="231.14" x2="78.74" y2="231.14" width="0.1524" layer="91"/>
<wire x1="78.74" y1="231.14" x2="78.74" y2="228.6" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="LED60" gate="G$1" pin="3.VSS"/>
<pinref part="GND60" gate="1" pin="GND"/>
<wire x1="111.76" y1="231.14" x2="109.22" y2="231.14" width="0.1524" layer="91"/>
<wire x1="109.22" y1="231.14" x2="109.22" y2="228.6" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="LED61" gate="G$1" pin="3.VSS"/>
<pinref part="GND61" gate="1" pin="GND"/>
<wire x1="142.24" y1="231.14" x2="139.7" y2="231.14" width="0.1524" layer="91"/>
<wire x1="139.7" y1="231.14" x2="139.7" y2="228.6" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="LED62" gate="G$1" pin="3.VSS"/>
<pinref part="GND62" gate="1" pin="GND"/>
<wire x1="172.72" y1="231.14" x2="170.18" y2="231.14" width="0.1524" layer="91"/>
<wire x1="170.18" y1="231.14" x2="170.18" y2="228.6" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="LED63" gate="G$1" pin="3.VSS"/>
<pinref part="GND63" gate="1" pin="GND"/>
<wire x1="203.2" y1="231.14" x2="200.66" y2="231.14" width="0.1524" layer="91"/>
<wire x1="200.66" y1="231.14" x2="200.66" y2="228.6" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="LED64" gate="G$1" pin="3.VSS"/>
<pinref part="GND64" gate="1" pin="GND"/>
<wire x1="233.68" y1="231.14" x2="231.14" y2="231.14" width="0.1524" layer="91"/>
<wire x1="231.14" y1="231.14" x2="231.14" y2="228.6" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="CONN" gate="G$1" pin="P$4"/>
<pinref part="GND65" gate="1" pin="GND"/>
<wire x1="302.26" y1="132.08" x2="309.88" y2="132.08" width="0.1524" layer="91"/>
<wire x1="309.88" y1="132.08" x2="309.88" y2="127" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND66" gate="1" pin="GND"/>
<pinref part="C1" gate="G$1" pin="2"/>
<wire x1="297.18" y1="91.44" x2="297.18" y2="93.98" width="0.1524" layer="91"/>
<pinref part="C2" gate="G$1" pin="2"/>
<wire x1="297.18" y1="93.98" x2="297.18" y2="96.52" width="0.1524" layer="91"/>
<wire x1="297.18" y1="93.98" x2="302.26" y2="93.98" width="0.1524" layer="91"/>
<wire x1="302.26" y1="93.98" x2="302.26" y2="96.52" width="0.1524" layer="91"/>
<junction x="297.18" y="93.98"/>
<pinref part="C3" gate="G$1" pin="2"/>
<wire x1="302.26" y1="93.98" x2="307.34" y2="93.98" width="0.1524" layer="91"/>
<wire x1="307.34" y1="93.98" x2="307.34" y2="96.52" width="0.1524" layer="91"/>
<junction x="302.26" y="93.98"/>
<pinref part="C4" gate="G$1" pin="2"/>
<wire x1="307.34" y1="93.98" x2="312.42" y2="93.98" width="0.1524" layer="91"/>
<wire x1="312.42" y1="93.98" x2="312.42" y2="96.52" width="0.1524" layer="91"/>
<junction x="307.34" y="93.98"/>
</segment>
</net>
<net name="+VBATT" class="0">
<segment>
<pinref part="LED1" gate="G$1" pin="1.VDD"/>
<wire x1="20.32" y1="27.94" x2="17.78" y2="27.94" width="0.1524" layer="91"/>
<wire x1="17.78" y1="27.94" x2="17.78" y2="30.48" width="0.1524" layer="91"/>
<pinref part="U$9" gate="G$1" pin="+VBATT"/>
</segment>
<segment>
<pinref part="LED2" gate="G$1" pin="1.VDD"/>
<pinref part="U$10" gate="G$1" pin="+VBATT"/>
<wire x1="50.8" y1="27.94" x2="48.26" y2="27.94" width="0.1524" layer="91"/>
<wire x1="48.26" y1="27.94" x2="48.26" y2="30.48" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="LED3" gate="G$1" pin="1.VDD"/>
<pinref part="U$11" gate="G$1" pin="+VBATT"/>
<wire x1="81.28" y1="27.94" x2="78.74" y2="27.94" width="0.1524" layer="91"/>
<wire x1="78.74" y1="27.94" x2="78.74" y2="30.48" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="LED4" gate="G$1" pin="1.VDD"/>
<pinref part="U$12" gate="G$1" pin="+VBATT"/>
<wire x1="111.76" y1="27.94" x2="109.22" y2="27.94" width="0.1524" layer="91"/>
<wire x1="109.22" y1="27.94" x2="109.22" y2="30.48" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="LED5" gate="G$1" pin="1.VDD"/>
<pinref part="U$13" gate="G$1" pin="+VBATT"/>
<wire x1="142.24" y1="27.94" x2="139.7" y2="27.94" width="0.1524" layer="91"/>
<wire x1="139.7" y1="27.94" x2="139.7" y2="30.48" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="LED6" gate="G$1" pin="1.VDD"/>
<wire x1="172.72" y1="27.94" x2="170.18" y2="27.94" width="0.1524" layer="91"/>
<pinref part="U$14" gate="G$1" pin="+VBATT"/>
<wire x1="170.18" y1="27.94" x2="170.18" y2="30.48" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="LED7" gate="G$1" pin="1.VDD"/>
<pinref part="U$15" gate="G$1" pin="+VBATT"/>
<wire x1="203.2" y1="27.94" x2="200.66" y2="27.94" width="0.1524" layer="91"/>
<wire x1="200.66" y1="27.94" x2="200.66" y2="30.48" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="LED8" gate="G$1" pin="1.VDD"/>
<pinref part="U$16" gate="G$1" pin="+VBATT"/>
<wire x1="233.68" y1="27.94" x2="231.14" y2="27.94" width="0.1524" layer="91"/>
<wire x1="231.14" y1="27.94" x2="231.14" y2="30.48" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="LED9" gate="G$1" pin="1.VDD"/>
<wire x1="20.32" y1="58.42" x2="17.78" y2="58.42" width="0.1524" layer="91"/>
<wire x1="17.78" y1="58.42" x2="17.78" y2="60.96" width="0.1524" layer="91"/>
<pinref part="U$1" gate="G$1" pin="+VBATT"/>
</segment>
<segment>
<pinref part="LED10" gate="G$1" pin="1.VDD"/>
<pinref part="U$2" gate="G$1" pin="+VBATT"/>
<wire x1="50.8" y1="58.42" x2="48.26" y2="58.42" width="0.1524" layer="91"/>
<wire x1="48.26" y1="58.42" x2="48.26" y2="60.96" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="LED11" gate="G$1" pin="1.VDD"/>
<pinref part="U$3" gate="G$1" pin="+VBATT"/>
<wire x1="81.28" y1="58.42" x2="78.74" y2="58.42" width="0.1524" layer="91"/>
<wire x1="78.74" y1="58.42" x2="78.74" y2="60.96" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="LED12" gate="G$1" pin="1.VDD"/>
<pinref part="U$4" gate="G$1" pin="+VBATT"/>
<wire x1="111.76" y1="58.42" x2="109.22" y2="58.42" width="0.1524" layer="91"/>
<wire x1="109.22" y1="58.42" x2="109.22" y2="60.96" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="LED13" gate="G$1" pin="1.VDD"/>
<pinref part="U$5" gate="G$1" pin="+VBATT"/>
<wire x1="142.24" y1="58.42" x2="139.7" y2="58.42" width="0.1524" layer="91"/>
<wire x1="139.7" y1="58.42" x2="139.7" y2="60.96" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="LED14" gate="G$1" pin="1.VDD"/>
<wire x1="172.72" y1="58.42" x2="170.18" y2="58.42" width="0.1524" layer="91"/>
<pinref part="U$6" gate="G$1" pin="+VBATT"/>
<wire x1="170.18" y1="58.42" x2="170.18" y2="60.96" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="LED15" gate="G$1" pin="1.VDD"/>
<pinref part="U$7" gate="G$1" pin="+VBATT"/>
<wire x1="203.2" y1="58.42" x2="200.66" y2="58.42" width="0.1524" layer="91"/>
<wire x1="200.66" y1="58.42" x2="200.66" y2="60.96" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="LED16" gate="G$1" pin="1.VDD"/>
<pinref part="U$8" gate="G$1" pin="+VBATT"/>
<wire x1="233.68" y1="58.42" x2="231.14" y2="58.42" width="0.1524" layer="91"/>
<wire x1="231.14" y1="58.42" x2="231.14" y2="60.96" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="LED17" gate="G$1" pin="1.VDD"/>
<wire x1="20.32" y1="88.9" x2="17.78" y2="88.9" width="0.1524" layer="91"/>
<wire x1="17.78" y1="88.9" x2="17.78" y2="91.44" width="0.1524" layer="91"/>
<pinref part="U$17" gate="G$1" pin="+VBATT"/>
</segment>
<segment>
<pinref part="LED18" gate="G$1" pin="1.VDD"/>
<pinref part="U$18" gate="G$1" pin="+VBATT"/>
<wire x1="50.8" y1="88.9" x2="48.26" y2="88.9" width="0.1524" layer="91"/>
<wire x1="48.26" y1="88.9" x2="48.26" y2="91.44" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="LED19" gate="G$1" pin="1.VDD"/>
<pinref part="U$19" gate="G$1" pin="+VBATT"/>
<wire x1="81.28" y1="88.9" x2="78.74" y2="88.9" width="0.1524" layer="91"/>
<wire x1="78.74" y1="88.9" x2="78.74" y2="91.44" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="LED20" gate="G$1" pin="1.VDD"/>
<pinref part="U$20" gate="G$1" pin="+VBATT"/>
<wire x1="111.76" y1="88.9" x2="109.22" y2="88.9" width="0.1524" layer="91"/>
<wire x1="109.22" y1="88.9" x2="109.22" y2="91.44" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="LED21" gate="G$1" pin="1.VDD"/>
<pinref part="U$21" gate="G$1" pin="+VBATT"/>
<wire x1="142.24" y1="88.9" x2="139.7" y2="88.9" width="0.1524" layer="91"/>
<wire x1="139.7" y1="88.9" x2="139.7" y2="91.44" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="LED22" gate="G$1" pin="1.VDD"/>
<wire x1="172.72" y1="88.9" x2="170.18" y2="88.9" width="0.1524" layer="91"/>
<pinref part="U$22" gate="G$1" pin="+VBATT"/>
<wire x1="170.18" y1="88.9" x2="170.18" y2="91.44" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="LED23" gate="G$1" pin="1.VDD"/>
<pinref part="U$23" gate="G$1" pin="+VBATT"/>
<wire x1="203.2" y1="88.9" x2="200.66" y2="88.9" width="0.1524" layer="91"/>
<wire x1="200.66" y1="88.9" x2="200.66" y2="91.44" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="LED24" gate="G$1" pin="1.VDD"/>
<pinref part="U$24" gate="G$1" pin="+VBATT"/>
<wire x1="233.68" y1="88.9" x2="231.14" y2="88.9" width="0.1524" layer="91"/>
<wire x1="231.14" y1="88.9" x2="231.14" y2="91.44" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="LED25" gate="G$1" pin="1.VDD"/>
<wire x1="20.32" y1="119.38" x2="17.78" y2="119.38" width="0.1524" layer="91"/>
<wire x1="17.78" y1="119.38" x2="17.78" y2="121.92" width="0.1524" layer="91"/>
<pinref part="U$25" gate="G$1" pin="+VBATT"/>
</segment>
<segment>
<pinref part="LED26" gate="G$1" pin="1.VDD"/>
<pinref part="U$26" gate="G$1" pin="+VBATT"/>
<wire x1="50.8" y1="119.38" x2="48.26" y2="119.38" width="0.1524" layer="91"/>
<wire x1="48.26" y1="119.38" x2="48.26" y2="121.92" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="LED27" gate="G$1" pin="1.VDD"/>
<pinref part="U$27" gate="G$1" pin="+VBATT"/>
<wire x1="81.28" y1="119.38" x2="78.74" y2="119.38" width="0.1524" layer="91"/>
<wire x1="78.74" y1="119.38" x2="78.74" y2="121.92" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="LED28" gate="G$1" pin="1.VDD"/>
<pinref part="U$28" gate="G$1" pin="+VBATT"/>
<wire x1="111.76" y1="119.38" x2="109.22" y2="119.38" width="0.1524" layer="91"/>
<wire x1="109.22" y1="119.38" x2="109.22" y2="121.92" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="LED29" gate="G$1" pin="1.VDD"/>
<pinref part="U$29" gate="G$1" pin="+VBATT"/>
<wire x1="142.24" y1="119.38" x2="139.7" y2="119.38" width="0.1524" layer="91"/>
<wire x1="139.7" y1="119.38" x2="139.7" y2="121.92" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="LED30" gate="G$1" pin="1.VDD"/>
<wire x1="172.72" y1="119.38" x2="170.18" y2="119.38" width="0.1524" layer="91"/>
<pinref part="U$30" gate="G$1" pin="+VBATT"/>
<wire x1="170.18" y1="119.38" x2="170.18" y2="121.92" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="LED31" gate="G$1" pin="1.VDD"/>
<pinref part="U$31" gate="G$1" pin="+VBATT"/>
<wire x1="203.2" y1="119.38" x2="200.66" y2="119.38" width="0.1524" layer="91"/>
<wire x1="200.66" y1="119.38" x2="200.66" y2="121.92" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="LED32" gate="G$1" pin="1.VDD"/>
<pinref part="U$32" gate="G$1" pin="+VBATT"/>
<wire x1="233.68" y1="119.38" x2="231.14" y2="119.38" width="0.1524" layer="91"/>
<wire x1="231.14" y1="119.38" x2="231.14" y2="121.92" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="LED33" gate="G$1" pin="1.VDD"/>
<wire x1="20.32" y1="149.86" x2="17.78" y2="149.86" width="0.1524" layer="91"/>
<wire x1="17.78" y1="149.86" x2="17.78" y2="152.4" width="0.1524" layer="91"/>
<pinref part="U$33" gate="G$1" pin="+VBATT"/>
</segment>
<segment>
<pinref part="LED34" gate="G$1" pin="1.VDD"/>
<pinref part="U$34" gate="G$1" pin="+VBATT"/>
<wire x1="50.8" y1="149.86" x2="48.26" y2="149.86" width="0.1524" layer="91"/>
<wire x1="48.26" y1="149.86" x2="48.26" y2="152.4" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="LED35" gate="G$1" pin="1.VDD"/>
<pinref part="U$35" gate="G$1" pin="+VBATT"/>
<wire x1="81.28" y1="149.86" x2="78.74" y2="149.86" width="0.1524" layer="91"/>
<wire x1="78.74" y1="149.86" x2="78.74" y2="152.4" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="LED36" gate="G$1" pin="1.VDD"/>
<pinref part="U$36" gate="G$1" pin="+VBATT"/>
<wire x1="111.76" y1="149.86" x2="109.22" y2="149.86" width="0.1524" layer="91"/>
<wire x1="109.22" y1="149.86" x2="109.22" y2="152.4" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="LED37" gate="G$1" pin="1.VDD"/>
<pinref part="U$37" gate="G$1" pin="+VBATT"/>
<wire x1="142.24" y1="149.86" x2="139.7" y2="149.86" width="0.1524" layer="91"/>
<wire x1="139.7" y1="149.86" x2="139.7" y2="152.4" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="LED38" gate="G$1" pin="1.VDD"/>
<wire x1="172.72" y1="149.86" x2="170.18" y2="149.86" width="0.1524" layer="91"/>
<pinref part="U$38" gate="G$1" pin="+VBATT"/>
<wire x1="170.18" y1="149.86" x2="170.18" y2="152.4" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="LED39" gate="G$1" pin="1.VDD"/>
<pinref part="U$39" gate="G$1" pin="+VBATT"/>
<wire x1="203.2" y1="149.86" x2="200.66" y2="149.86" width="0.1524" layer="91"/>
<wire x1="200.66" y1="149.86" x2="200.66" y2="152.4" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="LED40" gate="G$1" pin="1.VDD"/>
<pinref part="U$40" gate="G$1" pin="+VBATT"/>
<wire x1="233.68" y1="149.86" x2="231.14" y2="149.86" width="0.1524" layer="91"/>
<wire x1="231.14" y1="149.86" x2="231.14" y2="152.4" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="LED41" gate="G$1" pin="1.VDD"/>
<wire x1="20.32" y1="180.34" x2="17.78" y2="180.34" width="0.1524" layer="91"/>
<wire x1="17.78" y1="180.34" x2="17.78" y2="182.88" width="0.1524" layer="91"/>
<pinref part="U$41" gate="G$1" pin="+VBATT"/>
</segment>
<segment>
<pinref part="LED42" gate="G$1" pin="1.VDD"/>
<pinref part="U$42" gate="G$1" pin="+VBATT"/>
<wire x1="50.8" y1="180.34" x2="48.26" y2="180.34" width="0.1524" layer="91"/>
<wire x1="48.26" y1="180.34" x2="48.26" y2="182.88" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="LED43" gate="G$1" pin="1.VDD"/>
<pinref part="U$43" gate="G$1" pin="+VBATT"/>
<wire x1="81.28" y1="180.34" x2="78.74" y2="180.34" width="0.1524" layer="91"/>
<wire x1="78.74" y1="180.34" x2="78.74" y2="182.88" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="LED44" gate="G$1" pin="1.VDD"/>
<pinref part="U$44" gate="G$1" pin="+VBATT"/>
<wire x1="111.76" y1="180.34" x2="109.22" y2="180.34" width="0.1524" layer="91"/>
<wire x1="109.22" y1="180.34" x2="109.22" y2="182.88" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="LED45" gate="G$1" pin="1.VDD"/>
<pinref part="U$45" gate="G$1" pin="+VBATT"/>
<wire x1="142.24" y1="180.34" x2="139.7" y2="180.34" width="0.1524" layer="91"/>
<wire x1="139.7" y1="180.34" x2="139.7" y2="182.88" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="LED46" gate="G$1" pin="1.VDD"/>
<wire x1="172.72" y1="180.34" x2="170.18" y2="180.34" width="0.1524" layer="91"/>
<pinref part="U$46" gate="G$1" pin="+VBATT"/>
<wire x1="170.18" y1="180.34" x2="170.18" y2="182.88" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="LED47" gate="G$1" pin="1.VDD"/>
<pinref part="U$47" gate="G$1" pin="+VBATT"/>
<wire x1="203.2" y1="180.34" x2="200.66" y2="180.34" width="0.1524" layer="91"/>
<wire x1="200.66" y1="180.34" x2="200.66" y2="182.88" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="LED48" gate="G$1" pin="1.VDD"/>
<pinref part="U$48" gate="G$1" pin="+VBATT"/>
<wire x1="233.68" y1="180.34" x2="231.14" y2="180.34" width="0.1524" layer="91"/>
<wire x1="231.14" y1="180.34" x2="231.14" y2="182.88" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="LED49" gate="G$1" pin="1.VDD"/>
<wire x1="20.32" y1="210.82" x2="17.78" y2="210.82" width="0.1524" layer="91"/>
<wire x1="17.78" y1="210.82" x2="17.78" y2="213.36" width="0.1524" layer="91"/>
<pinref part="U$49" gate="G$1" pin="+VBATT"/>
</segment>
<segment>
<pinref part="LED50" gate="G$1" pin="1.VDD"/>
<pinref part="U$50" gate="G$1" pin="+VBATT"/>
<wire x1="50.8" y1="210.82" x2="48.26" y2="210.82" width="0.1524" layer="91"/>
<wire x1="48.26" y1="210.82" x2="48.26" y2="213.36" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="LED51" gate="G$1" pin="1.VDD"/>
<pinref part="U$51" gate="G$1" pin="+VBATT"/>
<wire x1="81.28" y1="210.82" x2="78.74" y2="210.82" width="0.1524" layer="91"/>
<wire x1="78.74" y1="210.82" x2="78.74" y2="213.36" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="LED52" gate="G$1" pin="1.VDD"/>
<pinref part="U$52" gate="G$1" pin="+VBATT"/>
<wire x1="111.76" y1="210.82" x2="109.22" y2="210.82" width="0.1524" layer="91"/>
<wire x1="109.22" y1="210.82" x2="109.22" y2="213.36" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="LED53" gate="G$1" pin="1.VDD"/>
<pinref part="U$53" gate="G$1" pin="+VBATT"/>
<wire x1="142.24" y1="210.82" x2="139.7" y2="210.82" width="0.1524" layer="91"/>
<wire x1="139.7" y1="210.82" x2="139.7" y2="213.36" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="LED54" gate="G$1" pin="1.VDD"/>
<wire x1="172.72" y1="210.82" x2="170.18" y2="210.82" width="0.1524" layer="91"/>
<pinref part="U$54" gate="G$1" pin="+VBATT"/>
<wire x1="170.18" y1="210.82" x2="170.18" y2="213.36" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="LED55" gate="G$1" pin="1.VDD"/>
<pinref part="U$55" gate="G$1" pin="+VBATT"/>
<wire x1="203.2" y1="210.82" x2="200.66" y2="210.82" width="0.1524" layer="91"/>
<wire x1="200.66" y1="210.82" x2="200.66" y2="213.36" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="LED56" gate="G$1" pin="1.VDD"/>
<pinref part="U$56" gate="G$1" pin="+VBATT"/>
<wire x1="233.68" y1="210.82" x2="231.14" y2="210.82" width="0.1524" layer="91"/>
<wire x1="231.14" y1="210.82" x2="231.14" y2="213.36" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="LED57" gate="G$1" pin="1.VDD"/>
<wire x1="20.32" y1="241.3" x2="17.78" y2="241.3" width="0.1524" layer="91"/>
<wire x1="17.78" y1="241.3" x2="17.78" y2="243.84" width="0.1524" layer="91"/>
<pinref part="U$57" gate="G$1" pin="+VBATT"/>
</segment>
<segment>
<pinref part="LED58" gate="G$1" pin="1.VDD"/>
<pinref part="U$58" gate="G$1" pin="+VBATT"/>
<wire x1="50.8" y1="241.3" x2="48.26" y2="241.3" width="0.1524" layer="91"/>
<wire x1="48.26" y1="241.3" x2="48.26" y2="243.84" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="LED59" gate="G$1" pin="1.VDD"/>
<pinref part="U$59" gate="G$1" pin="+VBATT"/>
<wire x1="81.28" y1="241.3" x2="78.74" y2="241.3" width="0.1524" layer="91"/>
<wire x1="78.74" y1="241.3" x2="78.74" y2="243.84" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="LED60" gate="G$1" pin="1.VDD"/>
<pinref part="U$60" gate="G$1" pin="+VBATT"/>
<wire x1="111.76" y1="241.3" x2="109.22" y2="241.3" width="0.1524" layer="91"/>
<wire x1="109.22" y1="241.3" x2="109.22" y2="243.84" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="LED61" gate="G$1" pin="1.VDD"/>
<pinref part="U$61" gate="G$1" pin="+VBATT"/>
<wire x1="142.24" y1="241.3" x2="139.7" y2="241.3" width="0.1524" layer="91"/>
<wire x1="139.7" y1="241.3" x2="139.7" y2="243.84" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="LED62" gate="G$1" pin="1.VDD"/>
<wire x1="172.72" y1="241.3" x2="170.18" y2="241.3" width="0.1524" layer="91"/>
<pinref part="U$62" gate="G$1" pin="+VBATT"/>
<wire x1="170.18" y1="241.3" x2="170.18" y2="243.84" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="LED63" gate="G$1" pin="1.VDD"/>
<pinref part="U$63" gate="G$1" pin="+VBATT"/>
<wire x1="203.2" y1="241.3" x2="200.66" y2="241.3" width="0.1524" layer="91"/>
<wire x1="200.66" y1="241.3" x2="200.66" y2="243.84" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="LED64" gate="G$1" pin="1.VDD"/>
<pinref part="U$64" gate="G$1" pin="+VBATT"/>
<wire x1="233.68" y1="241.3" x2="231.14" y2="241.3" width="0.1524" layer="91"/>
<wire x1="231.14" y1="241.3" x2="231.14" y2="243.84" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="CONN" gate="G$1" pin="P$1"/>
<pinref part="U$65" gate="G$1" pin="+VBATT"/>
<wire x1="302.26" y1="139.7" x2="309.88" y2="139.7" width="0.1524" layer="91"/>
<wire x1="309.88" y1="139.7" x2="309.88" y2="144.78" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C1" gate="G$1" pin="1"/>
<pinref part="U$66" gate="G$1" pin="+VBATT"/>
<wire x1="297.18" y1="104.14" x2="297.18" y2="106.68" width="0.1524" layer="91"/>
<pinref part="C2" gate="G$1" pin="1"/>
<wire x1="297.18" y1="106.68" x2="297.18" y2="111.76" width="0.1524" layer="91"/>
<wire x1="297.18" y1="106.68" x2="302.26" y2="106.68" width="0.1524" layer="91"/>
<wire x1="302.26" y1="106.68" x2="302.26" y2="104.14" width="0.1524" layer="91"/>
<junction x="297.18" y="106.68"/>
<pinref part="C3" gate="G$1" pin="1"/>
<wire x1="302.26" y1="106.68" x2="307.34" y2="106.68" width="0.1524" layer="91"/>
<wire x1="307.34" y1="106.68" x2="307.34" y2="104.14" width="0.1524" layer="91"/>
<junction x="302.26" y="106.68"/>
<pinref part="C4" gate="G$1" pin="1"/>
<wire x1="307.34" y1="106.68" x2="312.42" y2="106.68" width="0.1524" layer="91"/>
<wire x1="312.42" y1="106.68" x2="312.42" y2="104.14" width="0.1524" layer="91"/>
<junction x="307.34" y="106.68"/>
</segment>
</net>
<net name="N$8" class="0">
<segment>
<pinref part="LED9" gate="G$1" pin="3.DOUT"/>
<pinref part="LED10" gate="G$1" pin="4.DIN"/>
<wire x1="45.72" y1="53.34" x2="50.8" y2="53.34" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$9" class="0">
<segment>
<pinref part="LED10" gate="G$1" pin="3.DOUT"/>
<pinref part="LED11" gate="G$1" pin="4.DIN"/>
<wire x1="76.2" y1="53.34" x2="81.28" y2="53.34" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$10" class="0">
<segment>
<pinref part="LED11" gate="G$1" pin="3.DOUT"/>
<pinref part="LED12" gate="G$1" pin="4.DIN"/>
<wire x1="106.68" y1="53.34" x2="111.76" y2="53.34" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$11" class="0">
<segment>
<pinref part="LED12" gate="G$1" pin="3.DOUT"/>
<pinref part="LED13" gate="G$1" pin="4.DIN"/>
<wire x1="137.16" y1="53.34" x2="142.24" y2="53.34" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$12" class="0">
<segment>
<pinref part="LED13" gate="G$1" pin="3.DOUT"/>
<pinref part="LED14" gate="G$1" pin="4.DIN"/>
<wire x1="167.64" y1="53.34" x2="172.72" y2="53.34" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$13" class="0">
<segment>
<pinref part="LED14" gate="G$1" pin="3.DOUT"/>
<pinref part="LED15" gate="G$1" pin="4.DIN"/>
<wire x1="198.12" y1="53.34" x2="203.2" y2="53.34" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$14" class="0">
<segment>
<pinref part="LED15" gate="G$1" pin="3.DOUT"/>
<pinref part="LED16" gate="G$1" pin="4.DIN"/>
<wire x1="228.6" y1="53.34" x2="233.68" y2="53.34" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$15" class="0">
<segment>
<pinref part="LED17" gate="G$1" pin="3.DOUT"/>
<pinref part="LED18" gate="G$1" pin="4.DIN"/>
<wire x1="45.72" y1="83.82" x2="50.8" y2="83.82" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$16" class="0">
<segment>
<pinref part="LED18" gate="G$1" pin="3.DOUT"/>
<pinref part="LED19" gate="G$1" pin="4.DIN"/>
<wire x1="76.2" y1="83.82" x2="81.28" y2="83.82" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$17" class="0">
<segment>
<pinref part="LED19" gate="G$1" pin="3.DOUT"/>
<pinref part="LED20" gate="G$1" pin="4.DIN"/>
<wire x1="106.68" y1="83.82" x2="111.76" y2="83.82" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$18" class="0">
<segment>
<pinref part="LED20" gate="G$1" pin="3.DOUT"/>
<pinref part="LED21" gate="G$1" pin="4.DIN"/>
<wire x1="137.16" y1="83.82" x2="142.24" y2="83.82" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$19" class="0">
<segment>
<pinref part="LED21" gate="G$1" pin="3.DOUT"/>
<pinref part="LED22" gate="G$1" pin="4.DIN"/>
<wire x1="167.64" y1="83.82" x2="172.72" y2="83.82" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$20" class="0">
<segment>
<pinref part="LED22" gate="G$1" pin="3.DOUT"/>
<pinref part="LED23" gate="G$1" pin="4.DIN"/>
<wire x1="198.12" y1="83.82" x2="203.2" y2="83.82" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$21" class="0">
<segment>
<pinref part="LED23" gate="G$1" pin="3.DOUT"/>
<pinref part="LED24" gate="G$1" pin="4.DIN"/>
<wire x1="228.6" y1="83.82" x2="233.68" y2="83.82" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$22" class="0">
<segment>
<pinref part="LED25" gate="G$1" pin="3.DOUT"/>
<pinref part="LED26" gate="G$1" pin="4.DIN"/>
<wire x1="45.72" y1="114.3" x2="50.8" y2="114.3" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$23" class="0">
<segment>
<pinref part="LED26" gate="G$1" pin="3.DOUT"/>
<pinref part="LED27" gate="G$1" pin="4.DIN"/>
<wire x1="76.2" y1="114.3" x2="81.28" y2="114.3" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$24" class="0">
<segment>
<pinref part="LED27" gate="G$1" pin="3.DOUT"/>
<pinref part="LED28" gate="G$1" pin="4.DIN"/>
<wire x1="106.68" y1="114.3" x2="111.76" y2="114.3" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$25" class="0">
<segment>
<pinref part="LED28" gate="G$1" pin="3.DOUT"/>
<pinref part="LED29" gate="G$1" pin="4.DIN"/>
<wire x1="137.16" y1="114.3" x2="142.24" y2="114.3" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$26" class="0">
<segment>
<pinref part="LED29" gate="G$1" pin="3.DOUT"/>
<pinref part="LED30" gate="G$1" pin="4.DIN"/>
<wire x1="167.64" y1="114.3" x2="172.72" y2="114.3" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$27" class="0">
<segment>
<pinref part="LED30" gate="G$1" pin="3.DOUT"/>
<pinref part="LED31" gate="G$1" pin="4.DIN"/>
<wire x1="198.12" y1="114.3" x2="203.2" y2="114.3" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$28" class="0">
<segment>
<pinref part="LED31" gate="G$1" pin="3.DOUT"/>
<pinref part="LED32" gate="G$1" pin="4.DIN"/>
<wire x1="228.6" y1="114.3" x2="233.68" y2="114.3" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$29" class="0">
<segment>
<pinref part="LED33" gate="G$1" pin="3.DOUT"/>
<pinref part="LED34" gate="G$1" pin="4.DIN"/>
<wire x1="45.72" y1="144.78" x2="50.8" y2="144.78" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$30" class="0">
<segment>
<pinref part="LED34" gate="G$1" pin="3.DOUT"/>
<pinref part="LED35" gate="G$1" pin="4.DIN"/>
<wire x1="76.2" y1="144.78" x2="81.28" y2="144.78" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$31" class="0">
<segment>
<pinref part="LED35" gate="G$1" pin="3.DOUT"/>
<pinref part="LED36" gate="G$1" pin="4.DIN"/>
<wire x1="106.68" y1="144.78" x2="111.76" y2="144.78" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$32" class="0">
<segment>
<pinref part="LED36" gate="G$1" pin="3.DOUT"/>
<pinref part="LED37" gate="G$1" pin="4.DIN"/>
<wire x1="137.16" y1="144.78" x2="142.24" y2="144.78" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$33" class="0">
<segment>
<pinref part="LED37" gate="G$1" pin="3.DOUT"/>
<pinref part="LED38" gate="G$1" pin="4.DIN"/>
<wire x1="167.64" y1="144.78" x2="172.72" y2="144.78" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$34" class="0">
<segment>
<pinref part="LED38" gate="G$1" pin="3.DOUT"/>
<pinref part="LED39" gate="G$1" pin="4.DIN"/>
<wire x1="198.12" y1="144.78" x2="203.2" y2="144.78" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$35" class="0">
<segment>
<pinref part="LED39" gate="G$1" pin="3.DOUT"/>
<pinref part="LED40" gate="G$1" pin="4.DIN"/>
<wire x1="228.6" y1="144.78" x2="233.68" y2="144.78" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$36" class="0">
<segment>
<pinref part="LED41" gate="G$1" pin="3.DOUT"/>
<pinref part="LED42" gate="G$1" pin="4.DIN"/>
<wire x1="45.72" y1="175.26" x2="50.8" y2="175.26" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$37" class="0">
<segment>
<pinref part="LED42" gate="G$1" pin="3.DOUT"/>
<pinref part="LED43" gate="G$1" pin="4.DIN"/>
<wire x1="76.2" y1="175.26" x2="81.28" y2="175.26" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$38" class="0">
<segment>
<pinref part="LED43" gate="G$1" pin="3.DOUT"/>
<pinref part="LED44" gate="G$1" pin="4.DIN"/>
<wire x1="106.68" y1="175.26" x2="111.76" y2="175.26" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$39" class="0">
<segment>
<pinref part="LED44" gate="G$1" pin="3.DOUT"/>
<pinref part="LED45" gate="G$1" pin="4.DIN"/>
<wire x1="137.16" y1="175.26" x2="142.24" y2="175.26" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$40" class="0">
<segment>
<pinref part="LED45" gate="G$1" pin="3.DOUT"/>
<pinref part="LED46" gate="G$1" pin="4.DIN"/>
<wire x1="167.64" y1="175.26" x2="172.72" y2="175.26" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$41" class="0">
<segment>
<pinref part="LED46" gate="G$1" pin="3.DOUT"/>
<pinref part="LED47" gate="G$1" pin="4.DIN"/>
<wire x1="198.12" y1="175.26" x2="203.2" y2="175.26" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$42" class="0">
<segment>
<pinref part="LED47" gate="G$1" pin="3.DOUT"/>
<pinref part="LED48" gate="G$1" pin="4.DIN"/>
<wire x1="228.6" y1="175.26" x2="233.68" y2="175.26" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$43" class="0">
<segment>
<pinref part="LED49" gate="G$1" pin="3.DOUT"/>
<pinref part="LED50" gate="G$1" pin="4.DIN"/>
<wire x1="45.72" y1="205.74" x2="50.8" y2="205.74" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$44" class="0">
<segment>
<pinref part="LED50" gate="G$1" pin="3.DOUT"/>
<pinref part="LED51" gate="G$1" pin="4.DIN"/>
<wire x1="76.2" y1="205.74" x2="81.28" y2="205.74" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$45" class="0">
<segment>
<pinref part="LED51" gate="G$1" pin="3.DOUT"/>
<pinref part="LED52" gate="G$1" pin="4.DIN"/>
<wire x1="106.68" y1="205.74" x2="111.76" y2="205.74" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$46" class="0">
<segment>
<pinref part="LED52" gate="G$1" pin="3.DOUT"/>
<pinref part="LED53" gate="G$1" pin="4.DIN"/>
<wire x1="137.16" y1="205.74" x2="142.24" y2="205.74" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$47" class="0">
<segment>
<pinref part="LED53" gate="G$1" pin="3.DOUT"/>
<pinref part="LED54" gate="G$1" pin="4.DIN"/>
<wire x1="167.64" y1="205.74" x2="172.72" y2="205.74" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$48" class="0">
<segment>
<pinref part="LED54" gate="G$1" pin="3.DOUT"/>
<pinref part="LED55" gate="G$1" pin="4.DIN"/>
<wire x1="198.12" y1="205.74" x2="203.2" y2="205.74" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$49" class="0">
<segment>
<pinref part="LED55" gate="G$1" pin="3.DOUT"/>
<pinref part="LED56" gate="G$1" pin="4.DIN"/>
<wire x1="228.6" y1="205.74" x2="233.68" y2="205.74" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$50" class="0">
<segment>
<pinref part="LED57" gate="G$1" pin="3.DOUT"/>
<pinref part="LED58" gate="G$1" pin="4.DIN"/>
<wire x1="45.72" y1="236.22" x2="50.8" y2="236.22" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$51" class="0">
<segment>
<pinref part="LED58" gate="G$1" pin="3.DOUT"/>
<pinref part="LED59" gate="G$1" pin="4.DIN"/>
<wire x1="76.2" y1="236.22" x2="81.28" y2="236.22" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$52" class="0">
<segment>
<pinref part="LED59" gate="G$1" pin="3.DOUT"/>
<pinref part="LED60" gate="G$1" pin="4.DIN"/>
<wire x1="106.68" y1="236.22" x2="111.76" y2="236.22" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$53" class="0">
<segment>
<pinref part="LED60" gate="G$1" pin="3.DOUT"/>
<pinref part="LED61" gate="G$1" pin="4.DIN"/>
<wire x1="137.16" y1="236.22" x2="142.24" y2="236.22" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$54" class="0">
<segment>
<pinref part="LED61" gate="G$1" pin="3.DOUT"/>
<pinref part="LED62" gate="G$1" pin="4.DIN"/>
<wire x1="167.64" y1="236.22" x2="172.72" y2="236.22" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$55" class="0">
<segment>
<pinref part="LED62" gate="G$1" pin="3.DOUT"/>
<pinref part="LED63" gate="G$1" pin="4.DIN"/>
<wire x1="198.12" y1="236.22" x2="203.2" y2="236.22" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$56" class="0">
<segment>
<pinref part="LED63" gate="G$1" pin="3.DOUT"/>
<pinref part="LED64" gate="G$1" pin="4.DIN"/>
<wire x1="228.6" y1="236.22" x2="233.68" y2="236.22" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$57" class="0">
<segment>
<pinref part="LED8" gate="G$1" pin="3.DOUT"/>
<wire x1="259.08" y1="22.86" x2="264.16" y2="22.86" width="0.1524" layer="91"/>
<wire x1="264.16" y1="22.86" x2="264.16" y2="38.1" width="0.1524" layer="91"/>
<wire x1="264.16" y1="38.1" x2="10.16" y2="38.1" width="0.1524" layer="91"/>
<wire x1="10.16" y1="38.1" x2="10.16" y2="53.34" width="0.1524" layer="91"/>
<pinref part="LED9" gate="G$1" pin="4.DIN"/>
<wire x1="10.16" y1="53.34" x2="20.32" y2="53.34" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$58" class="0">
<segment>
<pinref part="LED16" gate="G$1" pin="3.DOUT"/>
<wire x1="259.08" y1="53.34" x2="264.16" y2="53.34" width="0.1524" layer="91"/>
<wire x1="264.16" y1="53.34" x2="264.16" y2="68.58" width="0.1524" layer="91"/>
<wire x1="264.16" y1="68.58" x2="10.16" y2="68.58" width="0.1524" layer="91"/>
<wire x1="10.16" y1="68.58" x2="10.16" y2="83.82" width="0.1524" layer="91"/>
<pinref part="LED17" gate="G$1" pin="4.DIN"/>
<wire x1="10.16" y1="83.82" x2="20.32" y2="83.82" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$59" class="0">
<segment>
<pinref part="LED24" gate="G$1" pin="3.DOUT"/>
<wire x1="259.08" y1="83.82" x2="264.16" y2="83.82" width="0.1524" layer="91"/>
<wire x1="264.16" y1="83.82" x2="264.16" y2="99.06" width="0.1524" layer="91"/>
<wire x1="264.16" y1="99.06" x2="10.16" y2="99.06" width="0.1524" layer="91"/>
<wire x1="10.16" y1="99.06" x2="10.16" y2="114.3" width="0.1524" layer="91"/>
<pinref part="LED25" gate="G$1" pin="4.DIN"/>
<wire x1="10.16" y1="114.3" x2="20.32" y2="114.3" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$60" class="0">
<segment>
<pinref part="LED32" gate="G$1" pin="3.DOUT"/>
<wire x1="259.08" y1="114.3" x2="264.16" y2="114.3" width="0.1524" layer="91"/>
<wire x1="264.16" y1="114.3" x2="264.16" y2="129.54" width="0.1524" layer="91"/>
<wire x1="264.16" y1="129.54" x2="10.16" y2="129.54" width="0.1524" layer="91"/>
<wire x1="10.16" y1="129.54" x2="10.16" y2="144.78" width="0.1524" layer="91"/>
<pinref part="LED33" gate="G$1" pin="4.DIN"/>
<wire x1="10.16" y1="144.78" x2="20.32" y2="144.78" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$61" class="0">
<segment>
<pinref part="LED40" gate="G$1" pin="3.DOUT"/>
<wire x1="259.08" y1="144.78" x2="264.16" y2="144.78" width="0.1524" layer="91"/>
<wire x1="264.16" y1="144.78" x2="264.16" y2="160.02" width="0.1524" layer="91"/>
<wire x1="264.16" y1="160.02" x2="12.7" y2="160.02" width="0.1524" layer="91"/>
<wire x1="12.7" y1="160.02" x2="12.7" y2="175.26" width="0.1524" layer="91"/>
<pinref part="LED41" gate="G$1" pin="4.DIN"/>
<wire x1="12.7" y1="175.26" x2="20.32" y2="175.26" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$62" class="0">
<segment>
<pinref part="LED48" gate="G$1" pin="3.DOUT"/>
<wire x1="259.08" y1="175.26" x2="264.16" y2="175.26" width="0.1524" layer="91"/>
<wire x1="264.16" y1="175.26" x2="264.16" y2="190.5" width="0.1524" layer="91"/>
<wire x1="264.16" y1="190.5" x2="12.7" y2="190.5" width="0.1524" layer="91"/>
<wire x1="12.7" y1="190.5" x2="12.7" y2="205.74" width="0.1524" layer="91"/>
<wire x1="12.7" y1="205.74" x2="20.32" y2="205.74" width="0.1524" layer="91"/>
<pinref part="LED49" gate="G$1" pin="4.DIN"/>
</segment>
</net>
<net name="N$63" class="0">
<segment>
<pinref part="LED56" gate="G$1" pin="3.DOUT"/>
<wire x1="259.08" y1="205.74" x2="264.16" y2="205.74" width="0.1524" layer="91"/>
<wire x1="264.16" y1="205.74" x2="264.16" y2="220.98" width="0.1524" layer="91"/>
<wire x1="264.16" y1="220.98" x2="12.7" y2="220.98" width="0.1524" layer="91"/>
<wire x1="12.7" y1="220.98" x2="12.7" y2="236.22" width="0.1524" layer="91"/>
<pinref part="LED57" gate="G$1" pin="4.DIN"/>
<wire x1="12.7" y1="236.22" x2="20.32" y2="236.22" width="0.1524" layer="91"/>
</segment>
</net>
<net name="DIN" class="0">
<segment>
<pinref part="LED1" gate="G$1" pin="4.DIN"/>
<wire x1="20.32" y1="22.86" x2="10.16" y2="22.86" width="0.1524" layer="91"/>
<label x="10.16" y="22.86" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="CONN" gate="G$1" pin="P$2"/>
<wire x1="302.26" y1="137.16" x2="317.5" y2="137.16" width="0.1524" layer="91"/>
<label x="307.34" y="137.16" size="1.778" layer="95"/>
</segment>
</net>
<net name="DOUT" class="0">
<segment>
<pinref part="LED64" gate="G$1" pin="3.DOUT"/>
<wire x1="259.08" y1="236.22" x2="269.24" y2="236.22" width="0.1524" layer="91"/>
<label x="261.62" y="236.22" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="CONN" gate="G$1" pin="P$3"/>
<wire x1="302.26" y1="134.62" x2="317.5" y2="134.62" width="0.1524" layer="91"/>
<label x="307.34" y="134.62" size="1.778" layer="95"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
</eagle>

