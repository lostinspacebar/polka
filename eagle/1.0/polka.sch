<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="6.5.0">
<drawing>
<settings>
<setting alwaysvectorfont="yes"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="yes" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="1" fill="3" visible="no" active="no"/>
<layer number="3" name="Route3" color="4" fill="3" visible="no" active="no"/>
<layer number="14" name="Route14" color="1" fill="6" visible="no" active="no"/>
<layer number="15" name="Route15" color="4" fill="6" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="9" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="1" fill="9" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="100" name="Muster" color="7" fill="1" visible="no" active="no"/>
<layer number="101" name="Patch_Top" color="12" fill="4" visible="no" active="yes"/>
<layer number="102" name="Vscore" color="7" fill="1" visible="no" active="yes"/>
<layer number="103" name="3d" color="13" fill="1" visible="no" active="yes"/>
<layer number="104" name="Name" color="7" fill="1" visible="no" active="yes"/>
<layer number="105" name="tPlate" color="7" fill="1" visible="no" active="yes"/>
<layer number="106" name="bPlate" color="7" fill="1" visible="no" active="yes"/>
<layer number="107" name="Crop" color="7" fill="1" visible="no" active="yes"/>
<layer number="108" name="fp8" color="7" fill="1" visible="no" active="yes"/>
<layer number="109" name="fp9" color="7" fill="1" visible="no" active="yes"/>
<layer number="110" name="fp0" color="7" fill="1" visible="no" active="yes"/>
<layer number="111" name="LPC17xx" color="7" fill="1" visible="no" active="yes"/>
<layer number="112" name="tSilk" color="7" fill="1" visible="no" active="yes"/>
<layer number="116" name="Patch_BOT" color="9" fill="4" visible="no" active="yes"/>
<layer number="118" name="Rect_Pads" color="7" fill="1" visible="no" active="yes"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="no" active="yes"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="no" active="yes"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="no" active="yes"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="no" active="yes"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="no" active="yes"/>
<layer number="126" name="_bNames" color="7" fill="1" visible="no" active="yes"/>
<layer number="127" name="_tValues" color="7" fill="1" visible="no" active="yes"/>
<layer number="128" name="_bValues" color="7" fill="1" visible="no" active="yes"/>
<layer number="131" name="tAdjust" color="7" fill="1" visible="no" active="yes"/>
<layer number="132" name="bAdjust" color="7" fill="1" visible="no" active="yes"/>
<layer number="144" name="Drill_legend" color="7" fill="1" visible="no" active="yes"/>
<layer number="151" name="HeatSink" color="7" fill="1" visible="no" active="yes"/>
<layer number="152" name="_bDocu" color="7" fill="1" visible="no" active="yes"/>
<layer number="199" name="Contour" color="7" fill="1" visible="no" active="yes"/>
<layer number="200" name="200bmp" color="1" fill="10" visible="no" active="yes"/>
<layer number="201" name="201bmp" color="2" fill="10" visible="no" active="yes"/>
<layer number="202" name="202bmp" color="3" fill="10" visible="no" active="yes"/>
<layer number="203" name="203bmp" color="4" fill="10" visible="no" active="yes"/>
<layer number="204" name="204bmp" color="5" fill="10" visible="no" active="yes"/>
<layer number="205" name="205bmp" color="6" fill="10" visible="no" active="yes"/>
<layer number="206" name="206bmp" color="7" fill="10" visible="no" active="yes"/>
<layer number="207" name="207bmp" color="8" fill="10" visible="no" active="yes"/>
<layer number="208" name="208bmp" color="9" fill="10" visible="no" active="yes"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="211" name="211bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="217" name="217bmp" color="18" fill="1" visible="no" active="no"/>
<layer number="218" name="218bmp" color="19" fill="1" visible="no" active="no"/>
<layer number="219" name="219bmp" color="20" fill="1" visible="no" active="no"/>
<layer number="220" name="220bmp" color="21" fill="1" visible="no" active="no"/>
<layer number="221" name="221bmp" color="22" fill="1" visible="no" active="no"/>
<layer number="222" name="222bmp" color="23" fill="1" visible="no" active="no"/>
<layer number="223" name="223bmp" color="24" fill="1" visible="no" active="no"/>
<layer number="224" name="224bmp" color="25" fill="1" visible="no" active="no"/>
<layer number="231" name="Eagle3D_PG1" color="7" fill="1" visible="no" active="yes"/>
<layer number="232" name="Eagle3D_PG2" color="7" fill="1" visible="no" active="yes"/>
<layer number="233" name="Eagle3D_PG3" color="7" fill="1" visible="no" active="yes"/>
<layer number="247" name="wrappinf" color="7" fill="1" visible="no" active="yes"/>
<layer number="248" name="PLCDoku" color="7" fill="1" visible="no" active="yes"/>
<layer number="249" name="MillDoku" color="7" fill="1" visible="no" active="yes"/>
<layer number="250" name="Descript" color="3" fill="1" visible="no" active="no"/>
<layer number="251" name="SMDround" color="12" fill="11" visible="no" active="no"/>
<layer number="252" name="BR-BS" color="7" fill="1" visible="no" active="yes"/>
<layer number="253" name="BR-LS" color="7" fill="1" visible="no" active="yes"/>
<layer number="254" name="cooling" color="7" fill="1" visible="no" active="yes"/>
<layer number="255" name="ZchnBlatt" color="7" fill="1" visible="no" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="aditya_main">
<description>Common anode RGB matrix</description>
<packages>
<package name="QFN-32">
<description>&lt;h3&gt;QFN 32-Pin package w/ Thermal Pad&lt;/h3&gt;
&lt;b&gt;***Unproven***&lt;/b&gt;
&lt;br&gt;&lt;br&gt;
&lt;B&gt;Applicable Parts:&lt;/b&gt;
&lt;ul&gt;&lt;li&gt;TLC5940&lt;/ul&gt;</description>
<wire x1="-2.5" y1="-2.5" x2="2.5" y2="-2.5" width="0.127" layer="51"/>
<wire x1="2.5" y1="-2.5" x2="2.5" y2="2.5" width="0.127" layer="51"/>
<wire x1="2.5" y1="2.5" x2="-2.5" y2="2.5" width="0.127" layer="51"/>
<wire x1="-2.5" y1="2.5" x2="-2.5" y2="-2.5" width="0.127" layer="51"/>
<wire x1="-2.5" y1="2.1" x2="-2.5" y2="2.5" width="0.127" layer="21"/>
<wire x1="-2.5" y1="2.5" x2="-2.1" y2="2.5" width="0.127" layer="21"/>
<wire x1="2.1" y1="2.5" x2="2.5" y2="2.5" width="0.127" layer="21"/>
<wire x1="2.5" y1="2.5" x2="2.5" y2="2.1" width="0.127" layer="21"/>
<wire x1="2.5" y1="-2.1" x2="2.5" y2="-2.5" width="0.127" layer="21"/>
<wire x1="2.5" y1="-2.5" x2="2.1" y2="-2.5" width="0.127" layer="21"/>
<wire x1="-2.5" y1="-2.1" x2="-2.1" y2="-2.5" width="0.127" layer="21"/>
<circle x="-1.8" y="-1.8" radius="0.1" width="0.127" layer="21"/>
<smd name="1" x="-1.75" y="-2.5" dx="0.8" dy="0.2846" layer="1" rot="R90"/>
<smd name="2" x="-1.25" y="-2.5" dx="0.8" dy="0.2846" layer="1" rot="R90"/>
<smd name="3" x="-0.75" y="-2.5" dx="0.8" dy="0.2846" layer="1" rot="R90"/>
<smd name="4" x="-0.25" y="-2.5" dx="0.8" dy="0.2846" layer="1" rot="R90"/>
<smd name="5" x="0.25" y="-2.5" dx="0.8" dy="0.2846" layer="1" rot="R90"/>
<smd name="6" x="0.75" y="-2.5" dx="0.8" dy="0.2846" layer="1" rot="R90"/>
<smd name="7" x="1.25" y="-2.5" dx="0.8" dy="0.2846" layer="1" rot="R90"/>
<smd name="8" x="1.75" y="-2.5" dx="0.8" dy="0.2846" layer="1" rot="R90"/>
<smd name="9" x="2.5" y="-1.75" dx="0.8" dy="0.2846" layer="1" rot="R180"/>
<smd name="10" x="2.5" y="-1.25" dx="0.8" dy="0.2846" layer="1" rot="R180"/>
<smd name="11" x="2.5" y="-0.75" dx="0.8" dy="0.2846" layer="1" rot="R180"/>
<smd name="12" x="2.5" y="-0.25" dx="0.8" dy="0.2846" layer="1" rot="R180"/>
<smd name="13" x="2.5" y="0.25" dx="0.8" dy="0.2846" layer="1" rot="R180"/>
<smd name="14" x="2.5" y="0.75" dx="0.8" dy="0.2846" layer="1" rot="R180"/>
<smd name="15" x="2.5" y="1.25" dx="0.8" dy="0.2846" layer="1" rot="R180"/>
<smd name="16" x="2.5" y="1.75" dx="0.8" dy="0.2846" layer="1" rot="R180"/>
<smd name="17" x="1.75" y="2.5" dx="0.8" dy="0.2846" layer="1" rot="R270"/>
<smd name="18" x="1.25" y="2.5" dx="0.8" dy="0.2846" layer="1" rot="R270"/>
<smd name="19" x="0.75" y="2.5" dx="0.8" dy="0.2846" layer="1" rot="R270"/>
<smd name="20" x="0.25" y="2.5" dx="0.8" dy="0.2846" layer="1" rot="R270"/>
<smd name="21" x="-0.25" y="2.5" dx="0.8" dy="0.2846" layer="1" rot="R270"/>
<smd name="22" x="-0.75" y="2.5" dx="0.8" dy="0.2846" layer="1" rot="R270"/>
<smd name="23" x="-1.25" y="2.5" dx="0.8" dy="0.2846" layer="1" rot="R270"/>
<smd name="24" x="-1.75" y="2.5" dx="0.8" dy="0.2846" layer="1" rot="R270"/>
<smd name="25" x="-2.5" y="1.75" dx="0.8" dy="0.2846" layer="1"/>
<smd name="26" x="-2.5" y="1.25" dx="0.8" dy="0.2846" layer="1"/>
<smd name="27" x="-2.5" y="0.75" dx="0.8" dy="0.2846" layer="1"/>
<smd name="28" x="-2.5" y="0.25" dx="0.8" dy="0.2846" layer="1"/>
<smd name="29" x="-2.5" y="-0.25" dx="0.8" dy="0.2846" layer="1"/>
<smd name="30" x="-2.5" y="-0.75" dx="0.8" dy="0.2846" layer="1"/>
<smd name="31" x="-2.5" y="-1.25" dx="0.8" dy="0.2846" layer="1"/>
<smd name="32" x="-2.5" y="-1.75" dx="0.8" dy="0.2846" layer="1"/>
<text x="-1.27" y="1.524" size="0.4318" layer="25">&gt;Name</text>
<text x="-1.27" y="-1.905" size="0.4318" layer="27">&gt;Value</text>
<rectangle x1="-2.5" y1="0.13" x2="-2.1" y2="0.37" layer="51"/>
<rectangle x1="-2.5" y1="-0.37" x2="-2.1" y2="-0.13" layer="51"/>
<rectangle x1="-2.5" y1="-0.87" x2="-2.1" y2="-0.63" layer="51"/>
<rectangle x1="-2.5" y1="-1.37" x2="-2.1" y2="-1.13" layer="51"/>
<rectangle x1="-2.5" y1="-1.87" x2="-2.1" y2="-1.63" layer="51"/>
<rectangle x1="-2.5" y1="0.63" x2="-2.1" y2="0.87" layer="51"/>
<rectangle x1="-2.5" y1="1.13" x2="-2.1" y2="1.37" layer="51"/>
<rectangle x1="-2.5" y1="1.63" x2="-2.1" y2="1.87" layer="51"/>
<rectangle x1="0.05" y1="2.18" x2="0.45" y2="2.42" layer="51" rot="R270"/>
<rectangle x1="-0.45" y1="2.18" x2="-0.05" y2="2.42" layer="51" rot="R270"/>
<rectangle x1="-0.95" y1="2.18" x2="-0.55" y2="2.42" layer="51" rot="R270"/>
<rectangle x1="-1.45" y1="2.18" x2="-1.05" y2="2.42" layer="51" rot="R270"/>
<rectangle x1="-1.95" y1="2.18" x2="-1.55" y2="2.42" layer="51" rot="R270"/>
<rectangle x1="0.55" y1="2.18" x2="0.95" y2="2.42" layer="51" rot="R270"/>
<rectangle x1="1.05" y1="2.18" x2="1.45" y2="2.42" layer="51" rot="R270"/>
<rectangle x1="1.55" y1="2.18" x2="1.95" y2="2.42" layer="51" rot="R270"/>
<rectangle x1="-0.45" y1="-2.42" x2="-0.05" y2="-2.18" layer="51" rot="R90"/>
<rectangle x1="0.05" y1="-2.42" x2="0.45" y2="-2.18" layer="51" rot="R90"/>
<rectangle x1="0.55" y1="-2.42" x2="0.95" y2="-2.18" layer="51" rot="R90"/>
<rectangle x1="1.05" y1="-2.42" x2="1.45" y2="-2.18" layer="51" rot="R90"/>
<rectangle x1="1.55" y1="-2.42" x2="1.95" y2="-2.18" layer="51" rot="R90"/>
<rectangle x1="-0.95" y1="-2.42" x2="-0.55" y2="-2.18" layer="51" rot="R90"/>
<rectangle x1="-1.45" y1="-2.42" x2="-1.05" y2="-2.18" layer="51" rot="R90"/>
<rectangle x1="-1.95" y1="-2.42" x2="-1.55" y2="-2.18" layer="51" rot="R90"/>
<rectangle x1="2.1" y1="-0.37" x2="2.5" y2="-0.13" layer="51" rot="R180"/>
<rectangle x1="2.1" y1="0.13" x2="2.5" y2="0.37" layer="51" rot="R180"/>
<rectangle x1="2.1" y1="0.63" x2="2.5" y2="0.87" layer="51" rot="R180"/>
<rectangle x1="2.1" y1="1.13" x2="2.5" y2="1.37" layer="51" rot="R180"/>
<rectangle x1="2.1" y1="1.63" x2="2.5" y2="1.87" layer="51" rot="R180"/>
<rectangle x1="2.1" y1="-0.87" x2="2.5" y2="-0.63" layer="51" rot="R180"/>
<rectangle x1="2.1" y1="-1.37" x2="2.5" y2="-1.13" layer="51" rot="R180"/>
<rectangle x1="2.1" y1="-1.87" x2="2.5" y2="-1.63" layer="51" rot="R180"/>
<rectangle x1="-1.45" y1="0.15" x2="-0.15" y2="1.45" layer="31"/>
<rectangle x1="0.15" y1="0.15" x2="1.45" y2="1.45" layer="31" rot="R270"/>
<rectangle x1="-1.45" y1="-1.45" x2="-0.15" y2="-0.15" layer="31" rot="R90"/>
<rectangle x1="0.15" y1="-1.45" x2="1.45" y2="-0.15" layer="31" rot="R180"/>
</package>
<package name="AXIAL-0.3">
<wire x1="-2.54" y1="0.762" x2="2.54" y2="0.762" width="0.2032" layer="21"/>
<wire x1="2.54" y1="0.762" x2="2.54" y2="0" width="0.2032" layer="21"/>
<wire x1="2.54" y1="0" x2="2.54" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="2.54" y1="-0.762" x2="-2.54" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="-2.54" y1="-0.762" x2="-2.54" y2="0" width="0.2032" layer="21"/>
<wire x1="-2.54" y1="0" x2="-2.54" y2="0.762" width="0.2032" layer="21"/>
<wire x1="2.54" y1="0" x2="2.794" y2="0" width="0.2032" layer="21"/>
<wire x1="-2.54" y1="0" x2="-2.794" y2="0" width="0.2032" layer="21"/>
<pad name="P$1" x="-3.81" y="0" drill="0.9" diameter="1.8796"/>
<pad name="P$2" x="3.81" y="0" drill="0.9" diameter="1.8796"/>
<text x="-2.54" y="1.27" size="0.762" layer="25">&gt;Name</text>
<text x="-2.032" y="-0.508" size="0.8128" layer="21">&gt;Value</text>
</package>
<package name="R2010">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-1.662" y1="1.245" x2="1.662" y2="1.245" width="0.1524" layer="51"/>
<wire x1="-1.637" y1="-1.245" x2="1.687" y2="-1.245" width="0.1524" layer="51"/>
<wire x1="-3.473" y1="1.483" x2="3.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="1.483" x2="3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="-1.483" x2="-3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-3.473" y1="-1.483" x2="-3.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="-1.027" y1="1.245" x2="1.027" y2="1.245" width="0.1524" layer="21"/>
<wire x1="-1.002" y1="-1.245" x2="1.016" y2="-1.245" width="0.1524" layer="21"/>
<smd name="1" x="-2.2" y="0" dx="1.8" dy="2.7" layer="1"/>
<smd name="2" x="2.2" y="0" dx="1.8" dy="2.7" layer="1"/>
<text x="-2.54" y="1.5875" size="0.4064" layer="25">&gt;NAME</text>
<text x="-2.54" y="-2.032" size="0.4064" layer="27">&gt;VALUE</text>
<rectangle x1="-2.4892" y1="-1.3208" x2="-1.6393" y2="1.3292" layer="51"/>
<rectangle x1="1.651" y1="-1.3208" x2="2.5009" y2="1.3292" layer="51"/>
</package>
<package name="0402">
<smd name="1" x="-0.49" y="0" dx="0.58" dy="0.66" layer="1" roundness="10"/>
<smd name="2" x="0.49" y="0" dx="0.58" dy="0.66" layer="1" roundness="10"/>
<text x="-0.75" y="-0.15" size="0.3" layer="25">&gt;NAME</text>
<rectangle x1="-0.0729" y1="-0.4389" x2="0.3269" y2="0.1611" layer="35"/>
<wire x1="-0.95" y1="0.5" x2="0.95" y2="0.5" width="0.05" layer="21"/>
<wire x1="0.95" y1="0.5" x2="0.95" y2="-0.5" width="0.05" layer="21"/>
<wire x1="0.95" y1="-0.5" x2="-0.95" y2="-0.5" width="0.05" layer="21"/>
<wire x1="-0.95" y1="-0.5" x2="-0.95" y2="0.5" width="0.05" layer="21"/>
<rectangle x1="-1.2192" y1="-0.508" x2="-1.016" y2="0.508" layer="21"/>
</package>
<package name="1206">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<smd name="2" x="1.5" y="0" dx="1.15" dy="1.8" layer="1" roundness="20"/>
<smd name="1" x="-1.5" y="0" dx="1.15" dy="1.8" layer="1" roundness="20"/>
<rectangle x1="-0.3" y1="-0.7" x2="0.3" y2="0.7" layer="35"/>
<wire x1="2.35" y1="1.15" x2="2.35" y2="-1.15" width="0.05" layer="49"/>
<wire x1="2.35" y1="-1.15" x2="-2.35" y2="-1.15" width="0.05" layer="49"/>
<wire x1="-2.35" y1="-1.15" x2="-2.35" y2="1.15" width="0.05" layer="49"/>
<wire x1="-2.35" y1="1.15" x2="2.35" y2="1.15" width="0.05" layer="49"/>
<text x="-1.8" y="-0.25" size="0.5" layer="49" ratio="6">&gt;NAME</text>
</package>
<package name="AXIAL-5MM">
<wire x1="-1.14" y1="0.762" x2="1.14" y2="0.762" width="0.2032" layer="21"/>
<wire x1="1.14" y1="0.762" x2="1.14" y2="0" width="0.2032" layer="21"/>
<wire x1="1.14" y1="0" x2="1.14" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="1.14" y1="-0.762" x2="-1.14" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="-1.14" y1="-0.762" x2="-1.14" y2="0" width="0.2032" layer="21"/>
<wire x1="-1.14" y1="0" x2="-1.14" y2="0.762" width="0.2032" layer="21"/>
<wire x1="1.14" y1="0" x2="1.394" y2="0" width="0.2032" layer="21"/>
<wire x1="-1.14" y1="0" x2="-1.394" y2="0" width="0.2032" layer="21"/>
<pad name="P$1" x="-2.5" y="0" drill="0.9" diameter="1.8796"/>
<pad name="P$2" x="2.5" y="0" drill="0.9" diameter="1.8796"/>
<text x="-2.54" y="1.17" size="0.762" layer="25">&gt;Name</text>
<text x="-1.032" y="-0.208" size="0.4" layer="21" ratio="15">&gt;Value</text>
</package>
<package name="0201">
<description>&lt;b&gt;RESISTOR&lt;/b&gt; chip&lt;p&gt;
Source: http://www.vishay.com/docs/20008/dcrcw.pdf</description>
<smd name="1" x="-0.33" y="0" dx="0.42" dy="0.46" layer="1" roundness="10"/>
<smd name="2" x="0.33" y="0" dx="0.42" dy="0.46" layer="1" roundness="10"/>
<wire x1="-0.7" y1="0.4" x2="-0.7" y2="-0.4" width="0.05" layer="49"/>
<wire x1="-0.7" y1="-0.4" x2="0.7" y2="-0.4" width="0.05" layer="49"/>
<wire x1="0.7" y1="-0.4" x2="0.7" y2="0.4" width="0.05" layer="49"/>
<wire x1="0.7" y1="0.4" x2="-0.7" y2="0.4" width="0.05" layer="49"/>
<text x="-0.55" y="-0.15" size="0.3" layer="49" font="vector">&gt;NAME</text>
<rectangle x1="-0.2" y1="0.3" x2="0.2" y2="0.4" layer="21"/>
<rectangle x1="-0.2" y1="-0.4" x2="0.2" y2="-0.3" layer="21"/>
</package>
<package name="RGB_MATRIX">
<pad name="P$1" x="-19.05" y="-22.86" drill="1.016" diameter="1.8796"/>
<pad name="P$2" x="-16.51" y="-22.86" drill="1.016" diameter="1.8796"/>
<pad name="P$3" x="-13.97" y="-22.86" drill="1.016" diameter="1.8796"/>
<pad name="P$4" x="-11.43" y="-22.86" drill="1.016" diameter="1.8796"/>
<pad name="P$5" x="-8.89" y="-22.86" drill="1.016" diameter="1.8796"/>
<pad name="P$6" x="-6.35" y="-22.86" drill="1.016" diameter="1.8796"/>
<pad name="P$7" x="-3.81" y="-22.86" drill="1.016" diameter="1.8796"/>
<pad name="P$8" x="-1.27" y="-22.86" drill="1.016" diameter="1.8796"/>
<pad name="P$9" x="1.27" y="-22.86" drill="1.016" diameter="1.8796"/>
<pad name="P$10" x="3.81" y="-22.86" drill="1.016" diameter="1.8796"/>
<pad name="P$11" x="6.35" y="-22.86" drill="1.016" diameter="1.8796"/>
<pad name="P$12" x="8.89" y="-22.86" drill="1.016" diameter="1.8796"/>
<pad name="P$13" x="11.43" y="-22.86" drill="1.016" diameter="1.8796"/>
<pad name="P$14" x="13.97" y="-22.86" drill="1.016" diameter="1.8796"/>
<pad name="P$15" x="16.51" y="-22.86" drill="1.016" diameter="1.8796"/>
<pad name="P$16" x="19.05" y="-22.86" drill="1.016" diameter="1.8796"/>
<pad name="P$17" x="19.05" y="22.86" drill="1.016" diameter="1.8796" rot="R180"/>
<pad name="P$18" x="16.51" y="22.86" drill="1.016" diameter="1.8796" rot="R180"/>
<pad name="P$19" x="13.97" y="22.86" drill="1.016" diameter="1.8796" rot="R180"/>
<pad name="P$20" x="11.43" y="22.86" drill="1.016" diameter="1.8796" rot="R180"/>
<pad name="P$21" x="8.89" y="22.86" drill="1.016" diameter="1.8796" rot="R180"/>
<pad name="P$22" x="6.35" y="22.86" drill="1.016" diameter="1.8796" rot="R180"/>
<pad name="P$23" x="3.81" y="22.86" drill="1.016" diameter="1.8796" rot="R180"/>
<pad name="P$24" x="1.27" y="22.86" drill="1.016" diameter="1.8796" rot="R180"/>
<pad name="P$25" x="-1.27" y="22.86" drill="1.016" diameter="1.8796" rot="R180"/>
<pad name="P$26" x="-3.81" y="22.86" drill="1.016" diameter="1.8796" rot="R180"/>
<pad name="P$27" x="-6.35" y="22.86" drill="1.016" diameter="1.8796" rot="R180"/>
<pad name="P$28" x="-8.89" y="22.86" drill="1.016" diameter="1.8796" rot="R180"/>
<pad name="P$29" x="-11.43" y="22.86" drill="1.016" diameter="1.8796" rot="R180"/>
<pad name="P$30" x="-13.97" y="22.86" drill="1.016" diameter="1.8796" rot="R180"/>
<pad name="P$31" x="-16.51" y="22.86" drill="1.016" diameter="1.8796" rot="R180"/>
<pad name="P$32" x="-19.05" y="22.86" drill="1.016" diameter="1.8796" rot="R180"/>
<wire x1="-30.25" y1="30.25" x2="30.25" y2="30.25" width="0.127" layer="49"/>
<wire x1="30.25" y1="30.25" x2="30.25" y2="-30.25" width="0.127" layer="49"/>
<wire x1="30.25" y1="-30.25" x2="-30.25" y2="-30.25" width="0.127" layer="49"/>
<wire x1="-30.25" y1="-30.25" x2="-30.25" y2="30.25" width="0.127" layer="49"/>
<circle x="-3.81" y="3.81" radius="2" width="0.127" layer="49"/>
<circle x="3.81" y="3.81" radius="2" width="0.127" layer="49"/>
<circle x="-3.81" y="-3.81" radius="2" width="0.127" layer="49"/>
<circle x="3.81" y="-3.81" radius="2" width="0.127" layer="49"/>
<circle x="-3.81" y="19.05" radius="2" width="0.127" layer="49"/>
<circle x="3.81" y="19.05" radius="2" width="0.127" layer="49"/>
<circle x="-3.81" y="11.43" radius="2" width="0.127" layer="49"/>
<circle x="3.81" y="11.43" radius="2" width="0.127" layer="49"/>
<circle x="-3.81" y="26.67" radius="2" width="0.127" layer="49"/>
<circle x="3.81" y="26.67" radius="2" width="0.127" layer="49"/>
<circle x="-3.81" y="-19.05" radius="2" width="0.127" layer="49"/>
<circle x="3.81" y="-19.05" radius="2" width="0.127" layer="49"/>
<circle x="-3.81" y="-26.67" radius="2" width="0.127" layer="49"/>
<circle x="3.81" y="-26.67" radius="2" width="0.127" layer="49"/>
<circle x="-3.81" y="-11.43" radius="2" width="0.127" layer="49"/>
<circle x="3.81" y="-11.43" radius="2" width="0.127" layer="49"/>
<circle x="11.43" y="3.81" radius="2" width="0.127" layer="49"/>
<circle x="19.05" y="3.81" radius="2" width="0.127" layer="49"/>
<circle x="11.43" y="-3.81" radius="2" width="0.127" layer="49"/>
<circle x="19.05" y="-3.81" radius="2" width="0.127" layer="49"/>
<circle x="11.43" y="19.05" radius="2" width="0.127" layer="49"/>
<circle x="19.05" y="19.05" radius="2" width="0.127" layer="49"/>
<circle x="11.43" y="11.43" radius="2" width="0.127" layer="49"/>
<circle x="19.05" y="11.43" radius="2" width="0.127" layer="49"/>
<circle x="11.43" y="26.67" radius="2" width="0.127" layer="49"/>
<circle x="19.05" y="26.67" radius="2" width="0.127" layer="49"/>
<circle x="11.43" y="-19.05" radius="2" width="0.127" layer="49"/>
<circle x="19.05" y="-19.05" radius="2" width="0.127" layer="49"/>
<circle x="11.43" y="-26.67" radius="2" width="0.127" layer="49"/>
<circle x="19.05" y="-26.67" radius="2" width="0.127" layer="49"/>
<circle x="11.43" y="-11.43" radius="2" width="0.127" layer="49"/>
<circle x="19.05" y="-11.43" radius="2" width="0.127" layer="49"/>
<circle x="26.67" y="3.81" radius="2" width="0.127" layer="49"/>
<circle x="26.67" y="-3.81" radius="2" width="0.127" layer="49"/>
<circle x="26.67" y="19.05" radius="2" width="0.127" layer="49"/>
<circle x="26.67" y="11.43" radius="2" width="0.127" layer="49"/>
<circle x="26.67" y="26.67" radius="2" width="0.127" layer="49"/>
<circle x="26.67" y="-19.05" radius="2" width="0.127" layer="49"/>
<circle x="26.67" y="-26.67" radius="2" width="0.127" layer="49"/>
<circle x="26.67" y="-11.43" radius="2" width="0.127" layer="49"/>
<circle x="-26.67" y="3.81" radius="2" width="0.127" layer="49"/>
<circle x="-19.05" y="3.81" radius="2" width="0.127" layer="49"/>
<circle x="-26.67" y="-3.81" radius="2" width="0.127" layer="49"/>
<circle x="-19.05" y="-3.81" radius="2" width="0.127" layer="49"/>
<circle x="-26.67" y="19.05" radius="2" width="0.127" layer="49"/>
<circle x="-19.05" y="19.05" radius="2" width="0.127" layer="49"/>
<circle x="-26.67" y="11.43" radius="2" width="0.127" layer="49"/>
<circle x="-19.05" y="11.43" radius="2" width="0.127" layer="49"/>
<circle x="-26.67" y="26.67" radius="2" width="0.127" layer="49"/>
<circle x="-19.05" y="26.67" radius="2" width="0.127" layer="49"/>
<circle x="-26.67" y="-19.05" radius="2" width="0.127" layer="49"/>
<circle x="-19.05" y="-19.05" radius="2" width="0.127" layer="49"/>
<circle x="-26.67" y="-26.67" radius="2" width="0.127" layer="49"/>
<circle x="-19.05" y="-26.67" radius="2" width="0.127" layer="49"/>
<circle x="-26.67" y="-11.43" radius="2" width="0.127" layer="49"/>
<circle x="-19.05" y="-11.43" radius="2" width="0.127" layer="49"/>
<circle x="-11.43" y="3.81" radius="2" width="0.127" layer="49"/>
<circle x="-11.43" y="-3.81" radius="2" width="0.127" layer="49"/>
<circle x="-11.43" y="19.05" radius="2" width="0.127" layer="49"/>
<circle x="-11.43" y="11.43" radius="2" width="0.127" layer="49"/>
<circle x="-11.43" y="26.67" radius="2" width="0.127" layer="49"/>
<circle x="-11.43" y="-19.05" radius="2" width="0.127" layer="49"/>
<circle x="-11.43" y="-26.67" radius="2" width="0.127" layer="49"/>
<circle x="-11.43" y="-11.43" radius="2" width="0.127" layer="49"/>
<rectangle x1="-21.59" y1="-24.13" x2="-20.32" y2="-21.59" layer="21"/>
<rectangle x1="-20.32" y1="-25.4" x2="-17.78" y2="-24.13" layer="21"/>
</package>
<package name="CAP-PTH-SMALL">
<wire x1="1.27" y1="0.635" x2="1.27" y2="-0.635" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0" drill="0.7" diameter="1.651"/>
<pad name="2" x="2.54" y="0" drill="0.7" diameter="1.651"/>
<text x="-0.508" y="1.27" size="0.762" layer="25">&gt;Name</text>
<text x="-0.508" y="-1.778" size="0.762" layer="27">&gt;Value</text>
</package>
<package name="SOT23-3">
<wire x1="1.4224" y1="0.6604" x2="1.4224" y2="-0.6604" width="0.1524" layer="51"/>
<wire x1="1.4224" y1="-0.6604" x2="-1.4224" y2="-0.6604" width="0.1524" layer="51"/>
<wire x1="-1.4224" y1="-0.6604" x2="-1.4224" y2="0.6604" width="0.1524" layer="51"/>
<wire x1="-1.4224" y1="0.6604" x2="1.4224" y2="0.6604" width="0.1524" layer="51"/>
<wire x1="-0.8" y1="0.7" x2="-1.4" y2="0.7" width="0.2032" layer="21"/>
<wire x1="-1.4" y1="0.7" x2="-1.4" y2="-0.1" width="0.2032" layer="21"/>
<wire x1="0.8" y1="0.7" x2="1.4" y2="0.7" width="0.2032" layer="21"/>
<wire x1="1.4" y1="0.7" x2="1.4" y2="-0.1" width="0.2032" layer="21"/>
<smd name="3" x="0" y="1.1" dx="0.8" dy="0.9" layer="1"/>
<smd name="2" x="0.95" y="-1" dx="0.8" dy="0.9" layer="1"/>
<smd name="1" x="-0.95" y="-1" dx="0.8" dy="0.9" layer="1"/>
<text x="-0.8255" y="1.778" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.016" y="-0.1905" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="DFN-3-1006">
<smd name="P1" x="0" y="0" dx="1" dy="0.5" layer="1" roundness="20" rot="R90"/>
<smd name="P2" x="0.7" y="0.3" dx="0.4" dy="0.4" layer="1" roundness="20" rot="R90"/>
<smd name="P3" x="0.7" y="-0.3" dx="0.4" dy="0.4" layer="1" roundness="20" rot="R90"/>
<wire x1="-0.45" y1="-0.7" x2="1.1" y2="-0.7" width="0.05" layer="49"/>
<wire x1="1.1" y1="-0.7" x2="1.1" y2="0.7" width="0.05" layer="49"/>
<wire x1="1.1" y1="0.7" x2="-0.45" y2="0.7" width="0.05" layer="49"/>
<wire x1="-0.45" y1="0.7" x2="-0.45" y2="-0.7" width="0.05" layer="49"/>
</package>
<package name="SOT323">
<wire x1="1.1224" y1="0.6604" x2="1.1224" y2="-0.6604" width="0.1524" layer="51"/>
<wire x1="1.1224" y1="-0.6604" x2="-1.1224" y2="-0.6604" width="0.1524" layer="51"/>
<wire x1="-1.1224" y1="-0.6604" x2="-1.1224" y2="0.6604" width="0.1524" layer="51"/>
<wire x1="-1.1224" y1="0.6604" x2="1.1224" y2="0.6604" width="0.1524" layer="51"/>
<wire x1="-0.8" y1="0.7" x2="-1.1" y2="0.7" width="0.2032" layer="21"/>
<wire x1="-1.1" y1="0.7" x2="-1.1" y2="-0.1" width="0.2032" layer="21"/>
<wire x1="0.8" y1="0.7" x2="1.1" y2="0.7" width="0.2032" layer="21"/>
<wire x1="1.1" y1="0.7" x2="1.1" y2="-0.1" width="0.2032" layer="21"/>
<smd name="1" x="-0.65" y="-0.925" dx="0.7" dy="0.7" layer="1"/>
<smd name="2" x="0.65" y="-0.925" dx="0.7" dy="0.7" layer="1"/>
<smd name="3" x="0" y="0.925" dx="0.7" dy="0.7" layer="1"/>
<text x="-1.1255" y="1.878" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.116" y="1.3095" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="0805">
<smd name="1" x="-0.9" y="0" dx="1.15" dy="1.45" layer="1" roundness="15"/>
<smd name="2" x="0.9" y="0" dx="1.15" dy="1.45" layer="1" roundness="15"/>
<text x="-1.25" y="-0.15" size="0.3" layer="49">&gt;NAME</text>
<wire x1="-1.75" y1="1" x2="-1.75" y2="-1" width="0.05" layer="21"/>
<wire x1="-1.75" y1="-1" x2="1.75" y2="-1" width="0.05" layer="21"/>
<wire x1="1.75" y1="-1" x2="1.75" y2="1" width="0.05" layer="21"/>
<wire x1="1.75" y1="1" x2="-1.75" y2="1" width="0.05" layer="21"/>
<rectangle x1="-1.9558" y1="-1.016" x2="-1.8034" y2="1.016" layer="21"/>
</package>
<package name="0603">
<smd name="1" x="-0.8" y="0" dx="0.95" dy="1" layer="1" roundness="15"/>
<smd name="2" x="0.8" y="0" dx="0.95" dy="1" layer="1" roundness="15"/>
<text x="-1.55" y="0.85" size="0.5" layer="21" font="vector">&gt;NAME</text>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
<wire x1="-1.55" y1="0.75" x2="-1.55" y2="-0.75" width="0.05" layer="21"/>
<wire x1="-1.55" y1="-0.75" x2="1.55" y2="-0.75" width="0.05" layer="21"/>
<wire x1="1.55" y1="-0.75" x2="1.55" y2="0.75" width="0.05" layer="21"/>
<wire x1="1.55" y1="0.75" x2="-1.55" y2="0.75" width="0.05" layer="21"/>
<rectangle x1="-1.905" y1="-0.762" x2="-1.6256" y2="0.762" layer="21"/>
</package>
<package name="2POS_254MM_1R_THRU">
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="-1.27" width="0.127" layer="21"/>
<wire x1="-1.27" y1="-1.27" x2="3.81" y2="-1.27" width="0.127" layer="21"/>
<wire x1="3.81" y1="-1.27" x2="3.81" y2="1.27" width="0.127" layer="21"/>
<wire x1="3.81" y1="1.27" x2="-1.27" y2="1.27" width="0.127" layer="21"/>
<pad name="P$1" x="0" y="0" drill="1.016" diameter="1.905" shape="square"/>
<pad name="P$2" x="2.54" y="0" drill="1.016" diameter="1.905"/>
<text x="-1.27" y="1.27" size="1.27" layer="21">&gt;NAME</text>
</package>
<package name="14POS_254MM_2R_THRU">
<wire x1="-1.27" y1="-1.27" x2="16.51" y2="-1.27" width="0.127" layer="21"/>
<wire x1="16.51" y1="-1.27" x2="16.51" y2="3.81" width="0.127" layer="21"/>
<wire x1="16.51" y1="3.81" x2="-1.27" y2="3.81" width="0.127" layer="21"/>
<wire x1="-1.27" y1="3.81" x2="-1.27" y2="-1.27" width="0.127" layer="21"/>
<pad name="P$1" x="0" y="0" drill="1.016" diameter="1.8796" shape="square"/>
<pad name="P$2" x="2.54" y="0" drill="1.016" diameter="1.8796"/>
<pad name="P$3" x="5.08" y="0" drill="1.016" diameter="1.8796"/>
<pad name="P$4" x="7.62" y="0" drill="1.016" diameter="1.8796"/>
<pad name="P$5" x="10.16" y="0" drill="1.016" diameter="1.8796"/>
<pad name="P$6" x="12.7" y="0" drill="1.016" diameter="1.8796"/>
<pad name="P$7" x="15.24" y="0" drill="1.016" diameter="1.8796"/>
<pad name="P$8" x="15.24" y="2.54" drill="1.016" diameter="1.8796"/>
<pad name="P$9" x="12.7" y="2.54" drill="1.016" diameter="1.8796"/>
<pad name="P$10" x="10.16" y="2.54" drill="1.016" diameter="1.8796"/>
<pad name="P$11" x="7.62" y="2.54" drill="1.016" diameter="1.8796"/>
<pad name="P$12" x="5.08" y="2.54" drill="1.016" diameter="1.8796"/>
<text x="-1.27" y="3.81" size="1.27" layer="21">&gt;NAME</text>
<pad name="P$13" x="2.54" y="2.54" drill="1.016" diameter="1.8796"/>
<pad name="P$14" x="0" y="2.54" drill="1.016" diameter="1.8796"/>
</package>
<package name="14POS_0.5MM_2R_SMD_AXK5F14547YG_SOCKET">
<smd name="P8" x="1.5" y="2.7" dx="1.4" dy="0.25" layer="1" rot="R90"/>
<smd name="P3" x="-0.5" y="-2.7" dx="1.4" dy="0.25" layer="1" rot="R90"/>
<smd name="P7" x="1.5" y="-2.7" dx="1.4" dy="0.25" layer="1" rot="R90"/>
<smd name="P6" x="1" y="-2.7" dx="1.4" dy="0.25" layer="1" rot="R90"/>
<smd name="P9" x="1" y="2.7" dx="1.4" dy="0.25" layer="1" rot="R90"/>
<smd name="P10" x="0.5" y="2.7" dx="1.4" dy="0.25" layer="1" rot="R90"/>
<smd name="P4" x="0" y="-2.7" dx="1.4" dy="0.25" layer="1" rot="R90"/>
<smd name="P5" x="0.5" y="-2.7" dx="1.4" dy="0.25" layer="1" rot="R90"/>
<smd name="P2" x="-1" y="-2.7" dx="1.4" dy="0.25" layer="1" rot="R90"/>
<smd name="P1" x="-1.5" y="-2.7" dx="1.4" dy="0.25" layer="1" rot="R90"/>
<wire x1="-3.25" y1="2.4" x2="3.25" y2="2.4" width="0.1524" layer="21"/>
<wire x1="3.25" y1="2.4" x2="3.25" y2="-2.4" width="0.1524" layer="21"/>
<wire x1="3.25" y1="-2.4" x2="-3.25" y2="-2.4" width="0.1524" layer="21"/>
<wire x1="-3.25" y1="-2.4" x2="-3.25" y2="2.4" width="0.1524" layer="21"/>
<smd name="P11" x="0" y="2.7" dx="1.4" dy="0.25" layer="1" rot="R90"/>
<smd name="P12" x="-0.5" y="2.7" dx="1.4" dy="0.25" layer="1" rot="R90"/>
<smd name="P13" x="-1" y="2.7" dx="1.4" dy="0.25" layer="1" rot="R90"/>
<smd name="P14" x="-1.5" y="2.7" dx="1.4" dy="0.25" layer="1" rot="R90"/>
</package>
<package name="14POS_0.5MM_2R_SMD_AXK6F14347YG_HEADER">
<smd name="P8" x="1.5" y="1.475" dx="1.35" dy="0.25" layer="1" rot="R90"/>
<smd name="P3" x="-0.5" y="-1.475" dx="1.35" dy="0.25" layer="1" rot="R90"/>
<smd name="P7" x="1.5" y="-1.475" dx="1.35" dy="0.25" layer="1" rot="R90"/>
<smd name="P6" x="1" y="-1.475" dx="1.35" dy="0.25" layer="1" rot="R90"/>
<smd name="P4" x="0" y="-1.475" dx="1.35" dy="0.25" layer="1" rot="R90"/>
<smd name="P5" x="0.5" y="-1.475" dx="1.35" dy="0.25" layer="1" rot="R90"/>
<smd name="P9" x="1" y="1.475" dx="1.35" dy="0.25" layer="1" rot="R90"/>
<smd name="P10" x="0.5" y="1.475" dx="1.35" dy="0.25" layer="1" rot="R90"/>
<smd name="P2" x="-1" y="-1.475" dx="1.35" dy="0.25" layer="1" rot="R90"/>
<smd name="P1" x="-1.5" y="-1.475" dx="1.35" dy="0.25" layer="1" rot="R90"/>
<wire x1="-3.25" y1="1.25" x2="3.25" y2="1.25" width="0.1524" layer="21"/>
<wire x1="3.25" y1="1.25" x2="3.25" y2="-1.25" width="0.1524" layer="21"/>
<wire x1="3.25" y1="-1.25" x2="-3.25" y2="-1.25" width="0.1524" layer="21"/>
<wire x1="-3.25" y1="-1.25" x2="-3.25" y2="1.25" width="0.1524" layer="21"/>
<smd name="P11" x="0" y="1.475" dx="1.35" dy="0.25" layer="1" rot="R90"/>
<smd name="P12" x="-0.5" y="1.475" dx="1.35" dy="0.25" layer="1" rot="R90"/>
<smd name="P13" x="-1" y="1.475" dx="1.35" dy="0.25" layer="1" rot="R90"/>
<smd name="P14" x="-1.5" y="1.475" dx="1.35" dy="0.25" layer="1" rot="R90"/>
</package>
<package name="14POS_254MM_1R_THRU">
<pad name="P$1" x="0" y="0" drill="1.0922" diameter="1.905" shape="square"/>
<pad name="P$2" x="2.54" y="0" drill="1.0922" diameter="1.905"/>
<pad name="P$3" x="5.08" y="0" drill="1.0922" diameter="1.905"/>
<pad name="P$4" x="7.62" y="0" drill="1.0922" diameter="1.905"/>
<pad name="P$5" x="10.16" y="0" drill="1.0922" diameter="1.905"/>
<pad name="P$6" x="12.7" y="0" drill="1.0922" diameter="1.905"/>
<pad name="P$7" x="15.24" y="0" drill="1.0922" diameter="1.905"/>
<pad name="P$8" x="17.78" y="0" drill="1.0922" diameter="1.905"/>
<pad name="P$9" x="20.32" y="0" drill="1.0922" diameter="1.905"/>
<pad name="P$10" x="22.86" y="0" drill="1.0922" diameter="1.905"/>
<pad name="P$11" x="25.4" y="0" drill="1.0922" diameter="1.905"/>
<pad name="P$12" x="27.94" y="0" drill="1.0922" diameter="1.905"/>
<pad name="P$13" x="30.48" y="0" drill="1.0922" diameter="1.905"/>
<pad name="P$14" x="33.02" y="0" drill="1.0922" diameter="1.905"/>
<wire x1="-1.524" y1="-1.524" x2="-1.524" y2="1.524" width="0.1524" layer="21"/>
<wire x1="-1.524" y1="1.524" x2="34.544" y2="1.524" width="0.1524" layer="21"/>
<wire x1="34.544" y1="1.524" x2="34.544" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="34.544" y1="-1.524" x2="-1.524" y2="-1.524" width="0.1524" layer="21"/>
</package>
<package name="14POS_2MM_1R_THRU">
<pad name="P$1" x="0" y="0" drill="0.8" shape="square"/>
<pad name="P$2" x="2" y="0" drill="0.8"/>
<pad name="P$3" x="4" y="0" drill="0.8"/>
<pad name="P$4" x="6" y="0" drill="0.8"/>
<pad name="P$5" x="8" y="0" drill="0.8"/>
<pad name="P$6" x="10" y="0" drill="0.8"/>
<pad name="P$7" x="12" y="0" drill="0.8"/>
<pad name="P$8" x="14" y="0" drill="0.8"/>
<pad name="P$9" x="16" y="0" drill="0.8"/>
<pad name="P$10" x="18" y="0" drill="0.8"/>
<pad name="P$11" x="20" y="0" drill="0.8"/>
<pad name="P$12" x="22" y="0" drill="0.8"/>
<pad name="P$13" x="24" y="0" drill="0.8"/>
<pad name="P$14" x="26" y="0" drill="0.8"/>
<wire x1="-1" y1="1" x2="-1" y2="-1" width="0.2032" layer="21"/>
<wire x1="-1" y1="-1" x2="27" y2="-1" width="0.2032" layer="21"/>
<wire x1="27" y1="-1" x2="27" y2="1" width="0.2032" layer="21"/>
<wire x1="27" y1="1" x2="-1" y2="1" width="0.2032" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="A3L-LOC">
<wire x1="0" y1="0" x2="50.8" y2="0" width="0.1016" layer="94"/>
<wire x1="50.8" y1="0" x2="98.425" y2="0" width="0.1016" layer="94"/>
<wire x1="98.425" y1="0" x2="146.05" y2="0" width="0.1016" layer="94"/>
<wire x1="146.05" y1="0" x2="193.675" y2="0" width="0.1016" layer="94"/>
<wire x1="193.675" y1="0" x2="241.3" y2="0" width="0.1016" layer="94"/>
<wire x1="241.3" y1="0" x2="288.925" y2="0" width="0.1016" layer="94"/>
<wire x1="288.925" y1="0" x2="336.55" y2="0" width="0.1016" layer="94"/>
<wire x1="336.55" y1="0" x2="387.35" y2="0" width="0.1016" layer="94"/>
<wire x1="387.35" y1="0" x2="387.35" y2="53.975" width="0.1016" layer="94"/>
<wire x1="387.35" y1="53.975" x2="387.35" y2="104.775" width="0.1016" layer="94"/>
<wire x1="387.35" y1="104.775" x2="387.35" y2="155.575" width="0.1016" layer="94"/>
<wire x1="387.35" y1="155.575" x2="387.35" y2="206.375" width="0.1016" layer="94"/>
<wire x1="387.35" y1="206.375" x2="387.35" y2="260.35" width="0.1016" layer="94"/>
<wire x1="146.05" y1="260.35" x2="98.425" y2="260.35" width="0.1016" layer="94"/>
<wire x1="98.425" y1="260.35" x2="50.8" y2="260.35" width="0.1016" layer="94"/>
<wire x1="50.8" y1="260.35" x2="0" y2="260.35" width="0.1016" layer="94"/>
<wire x1="0" y1="260.35" x2="0" y2="206.375" width="0.1016" layer="94"/>
<wire x1="0" y1="206.375" x2="0" y2="155.575" width="0.1016" layer="94"/>
<wire x1="0" y1="155.575" x2="0" y2="104.775" width="0.1016" layer="94"/>
<wire x1="0" y1="104.775" x2="0" y2="53.975" width="0.1016" layer="94"/>
<wire x1="0" y1="53.975" x2="0" y2="0" width="0.1016" layer="94"/>
<wire x1="3.175" y1="3.175" x2="50.8" y2="3.175" width="0.1016" layer="94"/>
<wire x1="50.8" y1="3.175" x2="98.425" y2="3.175" width="0.1016" layer="94"/>
<wire x1="98.425" y1="3.175" x2="146.05" y2="3.175" width="0.1016" layer="94"/>
<wire x1="146.05" y1="3.175" x2="193.675" y2="3.175" width="0.1016" layer="94"/>
<wire x1="193.675" y1="3.175" x2="241.3" y2="3.175" width="0.1016" layer="94"/>
<wire x1="241.3" y1="3.175" x2="288.925" y2="3.175" width="0.1016" layer="94"/>
<wire x1="336.55" y1="3.175" x2="307.34" y2="3.175" width="0.1016" layer="94"/>
<wire x1="288.925" y1="3.175" x2="307.34" y2="3.175" width="0.1016" layer="94"/>
<wire x1="336.55" y1="3.175" x2="366.395" y2="3.175" width="0.1016" layer="94"/>
<wire x1="366.395" y1="3.175" x2="384.175" y2="3.175" width="0.1016" layer="94"/>
<wire x1="384.175" y1="3.175" x2="384.175" y2="8.255" width="0.1016" layer="94"/>
<wire x1="384.175" y1="8.255" x2="384.175" y2="53.975" width="0.1016" layer="94"/>
<wire x1="384.175" y1="53.975" x2="384.175" y2="104.775" width="0.1016" layer="94"/>
<wire x1="384.175" y1="104.775" x2="384.175" y2="155.575" width="0.1016" layer="94"/>
<wire x1="384.175" y1="155.575" x2="384.175" y2="206.375" width="0.1016" layer="94"/>
<wire x1="384.175" y1="206.375" x2="384.175" y2="257.175" width="0.1016" layer="94"/>
<wire x1="384.175" y1="257.175" x2="336.55" y2="257.175" width="0.1016" layer="94"/>
<wire x1="336.55" y1="257.175" x2="288.925" y2="257.175" width="0.1016" layer="94"/>
<wire x1="288.925" y1="257.175" x2="241.3" y2="257.175" width="0.1016" layer="94"/>
<wire x1="241.3" y1="257.175" x2="193.675" y2="257.175" width="0.1016" layer="94"/>
<wire x1="193.675" y1="257.175" x2="146.05" y2="257.175" width="0.1016" layer="94"/>
<wire x1="146.05" y1="257.175" x2="98.425" y2="257.175" width="0.1016" layer="94"/>
<wire x1="98.425" y1="257.175" x2="50.8" y2="257.175" width="0.1016" layer="94"/>
<wire x1="50.8" y1="257.175" x2="3.175" y2="257.175" width="0.1016" layer="94"/>
<wire x1="3.175" y1="257.175" x2="3.175" y2="206.375" width="0.1016" layer="94"/>
<wire x1="3.175" y1="206.375" x2="3.175" y2="155.575" width="0.1016" layer="94"/>
<wire x1="3.175" y1="155.575" x2="3.175" y2="104.775" width="0.1016" layer="94"/>
<wire x1="3.175" y1="104.775" x2="3.175" y2="53.975" width="0.1016" layer="94"/>
<wire x1="3.175" y1="53.975" x2="3.175" y2="3.175" width="0.1016" layer="94"/>
<wire x1="387.35" y1="260.35" x2="336.55" y2="260.35" width="0.1016" layer="94"/>
<wire x1="336.55" y1="260.35" x2="288.925" y2="260.35" width="0.1016" layer="94"/>
<wire x1="288.925" y1="260.35" x2="241.3" y2="260.35" width="0.1016" layer="94"/>
<wire x1="241.3" y1="260.35" x2="193.675" y2="260.35" width="0.1016" layer="94"/>
<wire x1="193.675" y1="260.35" x2="146.05" y2="260.35" width="0.1016" layer="94"/>
<wire x1="193.675" y1="260.35" x2="193.675" y2="257.175" width="0.1016" layer="94"/>
<wire x1="193.675" y1="3.175" x2="193.675" y2="0" width="0.1016" layer="94"/>
<wire x1="0" y1="104.775" x2="3.175" y2="104.775" width="0.1016" layer="94"/>
<wire x1="384.175" y1="155.575" x2="387.35" y2="155.575" width="0.1016" layer="94"/>
<wire x1="98.425" y1="257.175" x2="98.425" y2="260.35" width="0.1016" layer="94"/>
<wire x1="98.425" y1="3.175" x2="98.425" y2="0" width="0.1016" layer="94"/>
<wire x1="288.925" y1="260.35" x2="288.925" y2="257.175" width="0.1016" layer="94"/>
<wire x1="288.925" y1="3.175" x2="288.925" y2="0" width="0.1016" layer="94"/>
<wire x1="0" y1="53.975" x2="3.175" y2="53.975" width="0.1016" layer="94"/>
<wire x1="384.175" y1="104.775" x2="387.35" y2="104.775" width="0.1016" layer="94"/>
<wire x1="0" y1="155.575" x2="3.175" y2="155.575" width="0.1016" layer="94"/>
<wire x1="384.175" y1="206.375" x2="387.35" y2="206.375" width="0.1016" layer="94"/>
<wire x1="50.8" y1="257.175" x2="50.8" y2="260.35" width="0.1016" layer="94"/>
<wire x1="0" y1="206.375" x2="3.175" y2="206.375" width="0.1016" layer="94"/>
<wire x1="384.175" y1="53.975" x2="387.35" y2="53.975" width="0.1016" layer="94"/>
<wire x1="146.05" y1="257.175" x2="146.05" y2="260.35" width="0.1016" layer="94"/>
<wire x1="241.3" y1="260.35" x2="241.3" y2="257.175" width="0.1016" layer="94"/>
<wire x1="336.55" y1="260.35" x2="336.55" y2="257.175" width="0.1016" layer="94"/>
<wire x1="336.55" y1="3.175" x2="336.55" y2="0" width="0.1016" layer="94"/>
<wire x1="241.3" y1="3.175" x2="241.3" y2="0" width="0.1016" layer="94"/>
<wire x1="146.05" y1="3.175" x2="146.05" y2="0" width="0.1016" layer="94"/>
<wire x1="50.8" y1="0" x2="50.8" y2="3.175" width="0.1016" layer="94"/>
<wire x1="366.395" y1="3.175" x2="366.395" y2="8.255" width="0.1016" layer="94"/>
<wire x1="366.395" y1="8.255" x2="384.175" y2="8.255" width="0.1016" layer="94"/>
<wire x1="366.395" y1="8.255" x2="307.34" y2="8.255" width="0.1016" layer="94"/>
<wire x1="307.34" y1="3.175" x2="307.34" y2="8.255" width="0.1016" layer="94"/>
<text x="24.384" y="0.254" size="2.54" layer="94" font="vector">A</text>
<text x="74.422" y="0.254" size="2.54" layer="94" font="vector">B</text>
<text x="121.158" y="0.254" size="2.54" layer="94" font="vector">C</text>
<text x="169.418" y="0.254" size="2.54" layer="94" font="vector">D</text>
<text x="216.916" y="0.254" size="2.54" layer="94" font="vector">E</text>
<text x="263.652" y="0.254" size="2.54" layer="94" font="vector">F</text>
<text x="310.642" y="0.254" size="2.54" layer="94" font="vector">G</text>
<text x="360.934" y="0.254" size="2.54" layer="94" font="vector">H</text>
<text x="385.064" y="28.702" size="2.54" layer="94" font="vector">1</text>
<text x="384.81" y="79.502" size="2.54" layer="94" font="vector">2</text>
<text x="384.81" y="130.302" size="2.54" layer="94" font="vector">3</text>
<text x="384.81" y="181.864" size="2.54" layer="94" font="vector">4</text>
<text x="384.81" y="231.14" size="2.54" layer="94" font="vector">5</text>
<text x="361.188" y="257.556" size="2.54" layer="94" font="vector">H</text>
<text x="311.404" y="257.556" size="2.54" layer="94" font="vector">G</text>
<text x="262.89" y="257.556" size="2.54" layer="94" font="vector">F</text>
<text x="215.9" y="257.556" size="2.54" layer="94" font="vector">E</text>
<text x="168.148" y="257.556" size="2.54" layer="94" font="vector">D</text>
<text x="120.904" y="257.556" size="2.54" layer="94" font="vector">C</text>
<text x="72.898" y="257.556" size="2.54" layer="94" font="vector">B</text>
<text x="24.384" y="257.556" size="2.54" layer="94" font="vector">A</text>
<text x="0.762" y="231.14" size="2.54" layer="94" font="vector">5</text>
<text x="0.762" y="181.61" size="2.54" layer="94" font="vector">4</text>
<text x="0.762" y="130.302" size="2.54" layer="94" font="vector">3</text>
<text x="0.762" y="79.248" size="2.54" layer="94" font="vector">2</text>
<text x="1.016" y="26.67" size="2.54" layer="94" font="vector">1</text>
<text x="308.991" y="4.191" size="2.54" layer="94" font="vector">&gt;DRAWING_NAME</text>
<text x="368.3" y="4.445" size="2.54" layer="94" font="vector">&gt;SHEET</text>
</symbol>
<symbol name="DRIVER_TLC5947">
<pin name="1.OUT0" x="20.32" y="25.4" length="middle" rot="R180"/>
<pin name="2.OUT1" x="20.32" y="22.86" length="middle" rot="R180"/>
<pin name="3.OUT2" x="20.32" y="20.32" length="middle" rot="R180"/>
<pin name="4.OUT3" x="20.32" y="17.78" length="middle" rot="R180"/>
<pin name="5.OUT4" x="20.32" y="15.24" length="middle" rot="R180"/>
<pin name="6.OUT5" x="20.32" y="12.7" length="middle" rot="R180"/>
<pin name="7.OUT6" x="20.32" y="10.16" length="middle" rot="R180"/>
<pin name="8.OUT7" x="20.32" y="7.62" length="middle" rot="R180"/>
<pin name="9.OUT8" x="20.32" y="5.08" length="middle" rot="R180"/>
<pin name="10.OUT9" x="20.32" y="2.54" length="middle" rot="R180"/>
<pin name="11.OUT10" x="20.32" y="0" length="middle" rot="R180"/>
<pin name="12.OUT11" x="20.32" y="-2.54" length="middle" rot="R180"/>
<pin name="13.OUT12" x="20.32" y="-5.08" length="middle" rot="R180"/>
<pin name="14.OUT13" x="20.32" y="-7.62" length="middle" rot="R180"/>
<pin name="15.OUT14" x="20.32" y="-10.16" length="middle" rot="R180"/>
<pin name="16.OUT15" x="20.32" y="-12.7" length="middle" rot="R180"/>
<pin name="17.OUT16" x="20.32" y="-15.24" length="middle" rot="R180"/>
<pin name="18.OUT17" x="20.32" y="-17.78" length="middle" rot="R180"/>
<pin name="19.OUT18" x="20.32" y="-20.32" length="middle" rot="R180"/>
<pin name="20.OUT19" x="20.32" y="-22.86" length="middle" rot="R180"/>
<pin name="21.OUT20" x="20.32" y="-25.4" length="middle" rot="R180"/>
<pin name="22.OUT21" x="20.32" y="-27.94" length="middle" rot="R180"/>
<pin name="23.OUT22" x="20.32" y="-30.48" length="middle" rot="R180"/>
<pin name="24.OUT23" x="20.32" y="-33.02" length="middle" rot="R180"/>
<pin name="25.SOUT" x="20.32" y="30.48" length="middle" rot="R180"/>
<pin name="26.XLAT" x="-20.32" y="22.86" length="middle"/>
<pin name="27.IREF" x="-20.32" y="-17.78" length="middle"/>
<pin name="28.VCC" x="-20.32" y="35.56" length="middle"/>
<pin name="29.GND" x="-20.32" y="-33.02" length="middle"/>
<pin name="30.BLANK" x="-20.32" y="25.4" length="middle"/>
<pin name="31.SCLK" x="-20.32" y="27.94" length="middle"/>
<pin name="32.SIN" x="-20.32" y="30.48" length="middle"/>
<wire x1="-15.24" y1="40.64" x2="-15.24" y2="-38.1" width="0.254" layer="94"/>
<wire x1="-15.24" y1="-38.1" x2="15.24" y2="-38.1" width="0.254" layer="94"/>
<wire x1="15.24" y1="-38.1" x2="15.24" y2="40.64" width="0.254" layer="94"/>
<wire x1="15.24" y1="40.64" x2="-15.24" y2="40.64" width="0.254" layer="94"/>
<text x="-15.24" y="40.64" size="1.778" layer="94">&gt;NAME</text>
<text x="5.08" y="-40.64" size="1.778" layer="94">TLC5947</text>
</symbol>
<symbol name="GND">
<wire x1="-1.905" y1="0" x2="1.905" y2="0" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
<symbol name="R">
<wire x1="-2.54" y1="0" x2="-2.159" y2="1.016" width="0.1524" layer="94"/>
<wire x1="-2.159" y1="1.016" x2="-1.524" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="-1.524" y1="-1.016" x2="-0.889" y2="1.016" width="0.1524" layer="94"/>
<wire x1="-0.889" y1="1.016" x2="-0.254" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="-0.254" y1="-1.016" x2="0.381" y2="1.016" width="0.1524" layer="94"/>
<wire x1="0.381" y1="1.016" x2="1.016" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="1.016" y1="-1.016" x2="1.651" y2="1.016" width="0.1524" layer="94"/>
<wire x1="1.651" y1="1.016" x2="2.286" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="2.286" y1="-1.016" x2="2.54" y2="0" width="0.1524" layer="94"/>
<text x="-2.413" y="1.3716" size="1.016" layer="95">&gt;NAME</text>
<text x="-2.794" y="-2.286" size="1.016" layer="96">&gt;VALUE</text>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
</symbol>
<symbol name="MATRIX_GTM2088ARGB-21">
<pin name="1.B1" x="-20.32" y="-15.24" length="middle"/>
<pin name="2.B2" x="-20.32" y="-17.78" length="middle"/>
<pin name="3.B3" x="-20.32" y="-20.32" length="middle"/>
<pin name="4.B4" x="-20.32" y="-22.86" length="middle"/>
<pin name="5.B5" x="-20.32" y="-25.4" length="middle"/>
<pin name="6.B6" x="-20.32" y="-27.94" length="middle"/>
<pin name="7.B7" x="-20.32" y="-30.48" length="middle"/>
<pin name="8.B8" x="-20.32" y="-33.02" length="middle"/>
<pin name="9.R1" x="-20.32" y="30.48" length="middle"/>
<pin name="10.R2" x="-20.32" y="27.94" length="middle"/>
<pin name="11.R3" x="-20.32" y="25.4" length="middle"/>
<pin name="12.R4" x="-20.32" y="22.86" length="middle"/>
<pin name="13.R5" x="-20.32" y="20.32" length="middle"/>
<pin name="14.R6" x="-20.32" y="17.78" length="middle"/>
<pin name="15.R7" x="-20.32" y="15.24" length="middle"/>
<pin name="16.R8" x="-20.32" y="12.7" length="middle"/>
<pin name="17.ROW1" x="20.32" y="30.48" length="middle" rot="R180"/>
<pin name="18.ROW2" x="20.32" y="27.94" length="middle" rot="R180"/>
<pin name="19.ROW3" x="20.32" y="25.4" length="middle" rot="R180"/>
<pin name="20.ROW4" x="20.32" y="22.86" length="middle" rot="R180"/>
<pin name="21.G8" x="-20.32" y="-10.16" length="middle"/>
<pin name="22.G7" x="-20.32" y="-7.62" length="middle"/>
<pin name="23.G6" x="-20.32" y="-5.08" length="middle"/>
<pin name="24.G5" x="-20.32" y="-2.54" length="middle"/>
<pin name="25.G4" x="-20.32" y="0" length="middle"/>
<pin name="26.G3" x="-20.32" y="2.54" length="middle"/>
<pin name="27.G2" x="-20.32" y="5.08" length="middle"/>
<pin name="28.G1" x="-20.32" y="7.62" length="middle"/>
<pin name="29.ROW5" x="20.32" y="20.32" length="middle" rot="R180"/>
<pin name="30.ROW6" x="20.32" y="17.78" length="middle" rot="R180"/>
<pin name="31.ROW7" x="20.32" y="15.24" length="middle" rot="R180"/>
<pin name="32.ROW8" x="20.32" y="12.7" length="middle" rot="R180"/>
<wire x1="-15.24" y1="35.56" x2="-15.24" y2="-38.1" width="0.254" layer="94"/>
<wire x1="-15.24" y1="-38.1" x2="15.24" y2="-38.1" width="0.254" layer="94"/>
<wire x1="15.24" y1="-38.1" x2="15.24" y2="35.56" width="0.254" layer="94"/>
<wire x1="15.24" y1="35.56" x2="-15.24" y2="35.56" width="0.254" layer="94"/>
<text x="-15.24" y="38.1" size="1.778" layer="94">&gt;NAME</text>
<text x="-7.62" y="-40.64" size="1.778" layer="94">GTM2088ARGB-21</text>
</symbol>
<symbol name="C">
<wire x1="0" y1="2.54" x2="0" y2="2.032" width="0.1524" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="0.508" width="0.1524" layer="94"/>
<text x="1.524" y="2.921" size="1.016" layer="95">&gt;NAME</text>
<text x="1.397" y="-1.016" size="1.016" layer="96">&gt;VALUE</text>
<text x="-1.27" y="2.54" size="1.27" layer="94">+</text>
<rectangle x1="-2.032" y1="0.508" x2="2.032" y2="1.016" layer="94"/>
<rectangle x1="-2.032" y1="1.524" x2="2.032" y2="2.032" layer="94"/>
<pin name="1" x="0" y="5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="2" x="0" y="-2.54" visible="off" length="short" direction="pas" swaplevel="1" rot="R90"/>
</symbol>
<symbol name="MOSFET-PCHANNEL">
<wire x1="-3.6576" y1="2.413" x2="-3.6576" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0" y1="1.905" x2="-2.0066" y2="1.905" width="0.1524" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="-1.905" width="0.1524" layer="94"/>
<wire x1="-2.032" y1="-1.905" x2="0" y2="-1.905" width="0.1524" layer="94"/>
<wire x1="0" y1="2.54" x2="0" y2="1.905" width="0.1524" layer="94"/>
<wire x1="0" y1="1.905" x2="2.54" y2="1.905" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-1.905" x2="0" y2="-1.905" width="0.1524" layer="94"/>
<wire x1="0" y1="-1.905" x2="0" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="-0.762" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-0.762" x2="2.54" y2="1.905" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-0.762" x2="3.175" y2="0.635" width="0.1524" layer="94"/>
<wire x1="3.175" y1="0.635" x2="1.905" y2="0.635" width="0.1524" layer="94"/>
<wire x1="1.905" y1="0.635" x2="2.54" y2="-0.762" width="0.1524" layer="94"/>
<wire x1="3.175" y1="-0.762" x2="2.54" y2="-0.762" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-0.762" x2="1.905" y2="-0.762" width="0.1524" layer="94"/>
<wire x1="1.905" y1="-0.762" x2="1.651" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="3.175" y1="-0.762" x2="3.429" y2="-0.508" width="0.1524" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="0.508" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="0.508" x2="-1.27" y2="-0.508" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="-0.508" x2="0" y2="0" width="0.1524" layer="94"/>
<wire x1="-1.143" y1="0" x2="-2.032" y2="0" width="0.1524" layer="94"/>
<wire x1="-1.143" y1="-0.254" x2="-0.254" y2="0" width="0.3048" layer="94"/>
<wire x1="-0.254" y1="0" x2="-1.143" y2="0.254" width="0.3048" layer="94"/>
<wire x1="-1.143" y1="0.254" x2="-1.143" y2="0" width="0.3048" layer="94"/>
<wire x1="-1.143" y1="0" x2="-0.889" y2="0" width="0.3048" layer="94"/>
<wire x1="-3.81" y1="0" x2="-5.08" y2="0" width="0.1524" layer="94"/>
<circle x="0" y="-1.905" radius="0.127" width="0.4064" layer="94"/>
<circle x="0" y="1.905" radius="0.127" width="0.4064" layer="94"/>
<text x="5.08" y="2.54" size="1.778" layer="95">&gt;NAME</text>
<text x="5.08" y="0" size="1.778" layer="96">&gt;VALUE</text>
<text x="-1.27" y="2.54" size="0.8128" layer="94">D</text>
<text x="-1.27" y="-3.556" size="0.8128" layer="94">S</text>
<text x="-5.08" y="-1.27" size="0.8128" layer="94">G</text>
<rectangle x1="-2.794" y1="-2.54" x2="-2.032" y2="-1.27" layer="94"/>
<rectangle x1="-2.794" y1="1.27" x2="-2.032" y2="2.54" layer="94"/>
<rectangle x1="-2.794" y1="-0.889" x2="-2.032" y2="0.889" layer="94"/>
<pin name="G" x="-7.62" y="0" visible="off" length="short" direction="pas"/>
<pin name="D" x="0" y="5.08" visible="off" length="short" direction="pas" rot="R270"/>
<pin name="S" x="0" y="-5.08" visible="off" length="short" direction="pas" rot="R90"/>
</symbol>
<symbol name="14POS_TERMINAL">
<wire x1="38.1" y1="0" x2="38.1" y2="2.54" width="0.254" layer="94"/>
<wire x1="38.1" y1="2.54" x2="35.56" y2="2.54" width="0.254" layer="94"/>
<wire x1="35.56" y1="2.54" x2="0" y2="2.54" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="35.56" y2="0" width="0.254" layer="94"/>
<wire x1="35.56" y1="0" x2="38.1" y2="0" width="0.254" layer="94"/>
<wire x1="35.56" y1="2.54" x2="35.56" y2="0" width="0.254" layer="94"/>
<text x="33.02" y="0" size="1.27" layer="95" font="vector" rot="MR180">&gt;NAME</text>
<pin name="P$1" x="35.56" y="7.62" visible="off" length="middle" rot="R270"/>
<pin name="P$2" x="33.02" y="7.62" visible="off" length="middle" rot="R270"/>
<pin name="P$3" x="30.48" y="7.62" visible="off" length="middle" rot="R270"/>
<pin name="P$4" x="27.94" y="7.62" visible="off" length="middle" rot="R270"/>
<pin name="P$5" x="25.4" y="7.62" visible="off" length="middle" rot="R270"/>
<pin name="P$6" x="22.86" y="7.62" visible="off" length="middle" rot="R270"/>
<pin name="P$7" x="20.32" y="7.62" visible="off" length="middle" rot="R270"/>
<pin name="P$8" x="17.78" y="7.62" visible="off" length="middle" rot="R270"/>
<pin name="P$9" x="15.24" y="7.62" visible="off" length="middle" rot="R270"/>
<pin name="P$10" x="12.7" y="7.62" visible="off" length="middle" rot="R270"/>
<pin name="P$11" x="10.16" y="7.62" visible="off" length="middle" rot="R270"/>
<pin name="P$12" x="7.62" y="7.62" visible="off" length="middle" rot="R270"/>
<pin name="P$13" x="5.08" y="7.62" visible="off" length="middle" rot="R270"/>
<pin name="P$14" x="2.54" y="7.62" visible="off" length="middle" rot="R270"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="FRAME-A3" prefix="FRAME">
<description>&lt;b&gt;Schematic Frame&lt;/b&gt;&lt;p&gt;
A3 Larger Frame</description>
<gates>
<gate name="G$1" symbol="A3L-LOC" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="DRIVER_TLC5947">
<gates>
<gate name="G$1" symbol="DRIVER_TLC5947" x="0" y="0"/>
</gates>
<devices>
<device name="_QFN32" package="QFN-32">
<connects>
<connect gate="G$1" pin="1.OUT0" pad="1"/>
<connect gate="G$1" pin="10.OUT9" pad="10"/>
<connect gate="G$1" pin="11.OUT10" pad="11"/>
<connect gate="G$1" pin="12.OUT11" pad="12"/>
<connect gate="G$1" pin="13.OUT12" pad="13"/>
<connect gate="G$1" pin="14.OUT13" pad="14"/>
<connect gate="G$1" pin="15.OUT14" pad="15"/>
<connect gate="G$1" pin="16.OUT15" pad="16"/>
<connect gate="G$1" pin="17.OUT16" pad="17"/>
<connect gate="G$1" pin="18.OUT17" pad="18"/>
<connect gate="G$1" pin="19.OUT18" pad="19"/>
<connect gate="G$1" pin="2.OUT1" pad="2"/>
<connect gate="G$1" pin="20.OUT19" pad="20"/>
<connect gate="G$1" pin="21.OUT20" pad="21"/>
<connect gate="G$1" pin="22.OUT21" pad="22"/>
<connect gate="G$1" pin="23.OUT22" pad="23"/>
<connect gate="G$1" pin="24.OUT23" pad="24"/>
<connect gate="G$1" pin="25.SOUT" pad="25"/>
<connect gate="G$1" pin="26.XLAT" pad="26"/>
<connect gate="G$1" pin="27.IREF" pad="27"/>
<connect gate="G$1" pin="28.VCC" pad="28"/>
<connect gate="G$1" pin="29.GND" pad="29"/>
<connect gate="G$1" pin="3.OUT2" pad="3"/>
<connect gate="G$1" pin="30.BLANK" pad="30"/>
<connect gate="G$1" pin="31.SCLK" pad="31"/>
<connect gate="G$1" pin="32.SIN" pad="32"/>
<connect gate="G$1" pin="4.OUT3" pad="4"/>
<connect gate="G$1" pin="5.OUT4" pad="5"/>
<connect gate="G$1" pin="6.OUT5" pad="6"/>
<connect gate="G$1" pin="7.OUT6" pad="7"/>
<connect gate="G$1" pin="8.OUT7" pad="8"/>
<connect gate="G$1" pin="9.OUT8" pad="9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="GND" prefix="GND">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="R" prefix="R" uservalue="yes">
<description>&lt;b&gt;Resistor&lt;/b&gt;
Basic schematic elements and footprints for 0603, 1206, and PTH 1/10th watt (small) resistors.</description>
<gates>
<gate name="G$1" symbol="R" x="0" y="0"/>
</gates>
<devices>
<device name="_AXIAL_762MM" package="AXIAL-0.3">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2010" package="R2010">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0805" package="0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0402" package="0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0603" package="0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1206" package="1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_AXIAL_5MM" package="AXIAL-5MM">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_AXIAL_254MM" package="2POS_254MM_1R_THRU">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0201" package="0201">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="MATRIX_GTM2088ARGB-21">
<gates>
<gate name="G$1" symbol="MATRIX_GTM2088ARGB-21" x="0" y="0"/>
</gates>
<devices>
<device name="" package="RGB_MATRIX">
<connects>
<connect gate="G$1" pin="1.B1" pad="P$1"/>
<connect gate="G$1" pin="10.R2" pad="P$10"/>
<connect gate="G$1" pin="11.R3" pad="P$11"/>
<connect gate="G$1" pin="12.R4" pad="P$12"/>
<connect gate="G$1" pin="13.R5" pad="P$13"/>
<connect gate="G$1" pin="14.R6" pad="P$14"/>
<connect gate="G$1" pin="15.R7" pad="P$15"/>
<connect gate="G$1" pin="16.R8" pad="P$16"/>
<connect gate="G$1" pin="17.ROW1" pad="P$17"/>
<connect gate="G$1" pin="18.ROW2" pad="P$18"/>
<connect gate="G$1" pin="19.ROW3" pad="P$19"/>
<connect gate="G$1" pin="2.B2" pad="P$2"/>
<connect gate="G$1" pin="20.ROW4" pad="P$20"/>
<connect gate="G$1" pin="21.G8" pad="P$21"/>
<connect gate="G$1" pin="22.G7" pad="P$22"/>
<connect gate="G$1" pin="23.G6" pad="P$23"/>
<connect gate="G$1" pin="24.G5" pad="P$24"/>
<connect gate="G$1" pin="25.G4" pad="P$25"/>
<connect gate="G$1" pin="26.G3" pad="P$26"/>
<connect gate="G$1" pin="27.G2" pad="P$27"/>
<connect gate="G$1" pin="28.G1" pad="P$28"/>
<connect gate="G$1" pin="29.ROW5" pad="P$29"/>
<connect gate="G$1" pin="3.B3" pad="P$3"/>
<connect gate="G$1" pin="30.ROW6" pad="P$30"/>
<connect gate="G$1" pin="31.ROW7" pad="P$31"/>
<connect gate="G$1" pin="32.ROW8" pad="P$32"/>
<connect gate="G$1" pin="4.B4" pad="P$4"/>
<connect gate="G$1" pin="5.B5" pad="P$5"/>
<connect gate="G$1" pin="6.B6" pad="P$6"/>
<connect gate="G$1" pin="7.B7" pad="P$7"/>
<connect gate="G$1" pin="8.B8" pad="P$8"/>
<connect gate="G$1" pin="9.R1" pad="P$9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="C" prefix="C" uservalue="yes">
<description>&lt;b&gt;Capacitor&lt;/b&gt;
Standard 0603 ceramic capacitor, and 0.1" leaded capacitor.</description>
<gates>
<gate name="G$1" symbol="C" x="0" y="0"/>
</gates>
<devices>
<device name="0805" package="0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0402" package="0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0603" package="0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1206" package="1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_TH_SMALL" package="CAP-PTH-SMALL">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0201" package="0201">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="MOSFET-PCHANNEL" prefix="Q">
<gates>
<gate name="G$1" symbol="MOSFET-PCHANNEL" x="0" y="0"/>
</gates>
<devices>
<device name="SMD" package="SOT23-3">
<connects>
<connect gate="G$1" pin="D" pad="3"/>
<connect gate="G$1" pin="G" pad="1"/>
<connect gate="G$1" pin="S" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_DFN1006" package="DFN-3-1006">
<connects>
<connect gate="G$1" pin="D" pad="P1"/>
<connect gate="G$1" pin="G" pad="P3"/>
<connect gate="G$1" pin="S" pad="P2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_SOT323" package="SOT323">
<connects>
<connect gate="G$1" pin="D" pad="3"/>
<connect gate="G$1" pin="G" pad="1"/>
<connect gate="G$1" pin="S" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="14POS_TERMINAL">
<gates>
<gate name="G$1" symbol="14POS_TERMINAL" x="0" y="0"/>
</gates>
<devices>
<device name="_2R_THRU" package="14POS_254MM_2R_THRU">
<connects>
<connect gate="G$1" pin="P$1" pad="P$1"/>
<connect gate="G$1" pin="P$10" pad="P$10"/>
<connect gate="G$1" pin="P$11" pad="P$11"/>
<connect gate="G$1" pin="P$12" pad="P$12"/>
<connect gate="G$1" pin="P$13" pad="P$13"/>
<connect gate="G$1" pin="P$14" pad="P$14"/>
<connect gate="G$1" pin="P$2" pad="P$2"/>
<connect gate="G$1" pin="P$3" pad="P$3"/>
<connect gate="G$1" pin="P$4" pad="P$4"/>
<connect gate="G$1" pin="P$5" pad="P$5"/>
<connect gate="G$1" pin="P$6" pad="P$6"/>
<connect gate="G$1" pin="P$7" pad="P$7"/>
<connect gate="G$1" pin="P$8" pad="P$8"/>
<connect gate="G$1" pin="P$9" pad="P$9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_2R_THRU_ALT" package="14POS_254MM_2R_THRU">
<connects>
<connect gate="G$1" pin="P$1" pad="P$1"/>
<connect gate="G$1" pin="P$10" pad="P$10"/>
<connect gate="G$1" pin="P$11" pad="P$6"/>
<connect gate="G$1" pin="P$12" pad="P$9"/>
<connect gate="G$1" pin="P$13" pad="P$7"/>
<connect gate="G$1" pin="P$14" pad="P$8"/>
<connect gate="G$1" pin="P$2" pad="P$14"/>
<connect gate="G$1" pin="P$3" pad="P$2"/>
<connect gate="G$1" pin="P$4" pad="P$13"/>
<connect gate="G$1" pin="P$5" pad="P$3"/>
<connect gate="G$1" pin="P$6" pad="P$12"/>
<connect gate="G$1" pin="P$7" pad="P$4"/>
<connect gate="G$1" pin="P$8" pad="P$11"/>
<connect gate="G$1" pin="P$9" pad="P$5"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_AXK5F14547YG_SOCKET" package="14POS_0.5MM_2R_SMD_AXK5F14547YG_SOCKET">
<connects>
<connect gate="G$1" pin="P$1" pad="P1"/>
<connect gate="G$1" pin="P$10" pad="P10"/>
<connect gate="G$1" pin="P$11" pad="P11"/>
<connect gate="G$1" pin="P$12" pad="P12"/>
<connect gate="G$1" pin="P$13" pad="P13"/>
<connect gate="G$1" pin="P$14" pad="P14"/>
<connect gate="G$1" pin="P$2" pad="P2"/>
<connect gate="G$1" pin="P$3" pad="P3"/>
<connect gate="G$1" pin="P$4" pad="P4"/>
<connect gate="G$1" pin="P$5" pad="P5"/>
<connect gate="G$1" pin="P$6" pad="P6"/>
<connect gate="G$1" pin="P$7" pad="P7"/>
<connect gate="G$1" pin="P$8" pad="P8"/>
<connect gate="G$1" pin="P$9" pad="P9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_AXK6F14347YG_HEADER" package="14POS_0.5MM_2R_SMD_AXK6F14347YG_HEADER">
<connects>
<connect gate="G$1" pin="P$1" pad="P1"/>
<connect gate="G$1" pin="P$10" pad="P10"/>
<connect gate="G$1" pin="P$11" pad="P11"/>
<connect gate="G$1" pin="P$12" pad="P12"/>
<connect gate="G$1" pin="P$13" pad="P13"/>
<connect gate="G$1" pin="P$14" pad="P14"/>
<connect gate="G$1" pin="P$2" pad="P2"/>
<connect gate="G$1" pin="P$3" pad="P3"/>
<connect gate="G$1" pin="P$4" pad="P4"/>
<connect gate="G$1" pin="P$5" pad="P5"/>
<connect gate="G$1" pin="P$6" pad="P6"/>
<connect gate="G$1" pin="P$7" pad="P7"/>
<connect gate="G$1" pin="P$8" pad="P8"/>
<connect gate="G$1" pin="P$9" pad="P9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_254MM_1R_THRU" package="14POS_254MM_1R_THRU">
<connects>
<connect gate="G$1" pin="P$1" pad="P$1"/>
<connect gate="G$1" pin="P$10" pad="P$10"/>
<connect gate="G$1" pin="P$11" pad="P$11"/>
<connect gate="G$1" pin="P$12" pad="P$12"/>
<connect gate="G$1" pin="P$13" pad="P$13"/>
<connect gate="G$1" pin="P$14" pad="P$14"/>
<connect gate="G$1" pin="P$2" pad="P$2"/>
<connect gate="G$1" pin="P$3" pad="P$3"/>
<connect gate="G$1" pin="P$4" pad="P$4"/>
<connect gate="G$1" pin="P$5" pad="P$5"/>
<connect gate="G$1" pin="P$6" pad="P$6"/>
<connect gate="G$1" pin="P$7" pad="P$7"/>
<connect gate="G$1" pin="P$8" pad="P$8"/>
<connect gate="G$1" pin="P$9" pad="P$9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_2MM_1R_THRU" package="14POS_2MM_1R_THRU">
<connects>
<connect gate="G$1" pin="P$1" pad="P$1"/>
<connect gate="G$1" pin="P$10" pad="P$10"/>
<connect gate="G$1" pin="P$11" pad="P$11"/>
<connect gate="G$1" pin="P$12" pad="P$12"/>
<connect gate="G$1" pin="P$13" pad="P$13"/>
<connect gate="G$1" pin="P$14" pad="P$14"/>
<connect gate="G$1" pin="P$2" pad="P$2"/>
<connect gate="G$1" pin="P$3" pad="P$3"/>
<connect gate="G$1" pin="P$4" pad="P$4"/>
<connect gate="G$1" pin="P$5" pad="P$5"/>
<connect gate="G$1" pin="P$6" pad="P$6"/>
<connect gate="G$1" pin="P$7" pad="P$7"/>
<connect gate="G$1" pin="P$8" pad="P$8"/>
<connect gate="G$1" pin="P$9" pad="P$9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SparkFun">
<description>&lt;h3&gt;SparkFun Electronics' preferred foot prints&lt;/h3&gt;
We've spent an enormous amount of time creating and checking these footprints and parts. If you enjoy using this library, please buy one of our products at www.sparkfun.com.
&lt;br&gt;&lt;br&gt;
&lt;b&gt;Licensing:&lt;/b&gt; CC v3.0 Share-Alike You are welcome to use this library for commercial purposes. For attribution, we ask that when you begin to sell your device using our footprint, you email us with a link to the product being sold. We want bragging rights that we helped (in a very small part) to create your 8th world wonder. We would like the opportunity to feature your device on our homepage.</description>
<packages>
<package name="DIL16">
<description>&lt;b&gt;Dual In Line Package&lt;/b&gt;</description>
<wire x1="10.16" y1="2.921" x2="-10.16" y2="2.921" width="0.3048" layer="21"/>
<wire x1="-10.16" y1="-2.921" x2="10.16" y2="-2.921" width="0.3048" layer="21"/>
<wire x1="10.16" y1="2.921" x2="10.16" y2="-2.921" width="0.3048" layer="21"/>
<wire x1="-10.16" y1="2.921" x2="-10.16" y2="1.016" width="0.3048" layer="21"/>
<wire x1="-10.16" y1="-2.921" x2="-10.16" y2="-1.016" width="0.3048" layer="21"/>
<wire x1="-10.16" y1="-1.016" x2="-10.16" y2="1.016" width="0.3048" layer="21" curve="180"/>
<pad name="1" x="-8.89" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="2" x="-6.35" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="3" x="-3.81" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="4" x="-1.27" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="5" x="1.27" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="6" x="3.81" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="7" x="6.35" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="8" x="8.89" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="9" x="8.89" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="10" x="6.35" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="11" x="3.81" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="12" x="1.27" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="13" x="-1.27" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="14" x="-3.81" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="15" x="-6.35" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="16" x="-8.89" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<text x="-10.541" y="-2.921" size="1.27" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="-7.493" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="SO16">
<description>&lt;b&gt;Small Outline package&lt;/b&gt; 150 mil</description>
<wire x1="4.699" y1="1.9558" x2="-4.699" y2="1.9558" width="0.1524" layer="21"/>
<wire x1="4.699" y1="-1.9558" x2="5.08" y2="-1.5748" width="0.1524" layer="21" curve="90"/>
<wire x1="-4.699" y1="1.9558" x2="-5.08" y2="1.5748" width="0.1524" layer="21" curve="90"/>
<wire x1="5.08" y1="1.5748" x2="4.699" y2="1.9558" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.08" y1="-1.5748" x2="-4.699" y2="-1.9558" width="0.1524" layer="21" curve="90"/>
<wire x1="-4.699" y1="-1.9558" x2="4.699" y2="-1.9558" width="0.1524" layer="21"/>
<wire x1="5.08" y1="-1.5748" x2="5.08" y2="1.5748" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="1.5748" x2="-5.08" y2="0.508" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="0.508" x2="-5.08" y2="-0.508" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="-0.508" x2="-5.08" y2="-1.5748" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="-0.508" x2="-5.08" y2="0.508" width="0.1524" layer="21" curve="180"/>
<wire x1="-5.08" y1="-1.6002" x2="5.08" y2="-1.6002" width="0.0508" layer="21"/>
<smd name="1" x="-4.445" y="-2.8" dx="0.6" dy="1.2" layer="1"/>
<smd name="2" x="-3.175" y="-2.8" dx="0.6" dy="1.2" layer="1"/>
<smd name="3" x="-1.905" y="-2.8" dx="0.6" dy="1.2" layer="1"/>
<smd name="4" x="-0.635" y="-2.8" dx="0.6" dy="1.2" layer="1"/>
<smd name="5" x="0.635" y="-2.8" dx="0.6" dy="1.2" layer="1"/>
<smd name="6" x="1.905" y="-2.8" dx="0.6" dy="1.2" layer="1"/>
<smd name="7" x="3.175" y="-2.8" dx="0.6" dy="1.2" layer="1"/>
<smd name="8" x="4.445" y="-2.8" dx="0.6" dy="1.2" layer="1"/>
<smd name="9" x="4.445" y="2.8" dx="0.6" dy="1.2" layer="1"/>
<smd name="10" x="3.175" y="2.8" dx="0.6" dy="1.2" layer="1"/>
<smd name="11" x="1.905" y="2.8" dx="0.6" dy="1.2" layer="1"/>
<smd name="12" x="0.635" y="2.8" dx="0.6" dy="1.2" layer="1"/>
<smd name="13" x="-0.635" y="2.8" dx="0.6" dy="1.2" layer="1"/>
<smd name="14" x="-1.905" y="2.8" dx="0.6" dy="1.2" layer="1"/>
<smd name="15" x="-3.175" y="2.8" dx="0.6" dy="1.2" layer="1"/>
<smd name="16" x="-4.445" y="2.8" dx="0.6" dy="1.2" layer="1"/>
<text x="-4.064" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<text x="-5.461" y="-1.778" size="1.27" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<rectangle x1="-0.889" y1="1.9558" x2="-0.381" y2="3.0988" layer="51"/>
<rectangle x1="-4.699" y1="-3.0988" x2="-4.191" y2="-1.9558" layer="51"/>
<rectangle x1="-3.429" y1="-3.0988" x2="-2.921" y2="-1.9558" layer="51"/>
<rectangle x1="-2.159" y1="-3.0734" x2="-1.651" y2="-1.9304" layer="51"/>
<rectangle x1="-0.889" y1="-3.0988" x2="-0.381" y2="-1.9558" layer="51"/>
<rectangle x1="-2.159" y1="1.9558" x2="-1.651" y2="3.0988" layer="51"/>
<rectangle x1="-3.429" y1="1.9558" x2="-2.921" y2="3.0988" layer="51"/>
<rectangle x1="-4.699" y1="1.9558" x2="-4.191" y2="3.0988" layer="51"/>
<rectangle x1="0.381" y1="-3.0988" x2="0.889" y2="-1.9558" layer="51"/>
<rectangle x1="1.651" y1="-3.0988" x2="2.159" y2="-1.9558" layer="51"/>
<rectangle x1="2.921" y1="-3.0988" x2="3.429" y2="-1.9558" layer="51"/>
<rectangle x1="4.191" y1="-3.0988" x2="4.699" y2="-1.9558" layer="51"/>
<rectangle x1="0.381" y1="1.9558" x2="0.889" y2="3.0988" layer="51"/>
<rectangle x1="1.651" y1="1.9558" x2="2.159" y2="3.0988" layer="51"/>
<rectangle x1="2.921" y1="1.9558" x2="3.429" y2="3.0988" layer="51"/>
<rectangle x1="4.191" y1="1.9558" x2="4.699" y2="3.0988" layer="51"/>
</package>
<package name="LCC20">
<description>&lt;b&gt;Leadless Chip Carrier&lt;/b&gt;&lt;p&gt; Ceramic Package</description>
<wire x1="-0.4001" y1="4.4" x2="-0.87" y2="4.4" width="0.2032" layer="51"/>
<wire x1="-3.3" y1="4.4" x2="-4.4" y2="3.3" width="0.2032" layer="51"/>
<wire x1="-0.4001" y1="4.3985" x2="0.4001" y2="4.3985" width="0.2032" layer="51" curve="180"/>
<wire x1="-1.6701" y1="4.3985" x2="-0.8699" y2="4.3985" width="0.2032" layer="51" curve="180"/>
<wire x1="-4.3985" y1="2.14" x2="-4.3985" y2="2.94" width="0.2032" layer="51" curve="180"/>
<wire x1="-2.9401" y1="4.4" x2="-3.3" y2="4.4" width="0.2032" layer="51"/>
<wire x1="0.87" y1="4.4" x2="0.4001" y2="4.4" width="0.2032" layer="51"/>
<wire x1="0.87" y1="4.3985" x2="1.67" y2="4.3985" width="0.2032" layer="51" curve="180"/>
<wire x1="-4.4" y1="3.3" x2="-4.4" y2="2.9401" width="0.2032" layer="51"/>
<wire x1="-4.4" y1="2.14" x2="-4.4" y2="1.6701" width="0.2032" layer="51"/>
<wire x1="-4.3985" y1="0.87" x2="-4.3985" y2="1.67" width="0.2032" layer="51" curve="180"/>
<wire x1="-4.3985" y1="-0.4001" x2="-4.3985" y2="0.4001" width="0.2032" layer="51" curve="180"/>
<wire x1="-4.3985" y1="-1.6701" x2="-4.3985" y2="-0.8699" width="0.2032" layer="51" curve="180"/>
<wire x1="-4.4" y1="0.87" x2="-4.4" y2="0.4001" width="0.2032" layer="51"/>
<wire x1="-4.4" y1="-0.4001" x2="-4.4" y2="-0.87" width="0.2032" layer="51"/>
<wire x1="-4.4" y1="-2.9401" x2="-4.4" y2="-4.4" width="0.2032" layer="51"/>
<wire x1="-4.4" y1="-4.4" x2="-4.4" y2="-4.4099" width="0.2032" layer="51"/>
<wire x1="2.14" y1="4.3985" x2="2.94" y2="4.3985" width="0.2032" layer="51" curve="180"/>
<wire x1="2.14" y1="4.4" x2="1.6701" y2="4.4" width="0.2032" layer="51"/>
<wire x1="4.4" y1="4.4" x2="2.9401" y2="4.4" width="0.2032" layer="51"/>
<wire x1="0.4001" y1="-4.4" x2="0.87" y2="-4.4" width="0.2032" layer="51"/>
<wire x1="0.4001" y1="-4.3985" x2="-0.4001" y2="-4.3985" width="0.2032" layer="51" curve="180"/>
<wire x1="1.67" y1="-4.3985" x2="0.87" y2="-4.3985" width="0.2032" layer="51" curve="180"/>
<wire x1="2.9401" y1="-4.4" x2="4.4" y2="-4.4" width="0.2032" layer="51"/>
<wire x1="-0.87" y1="-4.4" x2="-0.4001" y2="-4.4" width="0.2032" layer="51"/>
<wire x1="-0.8699" y1="-4.3985" x2="-1.6701" y2="-4.3985" width="0.2032" layer="51" curve="180"/>
<wire x1="-2.1399" y1="-4.3985" x2="-2.9401" y2="-4.3985" width="0.2032" layer="51" curve="180"/>
<wire x1="-2.14" y1="-4.4" x2="-1.6701" y2="-4.4" width="0.2032" layer="51"/>
<wire x1="-4.4" y1="-4.4" x2="-2.9401" y2="-4.4" width="0.2032" layer="51"/>
<wire x1="4.4" y1="0.4001" x2="4.4" y2="0.87" width="0.2032" layer="51"/>
<wire x1="4.3985" y1="0.4001" x2="4.3985" y2="-0.4001" width="0.2032" layer="51" curve="180"/>
<wire x1="4.3985" y1="1.6701" x2="4.3985" y2="0.8699" width="0.2032" layer="51" curve="180"/>
<wire x1="4.4" y1="2.9401" x2="4.4" y2="4.4" width="0.2032" layer="51"/>
<wire x1="4.4" y1="-0.87" x2="4.4" y2="-0.4001" width="0.2032" layer="51"/>
<wire x1="4.3985" y1="-0.87" x2="4.3985" y2="-1.67" width="0.2032" layer="51" curve="180"/>
<wire x1="4.3985" y1="-2.14" x2="4.3985" y2="-2.94" width="0.2032" layer="51" curve="180"/>
<wire x1="4.4" y1="-2.14" x2="4.4" y2="-1.6701" width="0.2032" layer="51"/>
<wire x1="4.4" y1="-4.4" x2="4.4" y2="-2.9401" width="0.2032" layer="51"/>
<wire x1="-2.9401" y1="4.3985" x2="-2.1399" y2="4.3985" width="0.2032" layer="51" curve="180"/>
<wire x1="-1.6701" y1="4.4" x2="-2.14" y2="4.4" width="0.2032" layer="51"/>
<wire x1="-4.3985" y1="-2.9401" x2="-4.3985" y2="-2.1399" width="0.2032" layer="51" curve="180"/>
<wire x1="-4.4" y1="-1.6701" x2="-4.4" y2="-2.14" width="0.2032" layer="51"/>
<wire x1="1.6701" y1="-4.4" x2="2.14" y2="-4.4" width="0.2032" layer="51"/>
<wire x1="2.94" y1="-4.3985" x2="2.14" y2="-4.3985" width="0.2032" layer="51" curve="180"/>
<wire x1="4.3985" y1="2.9401" x2="4.3985" y2="2.1399" width="0.2032" layer="51" curve="180"/>
<wire x1="4.4" y1="1.6701" x2="4.4" y2="2.14" width="0.2032" layer="51"/>
<smd name="1" x="0" y="3.8001" dx="0.8" dy="3.4" layer="1"/>
<smd name="2" x="-1.27" y="4.5001" dx="0.8" dy="2" layer="1"/>
<smd name="3" x="-2.54" y="4.5001" dx="0.8" dy="2" layer="1"/>
<smd name="4" x="-4.5001" y="2.54" dx="2" dy="0.8" layer="1"/>
<smd name="5" x="-4.5001" y="1.27" dx="2" dy="0.8" layer="1"/>
<smd name="6" x="-4.5001" y="0" dx="2" dy="0.8" layer="1"/>
<smd name="7" x="-4.5001" y="-1.27" dx="2" dy="0.8" layer="1"/>
<smd name="8" x="-4.5001" y="-2.54" dx="2" dy="0.8" layer="1"/>
<smd name="9" x="-2.54" y="-4.5001" dx="0.8" dy="2" layer="1"/>
<smd name="10" x="-1.27" y="-4.5001" dx="0.8" dy="2" layer="1"/>
<smd name="11" x="0" y="-4.5001" dx="0.8" dy="2" layer="1"/>
<smd name="12" x="1.27" y="-4.5001" dx="0.8" dy="2" layer="1"/>
<smd name="13" x="2.54" y="-4.5001" dx="0.8" dy="2" layer="1"/>
<smd name="14" x="4.5001" y="-2.54" dx="2" dy="0.8" layer="1"/>
<smd name="15" x="4.5001" y="-1.27" dx="2" dy="0.8" layer="1"/>
<smd name="16" x="4.5001" y="0" dx="2" dy="0.8" layer="1"/>
<smd name="17" x="4.5001" y="1.27" dx="2" dy="0.8" layer="1"/>
<smd name="18" x="4.5001" y="2.54" dx="2" dy="0.8" layer="1"/>
<smd name="19" x="2.54" y="4.5001" dx="0.8" dy="2" layer="1"/>
<smd name="20" x="1.27" y="4.5001" dx="0.8" dy="2" layer="1"/>
<text x="-3.4971" y="5.811" size="1.778" layer="25">&gt;NAME</text>
<text x="-3.9751" y="-7.6871" size="1.778" layer="27">&gt;VALUE</text>
</package>
<package name="TSSOP16">
<description>&lt;b&gt;TSOP16&lt;/b&gt;&lt;p&gt;
thin small outline package</description>
<wire x1="3.1" y1="-2.4" x2="-3" y2="-2.4" width="0.2032" layer="51"/>
<wire x1="-3" y1="2.6" x2="3.1" y2="2.6" width="0.2032" layer="51"/>
<wire x1="3.1" y1="-2.4" x2="3.1" y2="2.6" width="0.2032" layer="51"/>
<wire x1="-3" y1="2.6" x2="-2.8" y2="2.6" width="0.2032" layer="21"/>
<wire x1="-3" y1="2.6" x2="-3" y2="0.5" width="0.2032" layer="21"/>
<wire x1="-3" y1="-0.5" x2="-3" y2="-2.4" width="0.2032" layer="21"/>
<wire x1="-3" y1="-2.4" x2="-2.8" y2="-2.4" width="0.2032" layer="21"/>
<wire x1="2.9" y1="-2.4" x2="3.1" y2="-2.4" width="0.2032" layer="21"/>
<wire x1="3.1" y1="-2.4" x2="3.1" y2="2.6" width="0.2032" layer="21"/>
<wire x1="3.1" y1="2.6" x2="2.9" y2="2.6" width="0.2032" layer="21"/>
<wire x1="-3" y1="0.5" x2="-3" y2="-0.5" width="0.2032" layer="21" curve="-180"/>
<smd name="1" x="-2.225" y="-2.85" dx="0.4" dy="1.6" layer="1"/>
<smd name="2" x="-1.575" y="-2.85" dx="0.4" dy="1.6" layer="1"/>
<smd name="3" x="-0.925" y="-2.85" dx="0.4" dy="1.6" layer="1"/>
<smd name="4" x="-0.275" y="-2.85" dx="0.4" dy="1.6" layer="1"/>
<smd name="5" x="0.375" y="-2.85" dx="0.4" dy="1.6" layer="1"/>
<smd name="6" x="1.025" y="-2.85" dx="0.4" dy="1.6" layer="1"/>
<smd name="7" x="1.675" y="-2.85" dx="0.4" dy="1.6" layer="1"/>
<smd name="8" x="2.325" y="-2.85" dx="0.4" dy="1.6" layer="1"/>
<smd name="9" x="2.325" y="3.05" dx="0.4" dy="1.6" layer="1"/>
<smd name="10" x="1.675" y="3.05" dx="0.4" dy="1.6" layer="1"/>
<smd name="11" x="1.025" y="3.05" dx="0.4" dy="1.6" layer="1"/>
<smd name="12" x="0.375" y="3.05" dx="0.4" dy="1.6" layer="1"/>
<smd name="13" x="-0.275" y="3.05" dx="0.4" dy="1.6" layer="1"/>
<smd name="14" x="-0.925" y="3.05" dx="0.4" dy="1.6" layer="1"/>
<smd name="15" x="-1.575" y="3.05" dx="0.4" dy="1.6" layer="1"/>
<smd name="16" x="-2.225" y="3.05" dx="0.4" dy="1.6" layer="1"/>
<text x="-1.524" y="-1.27" size="0.6096" layer="25">&gt;NAME</text>
<text x="-1.524" y="0.889" size="0.6096" layer="27">&gt;VALUE</text>
<rectangle x1="-2.425" y1="-3.3501" x2="-2.0249" y2="-2.5299" layer="51"/>
<rectangle x1="-1.775" y1="-3.3501" x2="-1.3749" y2="-2.5299" layer="51"/>
<rectangle x1="-1.125" y1="-3.3501" x2="-0.725" y2="-2.5299" layer="51"/>
<rectangle x1="-0.475" y1="-3.3501" x2="-0.075" y2="-2.5299" layer="51"/>
<rectangle x1="0.175" y1="-3.3501" x2="0.575" y2="-2.5299" layer="51"/>
<rectangle x1="0.825" y1="-3.3501" x2="1.225" y2="-2.5299" layer="51"/>
<rectangle x1="1.4749" y1="-3.3501" x2="1.875" y2="-2.5299" layer="51"/>
<rectangle x1="2.1249" y1="-3.3501" x2="2.525" y2="-2.5299" layer="51"/>
<rectangle x1="2.1249" y1="2.7299" x2="2.525" y2="3.5501" layer="51"/>
<rectangle x1="1.4749" y1="2.7299" x2="1.875" y2="3.5501" layer="51"/>
<rectangle x1="0.825" y1="2.7299" x2="1.225" y2="3.5501" layer="51"/>
<rectangle x1="0.175" y1="2.7299" x2="0.575" y2="3.5501" layer="51"/>
<rectangle x1="-0.475" y1="2.7299" x2="-0.075" y2="3.5501" layer="51"/>
<rectangle x1="-1.125" y1="2.7299" x2="-0.725" y2="3.5501" layer="51"/>
<rectangle x1="-1.775" y1="2.7299" x2="-1.3749" y2="3.5501" layer="51"/>
<rectangle x1="-2.425" y1="2.7299" x2="-2.0249" y2="3.5501" layer="51"/>
<rectangle x1="-2.425" y1="-3.3501" x2="-2.0249" y2="-2.5299" layer="51"/>
<rectangle x1="-1.775" y1="-3.3501" x2="-1.3749" y2="-2.5299" layer="51"/>
<rectangle x1="-1.125" y1="-3.3501" x2="-0.725" y2="-2.5299" layer="51"/>
<rectangle x1="-0.475" y1="-3.3501" x2="-0.075" y2="-2.5299" layer="51"/>
<rectangle x1="0.175" y1="-3.3501" x2="0.575" y2="-2.5299" layer="51"/>
<rectangle x1="0.825" y1="-3.3501" x2="1.225" y2="-2.5299" layer="51"/>
<rectangle x1="1.4749" y1="-3.3501" x2="1.875" y2="-2.5299" layer="51"/>
<rectangle x1="2.1249" y1="-3.3501" x2="2.525" y2="-2.5299" layer="51"/>
<rectangle x1="2.1249" y1="2.7299" x2="2.525" y2="3.5501" layer="51"/>
<rectangle x1="1.4749" y1="2.7299" x2="1.875" y2="3.5501" layer="51"/>
<rectangle x1="0.825" y1="2.7299" x2="1.225" y2="3.5501" layer="51"/>
<rectangle x1="0.175" y1="2.7299" x2="0.575" y2="3.5501" layer="51"/>
<rectangle x1="-0.475" y1="2.7299" x2="-0.075" y2="3.5501" layer="51"/>
<rectangle x1="-1.125" y1="2.7299" x2="-0.725" y2="3.5501" layer="51"/>
<rectangle x1="-1.775" y1="2.7299" x2="-1.3749" y2="3.5501" layer="51"/>
<rectangle x1="-2.425" y1="2.7299" x2="-2.0249" y2="3.5501" layer="51"/>
</package>
<package name="LED-TRICOLOR-5050">
<description>&lt;H3&gt;5050 SMD RGB LED&lt;/h3&gt;
5.0mm x 5.0mm, 2.6mm thickness</description>
<wire x1="-2.5" y1="-2.5" x2="-2.5" y2="2.5" width="0.127" layer="51"/>
<wire x1="-2.5" y1="2.5" x2="2.5" y2="2.5" width="0.127" layer="51"/>
<wire x1="2.5" y1="2.5" x2="2.5" y2="-2.5" width="0.127" layer="51"/>
<wire x1="2.5" y1="-2.5" x2="-2.5" y2="-2.5" width="0.127" layer="51"/>
<wire x1="-1" y1="2.5" x2="1" y2="2.5" width="0.127" layer="21"/>
<wire x1="-1" y1="-2.5" x2="1" y2="-2.5" width="0.127" layer="21"/>
<wire x1="-2.5" y1="0.7" x2="-2.5" y2="0.9" width="0.127" layer="21"/>
<wire x1="-2.5" y1="-0.9" x2="-2.5" y2="-0.7" width="0.127" layer="21"/>
<wire x1="2.5" y1="-0.9" x2="2.5" y2="-0.7" width="0.127" layer="21"/>
<wire x1="2.5" y1="0.7" x2="2.5" y2="0.9" width="0.127" layer="21"/>
<circle x="-0.7" y="2" radius="0.2236" width="0.127" layer="21"/>
<circle x="0" y="0" radius="2" width="0.127" layer="51"/>
<smd name="2" x="-2.2" y="1.8" dx="2" dy="1.4" layer="1"/>
<smd name="1" x="2.2" y="1.8" dx="2" dy="1.4" layer="1"/>
<smd name="6" x="-2.2" y="-1.8" dx="2" dy="1.4" layer="1"/>
<smd name="5" x="2.2" y="-1.8" dx="2" dy="1.4" layer="1"/>
<smd name="4" x="-2.2" y="0" dx="2" dy="1" layer="1"/>
<smd name="3" x="2.2" y="0" dx="2" dy="1" layer="1"/>
<text x="-2.54" y="2.794" size="0.762" layer="25">&gt;Name</text>
<text x="-2.54" y="-3.556" size="0.762" layer="27">&gt;Value</text>
<rectangle x1="1.7" y1="-0.45" x2="2.7" y2="0.45" layer="51"/>
<rectangle x1="1.7" y1="1.15" x2="2.7" y2="2.05" layer="51"/>
<rectangle x1="1.7" y1="-2.05" x2="2.7" y2="-1.15" layer="51"/>
<rectangle x1="-2.7" y1="1.15" x2="-1.7" y2="2.05" layer="51" rot="R180"/>
<rectangle x1="-2.7" y1="-0.45" x2="-1.7" y2="0.45" layer="51" rot="R180"/>
<rectangle x1="-2.7" y1="-2.05" x2="-1.7" y2="-1.15" layer="51" rot="R180"/>
</package>
</packages>
<symbols>
<symbol name="74595">
<wire x1="-5.08" y1="-15.24" x2="10.16" y2="-15.24" width="0.4064" layer="94"/>
<wire x1="10.16" y1="-15.24" x2="10.16" y2="12.7" width="0.4064" layer="94"/>
<wire x1="10.16" y1="12.7" x2="-5.08" y2="12.7" width="0.4064" layer="94"/>
<wire x1="-5.08" y1="12.7" x2="-5.08" y2="-15.24" width="0.4064" layer="94"/>
<text x="-5.08" y="13.335" size="1.778" layer="95">&gt;NAME</text>
<text x="-5.08" y="-17.78" size="1.778" layer="96">&gt;VALUE</text>
<pin name="QB" x="15.24" y="5.08" length="middle" direction="hiz" rot="R180"/>
<pin name="QC" x="15.24" y="2.54" length="middle" direction="hiz" rot="R180"/>
<pin name="QD" x="15.24" y="0" length="middle" direction="hiz" rot="R180"/>
<pin name="QE" x="15.24" y="-2.54" length="middle" direction="hiz" rot="R180"/>
<pin name="QF" x="15.24" y="-5.08" length="middle" direction="hiz" rot="R180"/>
<pin name="QG" x="15.24" y="-7.62" length="middle" direction="hiz" rot="R180"/>
<pin name="QH" x="15.24" y="-10.16" length="middle" direction="hiz" rot="R180"/>
<pin name="SCL" x="-10.16" y="2.54" length="middle" direction="in" function="dot"/>
<pin name="SCK" x="-10.16" y="5.08" length="middle" direction="in" function="clk"/>
<pin name="RCK" x="-10.16" y="-2.54" length="middle" direction="in" function="clk"/>
<pin name="G" x="-10.16" y="-7.62" length="middle" direction="in" function="dot"/>
<pin name="SER" x="-10.16" y="10.16" length="middle" direction="in"/>
<pin name="QA" x="15.24" y="7.62" length="middle" direction="hiz" rot="R180"/>
<pin name="QH*" x="15.24" y="-12.7" length="middle" direction="hiz" rot="R180"/>
<pin name="GND" x="-10.16" y="-12.7" length="middle" direction="pwr"/>
<pin name="VCC" x="15.24" y="10.16" length="middle" direction="pwr" rot="R180"/>
</symbol>
<symbol name="3.3V">
<wire x1="0.762" y1="1.27" x2="0" y2="2.54" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="-0.762" y2="1.27" width="0.254" layer="94"/>
<text x="-1.016" y="3.556" size="1.778" layer="96">&gt;VALUE</text>
<pin name="3.3V" x="0" y="0" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
<symbol name="LED-TRICOLOR-INDV">
<wire x1="12.7" y1="5.08" x2="12.7" y2="-5.08" width="0.254" layer="94"/>
<wire x1="12.7" y1="-5.08" x2="-12.7" y2="-5.08" width="0.254" layer="94"/>
<wire x1="-12.7" y1="-5.08" x2="-12.7" y2="5.08" width="0.254" layer="94"/>
<wire x1="-12.7" y1="5.08" x2="12.7" y2="5.08" width="0.254" layer="94"/>
<wire x1="0.508" y1="-1.27" x2="0.508" y2="0" width="0.254" layer="94"/>
<wire x1="0.508" y1="0" x2="0.508" y2="1.27" width="0.254" layer="94"/>
<wire x1="0.508" y1="1.27" x2="-0.762" y2="0" width="0.254" layer="94"/>
<wire x1="-0.762" y1="0" x2="0.508" y2="-1.27" width="0.254" layer="94"/>
<wire x1="-0.762" y1="-1.27" x2="-0.762" y2="0" width="0.254" layer="94"/>
<wire x1="-0.762" y1="0" x2="-0.762" y2="1.27" width="0.254" layer="94"/>
<wire x1="-0.762" y1="0" x2="-2.032" y2="0" width="0.254" layer="94"/>
<wire x1="0.508" y1="0" x2="1.778" y2="0" width="0.254" layer="94"/>
<wire x1="0.508" y1="1.778" x2="0.508" y2="3.048" width="0.254" layer="94"/>
<wire x1="0.508" y1="3.048" x2="0.508" y2="4.318" width="0.254" layer="94"/>
<wire x1="0.508" y1="4.318" x2="-0.762" y2="3.048" width="0.254" layer="94"/>
<wire x1="-0.762" y1="3.048" x2="0.508" y2="1.778" width="0.254" layer="94"/>
<wire x1="-0.762" y1="1.778" x2="-0.762" y2="3.048" width="0.254" layer="94"/>
<wire x1="-0.762" y1="3.048" x2="-0.762" y2="4.318" width="0.254" layer="94"/>
<wire x1="-0.762" y1="3.048" x2="-2.032" y2="3.048" width="0.254" layer="94"/>
<wire x1="0.508" y1="3.048" x2="1.778" y2="3.048" width="0.254" layer="94"/>
<wire x1="0.508" y1="-4.318" x2="0.508" y2="-3.048" width="0.254" layer="94"/>
<wire x1="0.508" y1="-3.048" x2="0.508" y2="-1.778" width="0.254" layer="94"/>
<wire x1="0.508" y1="-1.778" x2="-0.762" y2="-3.048" width="0.254" layer="94"/>
<wire x1="-0.762" y1="-3.048" x2="0.508" y2="-4.318" width="0.254" layer="94"/>
<wire x1="-0.762" y1="-4.318" x2="-0.762" y2="-3.048" width="0.254" layer="94"/>
<wire x1="-0.762" y1="-3.048" x2="-0.762" y2="-1.778" width="0.254" layer="94"/>
<wire x1="-0.762" y1="-3.048" x2="-2.032" y2="-3.048" width="0.254" layer="94"/>
<wire x1="0.508" y1="-3.048" x2="1.778" y2="-3.048" width="0.254" layer="94"/>
<text x="-12.7" y="5.842" size="1.778" layer="95">&gt;NAME</text>
<text x="-12.7" y="-7.62" size="1.778" layer="95">&gt;VALUE</text>
<pin name="GRN-A" x="15.24" y="0" visible="pin" length="short" rot="R180"/>
<pin name="BLU-A" x="15.24" y="2.54" visible="pin" length="short" rot="R180"/>
<pin name="RED-C" x="-15.24" y="-2.54" visible="pin" length="short"/>
<pin name="RED-A" x="15.24" y="-2.54" visible="pin" length="short" rot="R180"/>
<pin name="BLU-C" x="-15.24" y="2.54" visible="pin" length="short"/>
<pin name="GRN-C" x="-15.24" y="0" visible="pin" length="short"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="74*595" prefix="IC">
<description>8-bit &lt;b&gt;SHIFT REGISTER&lt;/b&gt;, output latch</description>
<gates>
<gate name="A" symbol="74595" x="22.86" y="0"/>
</gates>
<devices>
<device name="N" package="DIL16">
<connects>
<connect gate="A" pin="G" pad="13"/>
<connect gate="A" pin="GND" pad="8"/>
<connect gate="A" pin="QA" pad="15"/>
<connect gate="A" pin="QB" pad="1"/>
<connect gate="A" pin="QC" pad="2"/>
<connect gate="A" pin="QD" pad="3"/>
<connect gate="A" pin="QE" pad="4"/>
<connect gate="A" pin="QF" pad="5"/>
<connect gate="A" pin="QG" pad="6"/>
<connect gate="A" pin="QH" pad="7"/>
<connect gate="A" pin="QH*" pad="9"/>
<connect gate="A" pin="RCK" pad="12"/>
<connect gate="A" pin="SCK" pad="11"/>
<connect gate="A" pin="SCL" pad="10"/>
<connect gate="A" pin="SER" pad="14"/>
<connect gate="A" pin="VCC" pad="16"/>
</connects>
<technologies>
<technology name="LS"/>
</technologies>
</device>
<device name="D" package="SO16">
<connects>
<connect gate="A" pin="G" pad="13"/>
<connect gate="A" pin="GND" pad="8"/>
<connect gate="A" pin="QA" pad="15"/>
<connect gate="A" pin="QB" pad="1"/>
<connect gate="A" pin="QC" pad="2"/>
<connect gate="A" pin="QD" pad="3"/>
<connect gate="A" pin="QE" pad="4"/>
<connect gate="A" pin="QF" pad="5"/>
<connect gate="A" pin="QG" pad="6"/>
<connect gate="A" pin="QH" pad="7"/>
<connect gate="A" pin="QH*" pad="9"/>
<connect gate="A" pin="RCK" pad="12"/>
<connect gate="A" pin="SCK" pad="11"/>
<connect gate="A" pin="SCL" pad="10"/>
<connect gate="A" pin="SER" pad="14"/>
<connect gate="A" pin="VCC" pad="16"/>
</connects>
<technologies>
<technology name="LS"/>
</technologies>
</device>
<device name="FK" package="LCC20">
<connects>
<connect gate="A" pin="G" pad="17"/>
<connect gate="A" pin="GND" pad="10"/>
<connect gate="A" pin="QA" pad="19"/>
<connect gate="A" pin="QB" pad="2"/>
<connect gate="A" pin="QC" pad="3"/>
<connect gate="A" pin="QD" pad="4"/>
<connect gate="A" pin="QE" pad="5"/>
<connect gate="A" pin="QF" pad="7"/>
<connect gate="A" pin="QG" pad="8"/>
<connect gate="A" pin="QH" pad="9"/>
<connect gate="A" pin="QH*" pad="12"/>
<connect gate="A" pin="RCK" pad="15"/>
<connect gate="A" pin="SCK" pad="14"/>
<connect gate="A" pin="SCL" pad="13"/>
<connect gate="A" pin="SER" pad="18"/>
<connect gate="A" pin="VCC" pad="20"/>
</connects>
<technologies>
<technology name="LS"/>
</technologies>
</device>
<device name="" package="TSSOP16">
<connects>
<connect gate="A" pin="G" pad="13"/>
<connect gate="A" pin="GND" pad="8"/>
<connect gate="A" pin="QA" pad="15"/>
<connect gate="A" pin="QB" pad="1"/>
<connect gate="A" pin="QC" pad="2"/>
<connect gate="A" pin="QD" pad="3"/>
<connect gate="A" pin="QE" pad="4"/>
<connect gate="A" pin="QF" pad="5"/>
<connect gate="A" pin="QG" pad="6"/>
<connect gate="A" pin="QH" pad="7"/>
<connect gate="A" pin="QH*" pad="9"/>
<connect gate="A" pin="RCK" pad="12"/>
<connect gate="A" pin="SCK" pad="11"/>
<connect gate="A" pin="SCL" pad="10"/>
<connect gate="A" pin="SER" pad="14"/>
<connect gate="A" pin="VCC" pad="16"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="3.3V" prefix="P+">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="3.3V" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="LED-TRICOLOR-5050" prefix="LED">
<gates>
<gate name="G$1" symbol="LED-TRICOLOR-INDV" x="0" y="0"/>
</gates>
<devices>
<device name="" package="LED-TRICOLOR-5050">
<connects>
<connect gate="G$1" pin="BLU-A" pad="3"/>
<connect gate="G$1" pin="BLU-C" pad="4"/>
<connect gate="G$1" pin="GRN-A" pad="1"/>
<connect gate="G$1" pin="GRN-C" pad="6"/>
<connect gate="G$1" pin="RED-A" pad="2"/>
<connect gate="G$1" pin="RED-C" pad="5"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="FRAME1" library="aditya_main" deviceset="FRAME-A3" device=""/>
<part name="DRIVER" library="aditya_main" deviceset="DRIVER_TLC5947" device="_QFN32"/>
<part name="GND1" library="aditya_main" deviceset="GND" device=""/>
<part name="R1" library="aditya_main" deviceset="R" device="0402"/>
<part name="U$2" library="aditya_main" deviceset="MATRIX_GTM2088ARGB-21" device=""/>
<part name="SHIFTY" library="SparkFun" deviceset="74*595" device="D" technology="LS"/>
<part name="GND3" library="aditya_main" deviceset="GND" device=""/>
<part name="C1" library="aditya_main" deviceset="C" device="0402" value="0.1uF"/>
<part name="C2" library="aditya_main" deviceset="C" device="0402" value="0.1uF"/>
<part name="C3" library="aditya_main" deviceset="C" device="0402" value="1uF"/>
<part name="C4" library="aditya_main" deviceset="C" device="0402" value="1uF"/>
<part name="C5" library="aditya_main" deviceset="C" device="0603" value="10uF"/>
<part name="C6" library="aditya_main" deviceset="C" device="0603" value="10uF"/>
<part name="GND6" library="aditya_main" deviceset="GND" device=""/>
<part name="Q1" library="aditya_main" deviceset="MOSFET-PCHANNEL" device="_SOT323"/>
<part name="R2" library="aditya_main" deviceset="R" device="0402" value="10K"/>
<part name="P+1" library="SparkFun" deviceset="3.3V" device=""/>
<part name="R3" library="aditya_main" deviceset="R" device="0402" value="10K"/>
<part name="R4" library="aditya_main" deviceset="R" device="0402" value="10K"/>
<part name="R5" library="aditya_main" deviceset="R" device="0402" value="10K"/>
<part name="R6" library="aditya_main" deviceset="R" device="0402" value="10K"/>
<part name="R7" library="aditya_main" deviceset="R" device="0402" value="10K"/>
<part name="R8" library="aditya_main" deviceset="R" device="0402" value="10K"/>
<part name="R9" library="aditya_main" deviceset="R" device="0402" value="10K"/>
<part name="Q2" library="aditya_main" deviceset="MOSFET-PCHANNEL" device="_SOT323"/>
<part name="Q3" library="aditya_main" deviceset="MOSFET-PCHANNEL" device="_SOT323"/>
<part name="Q4" library="aditya_main" deviceset="MOSFET-PCHANNEL" device="_SOT323"/>
<part name="Q5" library="aditya_main" deviceset="MOSFET-PCHANNEL" device="_SOT323"/>
<part name="Q6" library="aditya_main" deviceset="MOSFET-PCHANNEL" device="_SOT323"/>
<part name="Q7" library="aditya_main" deviceset="MOSFET-PCHANNEL" device="_SOT323"/>
<part name="Q8" library="aditya_main" deviceset="MOSFET-PCHANNEL" device="_SOT323"/>
<part name="U$1" library="aditya_main" deviceset="14POS_TERMINAL" device="_254MM_1R_THRU"/>
<part name="U$3" library="aditya_main" deviceset="14POS_TERMINAL" device="_254MM_1R_THRU"/>
<part name="P+2" library="SparkFun" deviceset="3.3V" device=""/>
<part name="P+3" library="SparkFun" deviceset="3.3V" device=""/>
<part name="U$4" library="aditya_main" deviceset="14POS_TERMINAL" device="_2MM_1R_THRU" value="14POS_TERMINAL_2MM_1R_THRU"/>
<part name="U$5" library="aditya_main" deviceset="14POS_TERMINAL" device="_2MM_1R_THRU" value="14POS_TERMINAL_2MM_1R_THRU"/>
<part name="U$6" library="aditya_main" deviceset="14POS_TERMINAL" device="_2MM_1R_THRU" value="14POS_TERMINAL_2MM_1R_THRU"/>
<part name="R10" library="aditya_main" deviceset="R" device="0402" value="0"/>
<part name="R11" library="aditya_main" deviceset="R" device="0402" value="0"/>
<part name="R12" library="aditya_main" deviceset="R" device="0402" value="0"/>
<part name="R13" library="aditya_main" deviceset="R" device="0402" value="0"/>
<part name="U$7" library="aditya_main" deviceset="14POS_TERMINAL" device="_2MM_1R_THRU" value="14POS_TERMINAL_2MM_1R_THRU"/>
<part name="C7" library="aditya_main" deviceset="C" device="0603" value="10uF"/>
<part name="C8" library="aditya_main" deviceset="C" device="0603" value="10uF"/>
<part name="C9" library="aditya_main" deviceset="C" device="0402" value="1uF"/>
<part name="C10" library="aditya_main" deviceset="C" device="0402" value="1uF"/>
<part name="GND2" library="aditya_main" deviceset="GND" device=""/>
<part name="U$9" library="aditya_main" deviceset="14POS_TERMINAL" device="_2MM_1R_THRU" value="14POS_TERMINAL_2MM_1R_THRU"/>
<part name="LED1" library="SparkFun" deviceset="LED-TRICOLOR-5050" device=""/>
<part name="LED2" library="SparkFun" deviceset="LED-TRICOLOR-5050" device=""/>
<part name="LED3" library="SparkFun" deviceset="LED-TRICOLOR-5050" device=""/>
<part name="LED4" library="SparkFun" deviceset="LED-TRICOLOR-5050" device=""/>
<part name="LED5" library="SparkFun" deviceset="LED-TRICOLOR-5050" device=""/>
<part name="LED6" library="SparkFun" deviceset="LED-TRICOLOR-5050" device=""/>
<part name="LED7" library="SparkFun" deviceset="LED-TRICOLOR-5050" device=""/>
<part name="LED8" library="SparkFun" deviceset="LED-TRICOLOR-5050" device=""/>
</parts>
<sheets>
<sheet>
<plain>
<wire x1="10.16" y1="142.24" x2="96.52" y2="142.24" width="0.1524" layer="95" style="shortdash"/>
<text x="15.24" y="132.08" size="5.08" layer="95">DRIVER</text>
<wire x1="10.16" y1="10.16" x2="10.16" y2="142.24" width="0.1524" layer="95" style="shortdash"/>
<wire x1="10.16" y1="10.16" x2="96.52" y2="10.16" width="0.1524" layer="95" style="shortdash"/>
<wire x1="96.52" y1="10.16" x2="96.52" y2="142.24" width="0.1524" layer="95" style="shortdash"/>
<wire x1="101.6" y1="10.16" x2="101.6" y2="142.24" width="0.1524" layer="95" style="shortdash"/>
<wire x1="101.6" y1="142.24" x2="180.34" y2="142.24" width="0.1524" layer="95" style="shortdash"/>
<wire x1="101.6" y1="10.16" x2="180.34" y2="10.16" width="0.1524" layer="95" style="shortdash"/>
<wire x1="180.34" y1="10.16" x2="180.34" y2="142.24" width="0.1524" layer="95" style="shortdash"/>
<text x="106.68" y="132.08" size="5.08" layer="95">MATRIX</text>
<wire x1="101.6" y1="147.32" x2="101.6" y2="248.92" width="0.1524" layer="95" style="shortdash"/>
<wire x1="101.6" y1="248.92" x2="378.46" y2="248.92" width="0.1524" layer="95" style="shortdash"/>
<wire x1="378.46" y1="147.32" x2="378.46" y2="248.92" width="0.1524" layer="95" style="shortdash"/>
<wire x1="101.6" y1="147.32" x2="378.46" y2="147.32" width="0.1524" layer="95" style="shortdash"/>
<text x="106.68" y="238.76" size="5.08" layer="95">ROW DRIVER</text>
<wire x1="10.16" y1="147.32" x2="10.16" y2="248.92" width="0.1524" layer="95" style="shortdash"/>
<wire x1="10.16" y1="248.92" x2="96.52" y2="248.92" width="0.1524" layer="95" style="shortdash"/>
<wire x1="96.52" y1="147.32" x2="96.52" y2="248.92" width="0.1524" layer="95" style="shortdash"/>
<text x="15.24" y="238.76" size="5.08" layer="95">ROW SHIFT</text>
<wire x1="10.16" y1="147.32" x2="96.52" y2="147.32" width="0.1524" layer="95" style="shortdash"/>
<wire x1="185.42" y1="78.74" x2="185.42" y2="142.24" width="0.1524" layer="95" style="shortdash"/>
<wire x1="236.22" y1="78.74" x2="236.22" y2="142.24" width="0.1524" layer="95" style="shortdash"/>
<wire x1="185.42" y1="78.74" x2="236.22" y2="78.74" width="0.1524" layer="95" style="shortdash"/>
<wire x1="185.42" y1="142.24" x2="236.22" y2="142.24" width="0.1524" layer="95" style="shortdash"/>
<text x="190.5" y="132.08" size="5.08" layer="95">IN SIDE</text>
<text x="45.72" y="175.26" size="1.778" layer="97">Actually a 74HC595.</text>
<wire x1="297.18" y1="78.74" x2="297.18" y2="142.24" width="0.1524" layer="95" style="shortdash"/>
<wire x1="297.18" y1="142.24" x2="378.46" y2="142.24" width="0.1524" layer="95" style="shortdash"/>
<text x="302.26" y="132.08" size="5.08" layer="95">DECUPS</text>
<wire x1="378.46" y1="78.74" x2="378.46" y2="142.24" width="0.1524" layer="95" style="shortdash"/>
<wire x1="297.18" y1="78.74" x2="378.46" y2="78.74" width="0.1524" layer="95" style="shortdash"/>
<wire x1="241.3" y1="78.74" x2="241.3" y2="142.24" width="0.1524" layer="95" style="shortdash"/>
<wire x1="292.1" y1="78.74" x2="292.1" y2="142.24" width="0.1524" layer="95" style="shortdash"/>
<wire x1="241.3" y1="78.74" x2="292.1" y2="78.74" width="0.1524" layer="95" style="shortdash"/>
<wire x1="241.3" y1="142.24" x2="292.1" y2="142.24" width="0.1524" layer="95" style="shortdash"/>
<text x="246.38" y="132.08" size="5.08" layer="95">OUT SIDE</text>
<wire x1="185.42" y1="10.16" x2="185.42" y2="73.66" width="0.1524" layer="95" style="shortdash"/>
<wire x1="256.54" y1="10.16" x2="256.54" y2="73.66" width="0.1524" layer="95" style="shortdash"/>
<wire x1="185.42" y1="10.16" x2="256.54" y2="10.16" width="0.1524" layer="95" style="shortdash"/>
<wire x1="185.42" y1="73.66" x2="256.54" y2="73.66" width="0.1524" layer="95" style="shortdash"/>
<text x="190.5" y="63.5" size="5.08" layer="95">BOTTOM</text>
<wire x1="261.62" y1="10.16" x2="261.62" y2="73.66" width="0.1524" layer="95" style="shortdash"/>
<wire x1="320.04" y1="10.16" x2="320.04" y2="73.66" width="0.1524" layer="95" style="shortdash"/>
<wire x1="261.62" y1="73.66" x2="320.04" y2="73.66" width="0.1524" layer="95" style="shortdash"/>
<text x="266.7" y="63.5" size="5.08" layer="95">TOP</text>
<wire x1="261.62" y1="10.16" x2="320.04" y2="10.16" width="0.1524" layer="95" style="shortdash"/>
<wire x1="325.12" y1="10.16" x2="325.12" y2="73.66" width="0.1524" layer="95" style="shortdash"/>
<wire x1="378.46" y1="10.16" x2="378.46" y2="73.66" width="0.1524" layer="95" style="shortdash"/>
<wire x1="325.12" y1="73.66" x2="378.46" y2="73.66" width="0.1524" layer="95" style="shortdash"/>
<text x="330.2" y="63.5" size="5.08" layer="95">SIDE</text>
<wire x1="325.12" y1="10.16" x2="378.46" y2="10.16" width="0.1524" layer="95" style="shortdash"/>
</plain>
<instances>
<instance part="FRAME1" gate="G$1" x="0" y="0"/>
<instance part="DRIVER" gate="G$1" x="55.88" y="71.12"/>
<instance part="GND1" gate="1" x="15.24" y="22.86"/>
<instance part="R1" gate="G$1" x="15.24" y="45.72" rot="R90"/>
<instance part="U$2" gate="G$1" x="139.7" y="78.74"/>
<instance part="SHIFTY" gate="A" x="50.8" y="195.58"/>
<instance part="GND3" gate="1" x="20.32" y="157.48"/>
<instance part="C1" gate="G$1" x="302.26" y="106.68"/>
<instance part="C2" gate="G$1" x="307.34" y="106.68"/>
<instance part="C3" gate="G$1" x="312.42" y="106.68"/>
<instance part="C4" gate="G$1" x="317.5" y="106.68"/>
<instance part="C5" gate="G$1" x="322.58" y="106.68"/>
<instance part="C6" gate="G$1" x="327.66" y="106.68"/>
<instance part="GND6" gate="1" x="302.26" y="93.98"/>
<instance part="Q1" gate="G$1" x="142.24" y="218.44" smashed="yes" rot="MR90">
<attribute name="NAME" x="142.24" y="223.52" size="1.778" layer="95" rot="MR0"/>
</instance>
<instance part="R2" gate="G$1" x="132.08" y="210.82" rot="R90"/>
<instance part="P+1" gate="G$1" x="86.36" y="226.06"/>
<instance part="R3" gate="G$1" x="132.08" y="170.18" rot="R90"/>
<instance part="R4" gate="G$1" x="195.58" y="210.82" rot="R90"/>
<instance part="R5" gate="G$1" x="195.58" y="170.18" rot="R90"/>
<instance part="R6" gate="G$1" x="259.08" y="210.82" rot="R90"/>
<instance part="R7" gate="G$1" x="259.08" y="170.18" rot="R90"/>
<instance part="R8" gate="G$1" x="322.58" y="210.82" rot="R90"/>
<instance part="R9" gate="G$1" x="322.58" y="170.18" rot="R90"/>
<instance part="Q2" gate="G$1" x="205.74" y="218.44" smashed="yes" rot="MR90">
<attribute name="NAME" x="205.74" y="223.52" size="1.778" layer="95" rot="MR0"/>
</instance>
<instance part="Q3" gate="G$1" x="269.24" y="218.44" smashed="yes" rot="MR90">
<attribute name="NAME" x="269.24" y="223.52" size="1.778" layer="95" rot="MR0"/>
</instance>
<instance part="Q4" gate="G$1" x="332.74" y="218.44" smashed="yes" rot="MR90">
<attribute name="NAME" x="332.74" y="223.52" size="1.778" layer="95" rot="MR0"/>
</instance>
<instance part="Q5" gate="G$1" x="142.24" y="177.8" smashed="yes" rot="MR90">
<attribute name="NAME" x="142.24" y="182.88" size="1.778" layer="95" rot="MR0"/>
</instance>
<instance part="Q6" gate="G$1" x="205.74" y="177.8" smashed="yes" rot="MR90">
<attribute name="NAME" x="205.74" y="182.88" size="1.778" layer="95" rot="MR0"/>
</instance>
<instance part="Q7" gate="G$1" x="269.24" y="177.8" smashed="yes" rot="MR90">
<attribute name="NAME" x="269.24" y="182.88" size="1.778" layer="95" rot="MR0"/>
</instance>
<instance part="Q8" gate="G$1" x="332.74" y="177.8" smashed="yes" rot="MR90">
<attribute name="NAME" x="332.74" y="182.88" size="1.778" layer="95" rot="MR0"/>
</instance>
<instance part="U$1" gate="G$1" x="190.5" y="86.36" rot="MR90"/>
<instance part="U$3" gate="G$1" x="246.38" y="86.36" rot="MR90"/>
<instance part="P+2" gate="G$1" x="15.24" y="121.92"/>
<instance part="P+3" gate="G$1" x="302.26" y="119.38"/>
<instance part="U$4" gate="G$1" x="190.5" y="17.78" rot="MR90"/>
<instance part="U$5" gate="G$1" x="223.52" y="17.78" rot="MR90"/>
<instance part="U$6" gate="G$1" x="266.7" y="17.78" rot="MR90"/>
<instance part="R10" gate="G$1" x="50.8" y="22.86" rot="R180"/>
<instance part="R11" gate="G$1" x="50.8" y="17.78" rot="R180"/>
<instance part="R12" gate="G$1" x="53.34" y="162.56" rot="R180"/>
<instance part="R13" gate="G$1" x="53.34" y="157.48" rot="R180"/>
<instance part="U$7" gate="G$1" x="304.8" y="17.78" rot="MR90"/>
<instance part="C7" gate="G$1" x="337.82" y="106.68"/>
<instance part="C8" gate="G$1" x="342.9" y="106.68"/>
<instance part="C9" gate="G$1" x="347.98" y="106.68"/>
<instance part="C10" gate="G$1" x="353.06" y="106.68"/>
<instance part="GND2" gate="1" x="337.82" y="93.98"/>
<instance part="U$9" gate="G$1" x="330.2" y="17.78" rot="MR90"/>
<instance part="LED1" gate="G$1" x="424.18" y="160.02"/>
<instance part="LED2" gate="G$1" x="457.2" y="160.02"/>
<instance part="LED3" gate="G$1" x="457.2" y="142.24"/>
<instance part="LED4" gate="G$1" x="424.18" y="142.24"/>
<instance part="LED5" gate="G$1" x="424.18" y="124.46"/>
<instance part="LED6" gate="G$1" x="457.2" y="124.46"/>
<instance part="LED7" gate="G$1" x="457.2" y="106.68"/>
<instance part="LED8" gate="G$1" x="424.18" y="106.68"/>
</instances>
<busses>
</busses>
<nets>
<net name="GND" class="0">
<segment>
<pinref part="DRIVER" gate="G$1" pin="29.GND"/>
<pinref part="GND1" gate="1" pin="GND"/>
<wire x1="35.56" y1="38.1" x2="15.24" y2="38.1" width="0.1524" layer="91"/>
<wire x1="15.24" y1="38.1" x2="15.24" y2="25.4" width="0.1524" layer="91"/>
<pinref part="R1" gate="G$1" pin="1"/>
<wire x1="15.24" y1="40.64" x2="15.24" y2="38.1" width="0.1524" layer="91"/>
<junction x="15.24" y="38.1"/>
</segment>
<segment>
<pinref part="SHIFTY" gate="A" pin="GND"/>
<pinref part="GND3" gate="1" pin="GND"/>
<wire x1="40.64" y1="182.88" x2="20.32" y2="182.88" width="0.1524" layer="91"/>
<wire x1="20.32" y1="182.88" x2="20.32" y2="160.02" width="0.1524" layer="91"/>
<pinref part="SHIFTY" gate="A" pin="G"/>
<wire x1="40.64" y1="187.96" x2="20.32" y2="187.96" width="0.1524" layer="91"/>
<wire x1="20.32" y1="187.96" x2="20.32" y2="182.88" width="0.1524" layer="91"/>
<junction x="20.32" y="182.88"/>
</segment>
<segment>
<pinref part="C1" gate="G$1" pin="2"/>
<wire x1="302.26" y1="104.14" x2="302.26" y2="101.6" width="0.1524" layer="91"/>
<pinref part="C2" gate="G$1" pin="2"/>
<wire x1="302.26" y1="101.6" x2="302.26" y2="96.52" width="0.1524" layer="91"/>
<wire x1="302.26" y1="101.6" x2="307.34" y2="101.6" width="0.1524" layer="91"/>
<wire x1="307.34" y1="101.6" x2="307.34" y2="104.14" width="0.1524" layer="91"/>
<junction x="302.26" y="101.6"/>
<pinref part="C3" gate="G$1" pin="2"/>
<wire x1="307.34" y1="101.6" x2="312.42" y2="101.6" width="0.1524" layer="91"/>
<wire x1="312.42" y1="101.6" x2="312.42" y2="104.14" width="0.1524" layer="91"/>
<junction x="307.34" y="101.6"/>
<pinref part="C4" gate="G$1" pin="2"/>
<wire x1="312.42" y1="101.6" x2="317.5" y2="101.6" width="0.1524" layer="91"/>
<wire x1="317.5" y1="101.6" x2="317.5" y2="104.14" width="0.1524" layer="91"/>
<junction x="312.42" y="101.6"/>
<pinref part="C5" gate="G$1" pin="2"/>
<wire x1="317.5" y1="101.6" x2="322.58" y2="101.6" width="0.1524" layer="91"/>
<wire x1="322.58" y1="101.6" x2="322.58" y2="104.14" width="0.1524" layer="91"/>
<junction x="317.5" y="101.6"/>
<pinref part="C6" gate="G$1" pin="2"/>
<wire x1="322.58" y1="101.6" x2="327.66" y2="101.6" width="0.1524" layer="91"/>
<wire x1="327.66" y1="101.6" x2="327.66" y2="104.14" width="0.1524" layer="91"/>
<junction x="322.58" y="101.6"/>
<pinref part="GND6" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="U$1" gate="G$1" pin="P$3"/>
<wire x1="226.06" y1="116.84" x2="198.12" y2="116.84" width="0.1524" layer="91"/>
<label x="203.2" y="116.84" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$1" gate="G$1" pin="P$4"/>
<wire x1="198.12" y1="114.3" x2="226.06" y2="114.3" width="0.1524" layer="91"/>
<label x="203.2" y="114.3" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$3" gate="G$1" pin="P$3"/>
<wire x1="281.94" y1="116.84" x2="254" y2="116.84" width="0.1524" layer="91"/>
<label x="259.08" y="116.84" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$3" gate="G$1" pin="P$4"/>
<wire x1="254" y1="114.3" x2="281.94" y2="114.3" width="0.1524" layer="91"/>
<label x="259.08" y="114.3" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$4" gate="G$1" pin="P$3"/>
<wire x1="218.44" y1="48.26" x2="198.12" y2="48.26" width="0.1524" layer="91"/>
<label x="203.2" y="48.26" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$4" gate="G$1" pin="P$4"/>
<wire x1="198.12" y1="45.72" x2="218.44" y2="45.72" width="0.1524" layer="91"/>
<label x="203.2" y="45.72" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$5" gate="G$1" pin="P$3"/>
<wire x1="251.46" y1="48.26" x2="231.14" y2="48.26" width="0.1524" layer="91"/>
<label x="236.22" y="48.26" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$5" gate="G$1" pin="P$4"/>
<wire x1="231.14" y1="45.72" x2="251.46" y2="45.72" width="0.1524" layer="91"/>
<label x="236.22" y="45.72" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$6" gate="G$1" pin="P$3"/>
<wire x1="294.64" y1="48.26" x2="274.32" y2="48.26" width="0.1524" layer="91"/>
<label x="279.4" y="48.26" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$6" gate="G$1" pin="P$4"/>
<wire x1="274.32" y1="45.72" x2="294.64" y2="45.72" width="0.1524" layer="91"/>
<label x="279.4" y="45.72" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="GND2" gate="1" pin="GND"/>
<pinref part="C7" gate="G$1" pin="2"/>
<wire x1="337.82" y1="96.52" x2="337.82" y2="101.6" width="0.1524" layer="91"/>
<pinref part="C8" gate="G$1" pin="2"/>
<wire x1="337.82" y1="101.6" x2="337.82" y2="104.14" width="0.1524" layer="91"/>
<wire x1="337.82" y1="101.6" x2="342.9" y2="101.6" width="0.1524" layer="91"/>
<wire x1="342.9" y1="101.6" x2="342.9" y2="104.14" width="0.1524" layer="91"/>
<junction x="337.82" y="101.6"/>
<pinref part="C9" gate="G$1" pin="2"/>
<wire x1="342.9" y1="101.6" x2="347.98" y2="101.6" width="0.1524" layer="91"/>
<wire x1="347.98" y1="101.6" x2="347.98" y2="104.14" width="0.1524" layer="91"/>
<junction x="342.9" y="101.6"/>
<pinref part="C10" gate="G$1" pin="2"/>
<wire x1="347.98" y1="101.6" x2="353.06" y2="101.6" width="0.1524" layer="91"/>
<wire x1="353.06" y1="101.6" x2="353.06" y2="104.14" width="0.1524" layer="91"/>
<junction x="347.98" y="101.6"/>
</segment>
</net>
<net name="N$1" class="0">
<segment>
<pinref part="DRIVER" gate="G$1" pin="27.IREF"/>
<pinref part="R1" gate="G$1" pin="2"/>
<wire x1="35.56" y1="53.34" x2="15.24" y2="53.34" width="0.1524" layer="91"/>
<wire x1="15.24" y1="53.34" x2="15.24" y2="50.8" width="0.1524" layer="91"/>
</segment>
</net>
<net name="SIN" class="0">
<segment>
<pinref part="DRIVER" gate="G$1" pin="32.SIN"/>
<wire x1="35.56" y1="101.6" x2="15.24" y2="101.6" width="0.1524" layer="91"/>
<label x="30.48" y="101.6" size="1.778" layer="95" rot="MR0"/>
</segment>
<segment>
<pinref part="U$1" gate="G$1" pin="P$5"/>
<wire x1="226.06" y1="111.76" x2="198.12" y2="111.76" width="0.1524" layer="91"/>
<label x="203.2" y="111.76" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$4" gate="G$1" pin="P$5"/>
<wire x1="218.44" y1="43.18" x2="198.12" y2="43.18" width="0.1524" layer="91"/>
<label x="203.2" y="43.18" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$5" gate="G$1" pin="P$5"/>
<wire x1="251.46" y1="43.18" x2="231.14" y2="43.18" width="0.1524" layer="91"/>
<label x="236.22" y="43.18" size="1.778" layer="95"/>
</segment>
</net>
<net name="SCLK" class="0">
<segment>
<pinref part="DRIVER" gate="G$1" pin="31.SCLK"/>
<wire x1="35.56" y1="99.06" x2="15.24" y2="99.06" width="0.1524" layer="91"/>
<label x="30.48" y="99.06" size="1.778" layer="95" rot="MR0"/>
</segment>
<segment>
<pinref part="U$1" gate="G$1" pin="P$6"/>
<wire x1="198.12" y1="109.22" x2="226.06" y2="109.22" width="0.1524" layer="91"/>
<label x="203.2" y="109.22" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$3" gate="G$1" pin="P$6"/>
<wire x1="254" y1="109.22" x2="281.94" y2="109.22" width="0.1524" layer="91"/>
<label x="259.08" y="109.22" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$4" gate="G$1" pin="P$6"/>
<wire x1="198.12" y1="40.64" x2="218.44" y2="40.64" width="0.1524" layer="91"/>
<label x="203.2" y="40.64" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$5" gate="G$1" pin="P$6"/>
<wire x1="231.14" y1="40.64" x2="251.46" y2="40.64" width="0.1524" layer="91"/>
<label x="236.22" y="40.64" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$6" gate="G$1" pin="P$6"/>
<wire x1="274.32" y1="40.64" x2="294.64" y2="40.64" width="0.1524" layer="91"/>
<label x="279.4" y="40.64" size="1.778" layer="95"/>
</segment>
</net>
<net name="BLANK" class="0">
<segment>
<pinref part="DRIVER" gate="G$1" pin="30.BLANK"/>
<wire x1="35.56" y1="96.52" x2="15.24" y2="96.52" width="0.1524" layer="91"/>
<label x="30.48" y="96.52" size="1.778" layer="95" rot="MR0"/>
</segment>
<segment>
<pinref part="U$1" gate="G$1" pin="P$7"/>
<wire x1="226.06" y1="106.68" x2="198.12" y2="106.68" width="0.1524" layer="91"/>
<label x="203.2" y="106.68" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$3" gate="G$1" pin="P$7"/>
<wire x1="281.94" y1="106.68" x2="254" y2="106.68" width="0.1524" layer="91"/>
<label x="259.08" y="106.68" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$4" gate="G$1" pin="P$7"/>
<wire x1="218.44" y1="38.1" x2="198.12" y2="38.1" width="0.1524" layer="91"/>
<label x="203.2" y="38.1" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$5" gate="G$1" pin="P$7"/>
<wire x1="251.46" y1="38.1" x2="231.14" y2="38.1" width="0.1524" layer="91"/>
<label x="236.22" y="38.1" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$6" gate="G$1" pin="P$7"/>
<wire x1="294.64" y1="38.1" x2="274.32" y2="38.1" width="0.1524" layer="91"/>
<label x="279.4" y="38.1" size="1.778" layer="95"/>
</segment>
</net>
<net name="XLAT" class="0">
<segment>
<pinref part="DRIVER" gate="G$1" pin="26.XLAT"/>
<wire x1="35.56" y1="93.98" x2="15.24" y2="93.98" width="0.1524" layer="91"/>
<label x="30.48" y="93.98" size="1.778" layer="95" rot="MR0"/>
</segment>
<segment>
<pinref part="U$1" gate="G$1" pin="P$8"/>
<wire x1="198.12" y1="104.14" x2="226.06" y2="104.14" width="0.1524" layer="91"/>
<label x="203.2" y="104.14" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$3" gate="G$1" pin="P$8"/>
<wire x1="254" y1="104.14" x2="281.94" y2="104.14" width="0.1524" layer="91"/>
<label x="259.08" y="104.14" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$4" gate="G$1" pin="P$8"/>
<wire x1="198.12" y1="35.56" x2="218.44" y2="35.56" width="0.1524" layer="91"/>
<label x="203.2" y="35.56" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$5" gate="G$1" pin="P$8"/>
<wire x1="231.14" y1="35.56" x2="251.46" y2="35.56" width="0.1524" layer="91"/>
<label x="236.22" y="35.56" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$6" gate="G$1" pin="P$8"/>
<wire x1="274.32" y1="35.56" x2="294.64" y2="35.56" width="0.1524" layer="91"/>
<label x="279.4" y="35.56" size="1.778" layer="95"/>
</segment>
</net>
<net name="SOUT" class="0">
<segment>
<pinref part="DRIVER" gate="G$1" pin="25.SOUT"/>
<wire x1="76.2" y1="101.6" x2="91.44" y2="101.6" width="0.1524" layer="91"/>
<label x="81.28" y="101.6" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="R10" gate="G$1" pin="2"/>
<wire x1="45.72" y1="22.86" x2="40.64" y2="22.86" width="0.1524" layer="91"/>
<label x="25.4" y="22.86" size="1.778" layer="95"/>
<pinref part="R11" gate="G$1" pin="2"/>
<wire x1="40.64" y1="22.86" x2="22.86" y2="22.86" width="0.1524" layer="91"/>
<wire x1="45.72" y1="17.78" x2="40.64" y2="17.78" width="0.1524" layer="91"/>
<wire x1="40.64" y1="17.78" x2="40.64" y2="22.86" width="0.1524" layer="91"/>
<junction x="40.64" y="22.86"/>
</segment>
</net>
<net name="OUT0" class="0">
<segment>
<pinref part="DRIVER" gate="G$1" pin="1.OUT0"/>
<wire x1="76.2" y1="96.52" x2="91.44" y2="96.52" width="0.1524" layer="91"/>
<label x="81.28" y="96.52" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$2" gate="G$1" pin="1.B1"/>
<wire x1="119.38" y1="63.5" x2="106.68" y2="63.5" width="0.1524" layer="91"/>
<label x="114.3" y="63.5" size="1.778" layer="95" rot="MR0"/>
</segment>
</net>
<net name="OUT1" class="0">
<segment>
<pinref part="DRIVER" gate="G$1" pin="2.OUT1"/>
<wire x1="76.2" y1="93.98" x2="91.44" y2="93.98" width="0.1524" layer="91"/>
<label x="81.28" y="93.98" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$2" gate="G$1" pin="2.B2"/>
<wire x1="119.38" y1="60.96" x2="106.68" y2="60.96" width="0.1524" layer="91"/>
<label x="114.3" y="60.96" size="1.778" layer="95" rot="MR0"/>
</segment>
</net>
<net name="OUT2" class="0">
<segment>
<pinref part="DRIVER" gate="G$1" pin="3.OUT2"/>
<wire x1="76.2" y1="91.44" x2="91.44" y2="91.44" width="0.1524" layer="91"/>
<label x="81.28" y="91.44" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$2" gate="G$1" pin="3.B3"/>
<wire x1="119.38" y1="58.42" x2="106.68" y2="58.42" width="0.1524" layer="91"/>
<label x="114.3" y="58.42" size="1.778" layer="95" rot="MR0"/>
</segment>
</net>
<net name="OUT3" class="0">
<segment>
<pinref part="DRIVER" gate="G$1" pin="4.OUT3"/>
<wire x1="91.44" y1="88.9" x2="76.2" y2="88.9" width="0.1524" layer="91"/>
<label x="81.28" y="88.9" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$2" gate="G$1" pin="4.B4"/>
<wire x1="119.38" y1="55.88" x2="106.68" y2="55.88" width="0.1524" layer="91"/>
<label x="114.3" y="55.88" size="1.778" layer="95" rot="MR0"/>
</segment>
</net>
<net name="OUT4" class="0">
<segment>
<pinref part="DRIVER" gate="G$1" pin="5.OUT4"/>
<wire x1="76.2" y1="86.36" x2="91.44" y2="86.36" width="0.1524" layer="91"/>
<label x="81.28" y="86.36" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$2" gate="G$1" pin="5.B5"/>
<wire x1="119.38" y1="53.34" x2="106.68" y2="53.34" width="0.1524" layer="91"/>
<label x="114.3" y="53.34" size="1.778" layer="95" rot="MR0"/>
</segment>
</net>
<net name="OUT5" class="0">
<segment>
<pinref part="DRIVER" gate="G$1" pin="6.OUT5"/>
<wire x1="76.2" y1="83.82" x2="91.44" y2="83.82" width="0.1524" layer="91"/>
<label x="81.28" y="83.82" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$2" gate="G$1" pin="6.B6"/>
<wire x1="106.68" y1="50.8" x2="119.38" y2="50.8" width="0.1524" layer="91"/>
<label x="114.3" y="50.8" size="1.778" layer="95" rot="MR0"/>
</segment>
</net>
<net name="OUT6" class="0">
<segment>
<pinref part="DRIVER" gate="G$1" pin="7.OUT6"/>
<wire x1="76.2" y1="81.28" x2="91.44" y2="81.28" width="0.1524" layer="91"/>
<label x="81.28" y="81.28" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$2" gate="G$1" pin="7.B7"/>
<wire x1="119.38" y1="48.26" x2="106.68" y2="48.26" width="0.1524" layer="91"/>
<label x="114.3" y="48.26" size="1.778" layer="95" rot="MR0"/>
</segment>
</net>
<net name="OUT7" class="0">
<segment>
<pinref part="DRIVER" gate="G$1" pin="8.OUT7"/>
<wire x1="76.2" y1="78.74" x2="91.44" y2="78.74" width="0.1524" layer="91"/>
<label x="81.28" y="78.74" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$2" gate="G$1" pin="8.B8"/>
<wire x1="119.38" y1="45.72" x2="106.68" y2="45.72" width="0.1524" layer="91"/>
<label x="114.3" y="45.72" size="1.778" layer="95" rot="MR0"/>
</segment>
</net>
<net name="OUT8" class="0">
<segment>
<pinref part="DRIVER" gate="G$1" pin="9.OUT8"/>
<wire x1="76.2" y1="76.2" x2="91.44" y2="76.2" width="0.1524" layer="91"/>
<label x="81.28" y="76.2" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$2" gate="G$1" pin="9.R1"/>
<wire x1="119.38" y1="109.22" x2="106.68" y2="109.22" width="0.1524" layer="91"/>
<label x="114.3" y="109.22" size="1.778" layer="95" rot="MR0"/>
</segment>
</net>
<net name="OUT9" class="0">
<segment>
<pinref part="DRIVER" gate="G$1" pin="10.OUT9"/>
<wire x1="76.2" y1="73.66" x2="91.44" y2="73.66" width="0.1524" layer="91"/>
<label x="81.28" y="73.66" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$2" gate="G$1" pin="10.R2"/>
<wire x1="119.38" y1="106.68" x2="106.68" y2="106.68" width="0.1524" layer="91"/>
<label x="114.3" y="106.68" size="1.778" layer="95" rot="MR0"/>
</segment>
</net>
<net name="OUT10" class="0">
<segment>
<pinref part="DRIVER" gate="G$1" pin="11.OUT10"/>
<wire x1="76.2" y1="71.12" x2="91.44" y2="71.12" width="0.1524" layer="91"/>
<label x="81.28" y="71.12" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$2" gate="G$1" pin="11.R3"/>
<wire x1="106.68" y1="104.14" x2="119.38" y2="104.14" width="0.1524" layer="91"/>
<label x="114.3" y="104.14" size="1.778" layer="95" rot="MR0"/>
</segment>
</net>
<net name="OUT11" class="0">
<segment>
<pinref part="DRIVER" gate="G$1" pin="12.OUT11"/>
<wire x1="76.2" y1="68.58" x2="91.44" y2="68.58" width="0.1524" layer="91"/>
<label x="81.28" y="68.58" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$2" gate="G$1" pin="12.R4"/>
<wire x1="119.38" y1="101.6" x2="106.68" y2="101.6" width="0.1524" layer="91"/>
<label x="114.3" y="101.6" size="1.778" layer="95" rot="MR0"/>
</segment>
</net>
<net name="OUT12" class="0">
<segment>
<pinref part="DRIVER" gate="G$1" pin="13.OUT12"/>
<wire x1="76.2" y1="66.04" x2="91.44" y2="66.04" width="0.1524" layer="91"/>
<label x="81.28" y="66.04" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$2" gate="G$1" pin="13.R5"/>
<wire x1="106.68" y1="99.06" x2="119.38" y2="99.06" width="0.1524" layer="91"/>
<label x="114.3" y="99.06" size="1.778" layer="95" rot="MR0"/>
</segment>
</net>
<net name="OUT13" class="0">
<segment>
<pinref part="DRIVER" gate="G$1" pin="14.OUT13"/>
<wire x1="76.2" y1="63.5" x2="91.44" y2="63.5" width="0.1524" layer="91"/>
<label x="81.28" y="63.5" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$2" gate="G$1" pin="14.R6"/>
<wire x1="119.38" y1="96.52" x2="106.68" y2="96.52" width="0.1524" layer="91"/>
<label x="114.3" y="96.52" size="1.778" layer="95" rot="MR0"/>
</segment>
</net>
<net name="OUT14" class="0">
<segment>
<pinref part="DRIVER" gate="G$1" pin="15.OUT14"/>
<wire x1="76.2" y1="60.96" x2="91.44" y2="60.96" width="0.1524" layer="91"/>
<label x="81.28" y="60.96" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$2" gate="G$1" pin="15.R7"/>
<wire x1="119.38" y1="93.98" x2="106.68" y2="93.98" width="0.1524" layer="91"/>
<label x="114.3" y="93.98" size="1.778" layer="95" rot="MR0"/>
</segment>
</net>
<net name="OUT15" class="0">
<segment>
<pinref part="DRIVER" gate="G$1" pin="16.OUT15"/>
<wire x1="76.2" y1="58.42" x2="91.44" y2="58.42" width="0.1524" layer="91"/>
<label x="81.28" y="58.42" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$2" gate="G$1" pin="16.R8"/>
<wire x1="119.38" y1="91.44" x2="106.68" y2="91.44" width="0.1524" layer="91"/>
<label x="114.3" y="91.44" size="1.778" layer="95" rot="MR0"/>
</segment>
</net>
<net name="OUT16" class="0">
<segment>
<pinref part="DRIVER" gate="G$1" pin="17.OUT16"/>
<wire x1="76.2" y1="55.88" x2="91.44" y2="55.88" width="0.1524" layer="91"/>
<label x="81.28" y="55.88" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$2" gate="G$1" pin="21.G8"/>
<wire x1="119.38" y1="68.58" x2="106.68" y2="68.58" width="0.1524" layer="91"/>
<label x="114.3" y="68.58" size="1.778" layer="95" rot="MR0"/>
</segment>
</net>
<net name="OUT17" class="0">
<segment>
<pinref part="DRIVER" gate="G$1" pin="18.OUT17"/>
<wire x1="76.2" y1="53.34" x2="91.44" y2="53.34" width="0.1524" layer="91"/>
<label x="81.28" y="53.34" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$2" gate="G$1" pin="22.G7"/>
<wire x1="119.38" y1="71.12" x2="106.68" y2="71.12" width="0.1524" layer="91"/>
<label x="114.3" y="71.12" size="1.778" layer="95" rot="MR0"/>
</segment>
</net>
<net name="OUT18" class="0">
<segment>
<pinref part="DRIVER" gate="G$1" pin="19.OUT18"/>
<wire x1="76.2" y1="50.8" x2="91.44" y2="50.8" width="0.1524" layer="91"/>
<label x="81.28" y="50.8" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$2" gate="G$1" pin="23.G6"/>
<wire x1="119.38" y1="73.66" x2="106.68" y2="73.66" width="0.1524" layer="91"/>
<label x="114.3" y="73.66" size="1.778" layer="95" rot="MR0"/>
</segment>
</net>
<net name="OUT19" class="0">
<segment>
<pinref part="DRIVER" gate="G$1" pin="20.OUT19"/>
<wire x1="76.2" y1="48.26" x2="91.44" y2="48.26" width="0.1524" layer="91"/>
<label x="81.28" y="48.26" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$2" gate="G$1" pin="24.G5"/>
<wire x1="119.38" y1="76.2" x2="106.68" y2="76.2" width="0.1524" layer="91"/>
<label x="114.3" y="76.2" size="1.778" layer="95" rot="MR0"/>
</segment>
</net>
<net name="OUT20" class="0">
<segment>
<pinref part="DRIVER" gate="G$1" pin="21.OUT20"/>
<wire x1="76.2" y1="45.72" x2="91.44" y2="45.72" width="0.1524" layer="91"/>
<label x="81.28" y="45.72" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$2" gate="G$1" pin="25.G4"/>
<wire x1="119.38" y1="78.74" x2="106.68" y2="78.74" width="0.1524" layer="91"/>
<label x="114.3" y="78.74" size="1.778" layer="95" rot="MR0"/>
</segment>
</net>
<net name="OUT21" class="0">
<segment>
<pinref part="DRIVER" gate="G$1" pin="22.OUT21"/>
<wire x1="76.2" y1="43.18" x2="91.44" y2="43.18" width="0.1524" layer="91"/>
<label x="81.28" y="43.18" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$2" gate="G$1" pin="26.G3"/>
<wire x1="106.68" y1="81.28" x2="119.38" y2="81.28" width="0.1524" layer="91"/>
<label x="114.3" y="81.28" size="1.778" layer="95" rot="MR0"/>
</segment>
</net>
<net name="OUT22" class="0">
<segment>
<pinref part="DRIVER" gate="G$1" pin="23.OUT22"/>
<wire x1="76.2" y1="40.64" x2="91.44" y2="40.64" width="0.1524" layer="91"/>
<label x="81.28" y="40.64" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$2" gate="G$1" pin="27.G2"/>
<wire x1="119.38" y1="83.82" x2="106.68" y2="83.82" width="0.1524" layer="91"/>
<label x="114.3" y="83.82" size="1.778" layer="95" rot="MR0"/>
</segment>
</net>
<net name="OUT23" class="0">
<segment>
<pinref part="DRIVER" gate="G$1" pin="24.OUT23"/>
<wire x1="76.2" y1="38.1" x2="91.44" y2="38.1" width="0.1524" layer="91"/>
<label x="81.28" y="38.1" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$2" gate="G$1" pin="28.G1"/>
<wire x1="119.38" y1="86.36" x2="106.68" y2="86.36" width="0.1524" layer="91"/>
<label x="114.3" y="86.36" size="1.778" layer="95" rot="MR0"/>
</segment>
</net>
<net name="ROW1" class="0">
<segment>
<pinref part="U$2" gate="G$1" pin="17.ROW1"/>
<wire x1="160.02" y1="109.22" x2="175.26" y2="109.22" width="0.1524" layer="91"/>
<label x="165.1" y="109.22" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="Q1" gate="G$1" pin="D"/>
<wire x1="147.32" y1="218.44" x2="167.64" y2="218.44" width="0.1524" layer="91"/>
<label x="152.4" y="218.44" size="1.778" layer="95"/>
</segment>
</net>
<net name="ROW2" class="0">
<segment>
<pinref part="U$2" gate="G$1" pin="18.ROW2"/>
<wire x1="160.02" y1="106.68" x2="175.26" y2="106.68" width="0.1524" layer="91"/>
<label x="165.1" y="106.68" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="210.82" y1="218.44" x2="231.14" y2="218.44" width="0.1524" layer="91"/>
<label x="215.9" y="218.44" size="1.778" layer="95"/>
<pinref part="Q2" gate="G$1" pin="D"/>
</segment>
</net>
<net name="ROW3" class="0">
<segment>
<pinref part="U$2" gate="G$1" pin="19.ROW3"/>
<wire x1="175.26" y1="104.14" x2="160.02" y2="104.14" width="0.1524" layer="91"/>
<label x="165.1" y="104.14" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="274.32" y1="218.44" x2="294.64" y2="218.44" width="0.1524" layer="91"/>
<label x="279.4" y="218.44" size="1.778" layer="95"/>
<pinref part="Q3" gate="G$1" pin="D"/>
</segment>
</net>
<net name="ROW4" class="0">
<segment>
<pinref part="U$2" gate="G$1" pin="20.ROW4"/>
<wire x1="160.02" y1="101.6" x2="175.26" y2="101.6" width="0.1524" layer="91"/>
<label x="165.1" y="101.6" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="337.82" y1="218.44" x2="358.14" y2="218.44" width="0.1524" layer="91"/>
<label x="342.9" y="218.44" size="1.778" layer="95"/>
<pinref part="Q4" gate="G$1" pin="D"/>
</segment>
</net>
<net name="ROW5" class="0">
<segment>
<pinref part="U$2" gate="G$1" pin="29.ROW5"/>
<wire x1="175.26" y1="99.06" x2="160.02" y2="99.06" width="0.1524" layer="91"/>
<label x="165.1" y="99.06" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="147.32" y1="177.8" x2="167.64" y2="177.8" width="0.1524" layer="91"/>
<label x="152.4" y="177.8" size="1.778" layer="95"/>
<pinref part="Q5" gate="G$1" pin="D"/>
</segment>
</net>
<net name="ROW6" class="0">
<segment>
<pinref part="U$2" gate="G$1" pin="30.ROW6"/>
<wire x1="160.02" y1="96.52" x2="175.26" y2="96.52" width="0.1524" layer="91"/>
<label x="165.1" y="96.52" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="210.82" y1="177.8" x2="231.14" y2="177.8" width="0.1524" layer="91"/>
<label x="215.9" y="177.8" size="1.778" layer="95"/>
<pinref part="Q6" gate="G$1" pin="D"/>
</segment>
</net>
<net name="ROW7" class="0">
<segment>
<pinref part="U$2" gate="G$1" pin="31.ROW7"/>
<wire x1="175.26" y1="93.98" x2="160.02" y2="93.98" width="0.1524" layer="91"/>
<label x="165.1" y="93.98" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="274.32" y1="177.8" x2="294.64" y2="177.8" width="0.1524" layer="91"/>
<label x="279.4" y="177.8" size="1.778" layer="95"/>
<pinref part="Q7" gate="G$1" pin="D"/>
</segment>
</net>
<net name="ROW8" class="0">
<segment>
<pinref part="U$2" gate="G$1" pin="32.ROW8"/>
<wire x1="160.02" y1="91.44" x2="175.26" y2="91.44" width="0.1524" layer="91"/>
<label x="165.1" y="91.44" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="337.82" y1="177.8" x2="358.14" y2="177.8" width="0.1524" layer="91"/>
<label x="342.9" y="177.8" size="1.778" layer="95"/>
<pinref part="Q8" gate="G$1" pin="D"/>
</segment>
</net>
<net name="ROW1_IN" class="0">
<segment>
<pinref part="SHIFTY" gate="A" pin="QA"/>
<wire x1="66.04" y1="203.2" x2="86.36" y2="203.2" width="0.1524" layer="91"/>
<label x="71.12" y="203.2" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="R2" gate="G$1" pin="1"/>
<wire x1="132.08" y1="205.74" x2="132.08" y2="203.2" width="0.1524" layer="91"/>
<pinref part="Q1" gate="G$1" pin="G"/>
<wire x1="132.08" y1="203.2" x2="142.24" y2="203.2" width="0.1524" layer="91"/>
<wire x1="142.24" y1="203.2" x2="142.24" y2="210.82" width="0.1524" layer="91"/>
<wire x1="132.08" y1="203.2" x2="111.76" y2="203.2" width="0.1524" layer="91"/>
<junction x="132.08" y="203.2"/>
<label x="127" y="203.2" size="1.778" layer="95" rot="MR0"/>
</segment>
</net>
<net name="ROW2_IN" class="0">
<segment>
<pinref part="SHIFTY" gate="A" pin="QB"/>
<wire x1="66.04" y1="200.66" x2="86.36" y2="200.66" width="0.1524" layer="91"/>
<label x="71.12" y="200.66" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="R4" gate="G$1" pin="1"/>
<wire x1="195.58" y1="205.74" x2="195.58" y2="203.2" width="0.1524" layer="91"/>
<wire x1="195.58" y1="203.2" x2="205.74" y2="203.2" width="0.1524" layer="91"/>
<wire x1="205.74" y1="203.2" x2="205.74" y2="210.82" width="0.1524" layer="91"/>
<wire x1="195.58" y1="203.2" x2="175.26" y2="203.2" width="0.1524" layer="91"/>
<junction x="195.58" y="203.2"/>
<label x="190.5" y="203.2" size="1.778" layer="95" rot="MR0"/>
<pinref part="Q2" gate="G$1" pin="G"/>
</segment>
</net>
<net name="ROW3_IN" class="0">
<segment>
<pinref part="SHIFTY" gate="A" pin="QC"/>
<wire x1="86.36" y1="198.12" x2="66.04" y2="198.12" width="0.1524" layer="91"/>
<label x="71.12" y="198.12" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="R6" gate="G$1" pin="1"/>
<wire x1="259.08" y1="205.74" x2="259.08" y2="203.2" width="0.1524" layer="91"/>
<wire x1="259.08" y1="203.2" x2="269.24" y2="203.2" width="0.1524" layer="91"/>
<wire x1="269.24" y1="203.2" x2="269.24" y2="210.82" width="0.1524" layer="91"/>
<wire x1="259.08" y1="203.2" x2="238.76" y2="203.2" width="0.1524" layer="91"/>
<junction x="259.08" y="203.2"/>
<label x="254" y="203.2" size="1.778" layer="95" rot="MR0"/>
<pinref part="Q3" gate="G$1" pin="G"/>
</segment>
</net>
<net name="ROW4_IN" class="0">
<segment>
<pinref part="SHIFTY" gate="A" pin="QD"/>
<wire x1="66.04" y1="195.58" x2="86.36" y2="195.58" width="0.1524" layer="91"/>
<label x="71.12" y="195.58" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="R8" gate="G$1" pin="1"/>
<wire x1="322.58" y1="205.74" x2="322.58" y2="203.2" width="0.1524" layer="91"/>
<wire x1="322.58" y1="203.2" x2="332.74" y2="203.2" width="0.1524" layer="91"/>
<wire x1="332.74" y1="203.2" x2="332.74" y2="210.82" width="0.1524" layer="91"/>
<wire x1="322.58" y1="203.2" x2="302.26" y2="203.2" width="0.1524" layer="91"/>
<junction x="322.58" y="203.2"/>
<label x="317.5" y="203.2" size="1.778" layer="95" rot="MR0"/>
<pinref part="Q4" gate="G$1" pin="G"/>
</segment>
</net>
<net name="ROW5_IN" class="0">
<segment>
<pinref part="SHIFTY" gate="A" pin="QE"/>
<wire x1="86.36" y1="193.04" x2="66.04" y2="193.04" width="0.1524" layer="91"/>
<label x="71.12" y="193.04" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="R3" gate="G$1" pin="1"/>
<wire x1="132.08" y1="165.1" x2="132.08" y2="162.56" width="0.1524" layer="91"/>
<wire x1="132.08" y1="162.56" x2="142.24" y2="162.56" width="0.1524" layer="91"/>
<wire x1="142.24" y1="162.56" x2="142.24" y2="170.18" width="0.1524" layer="91"/>
<wire x1="132.08" y1="162.56" x2="111.76" y2="162.56" width="0.1524" layer="91"/>
<junction x="132.08" y="162.56"/>
<label x="127" y="162.56" size="1.778" layer="95" rot="MR0"/>
<pinref part="Q5" gate="G$1" pin="G"/>
</segment>
</net>
<net name="ROW6_IN" class="0">
<segment>
<pinref part="SHIFTY" gate="A" pin="QF"/>
<wire x1="66.04" y1="190.5" x2="86.36" y2="190.5" width="0.1524" layer="91"/>
<label x="71.12" y="190.5" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="R5" gate="G$1" pin="1"/>
<wire x1="195.58" y1="165.1" x2="195.58" y2="162.56" width="0.1524" layer="91"/>
<wire x1="195.58" y1="162.56" x2="205.74" y2="162.56" width="0.1524" layer="91"/>
<wire x1="205.74" y1="162.56" x2="205.74" y2="170.18" width="0.1524" layer="91"/>
<wire x1="195.58" y1="162.56" x2="175.26" y2="162.56" width="0.1524" layer="91"/>
<junction x="195.58" y="162.56"/>
<label x="190.5" y="162.56" size="1.778" layer="95" rot="MR0"/>
<pinref part="Q6" gate="G$1" pin="G"/>
</segment>
</net>
<net name="ROW7_IN" class="0">
<segment>
<pinref part="SHIFTY" gate="A" pin="QG"/>
<wire x1="86.36" y1="187.96" x2="66.04" y2="187.96" width="0.1524" layer="91"/>
<label x="71.12" y="187.96" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="R7" gate="G$1" pin="1"/>
<wire x1="259.08" y1="165.1" x2="259.08" y2="162.56" width="0.1524" layer="91"/>
<wire x1="259.08" y1="162.56" x2="269.24" y2="162.56" width="0.1524" layer="91"/>
<wire x1="269.24" y1="162.56" x2="269.24" y2="170.18" width="0.1524" layer="91"/>
<wire x1="259.08" y1="162.56" x2="238.76" y2="162.56" width="0.1524" layer="91"/>
<junction x="259.08" y="162.56"/>
<label x="254" y="162.56" size="1.778" layer="95" rot="MR0"/>
<pinref part="Q7" gate="G$1" pin="G"/>
</segment>
</net>
<net name="ROW8_IN" class="0">
<segment>
<pinref part="SHIFTY" gate="A" pin="QH"/>
<wire x1="66.04" y1="185.42" x2="86.36" y2="185.42" width="0.1524" layer="91"/>
<label x="71.12" y="185.42" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="R9" gate="G$1" pin="1"/>
<wire x1="322.58" y1="165.1" x2="322.58" y2="162.56" width="0.1524" layer="91"/>
<wire x1="322.58" y1="162.56" x2="332.74" y2="162.56" width="0.1524" layer="91"/>
<wire x1="332.74" y1="162.56" x2="332.74" y2="170.18" width="0.1524" layer="91"/>
<wire x1="322.58" y1="162.56" x2="302.26" y2="162.56" width="0.1524" layer="91"/>
<junction x="322.58" y="162.56"/>
<label x="317.5" y="162.56" size="1.778" layer="95" rot="MR0"/>
<pinref part="Q8" gate="G$1" pin="G"/>
</segment>
</net>
<net name="ROW_SIN" class="0">
<segment>
<pinref part="SHIFTY" gate="A" pin="SER"/>
<wire x1="40.64" y1="205.74" x2="20.32" y2="205.74" width="0.1524" layer="91"/>
<label x="35.56" y="205.74" size="1.778" layer="95" rot="MR0"/>
</segment>
<segment>
<pinref part="U$1" gate="G$1" pin="P$12"/>
<wire x1="198.12" y1="93.98" x2="226.06" y2="93.98" width="0.1524" layer="91"/>
<label x="203.2" y="93.98" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$4" gate="G$1" pin="P$12"/>
<wire x1="198.12" y1="25.4" x2="218.44" y2="25.4" width="0.1524" layer="91"/>
<label x="203.2" y="25.4" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$5" gate="G$1" pin="P$12"/>
<wire x1="231.14" y1="25.4" x2="251.46" y2="25.4" width="0.1524" layer="91"/>
<label x="236.22" y="25.4" size="1.778" layer="95"/>
</segment>
</net>
<net name="ROW_SCLK" class="0">
<segment>
<pinref part="SHIFTY" gate="A" pin="SCK"/>
<wire x1="40.64" y1="200.66" x2="20.32" y2="200.66" width="0.1524" layer="91"/>
<label x="35.56" y="200.66" size="1.778" layer="95" rot="MR0"/>
</segment>
<segment>
<pinref part="U$1" gate="G$1" pin="P$10"/>
<wire x1="198.12" y1="99.06" x2="226.06" y2="99.06" width="0.1524" layer="91"/>
<label x="203.2" y="99.06" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$3" gate="G$1" pin="P$10"/>
<wire x1="254" y1="99.06" x2="281.94" y2="99.06" width="0.1524" layer="91"/>
<label x="259.08" y="99.06" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$4" gate="G$1" pin="P$10"/>
<wire x1="198.12" y1="30.48" x2="218.44" y2="30.48" width="0.1524" layer="91"/>
<label x="203.2" y="30.48" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$5" gate="G$1" pin="P$10"/>
<wire x1="231.14" y1="30.48" x2="251.46" y2="30.48" width="0.1524" layer="91"/>
<label x="236.22" y="30.48" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$6" gate="G$1" pin="P$10"/>
<wire x1="274.32" y1="30.48" x2="294.64" y2="30.48" width="0.1524" layer="91"/>
<label x="279.4" y="30.48" size="1.778" layer="95"/>
</segment>
</net>
<net name="ROW_LATCH" class="0">
<segment>
<pinref part="SHIFTY" gate="A" pin="RCK"/>
<wire x1="40.64" y1="193.04" x2="20.32" y2="193.04" width="0.1524" layer="91"/>
<label x="35.56" y="193.04" size="1.778" layer="95" rot="MR0"/>
</segment>
<segment>
<pinref part="U$1" gate="G$1" pin="P$11"/>
<wire x1="226.06" y1="96.52" x2="198.12" y2="96.52" width="0.1524" layer="91"/>
<label x="203.2" y="96.52" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$3" gate="G$1" pin="P$11"/>
<wire x1="281.94" y1="96.52" x2="254" y2="96.52" width="0.1524" layer="91"/>
<label x="259.08" y="96.52" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$4" gate="G$1" pin="P$11"/>
<wire x1="218.44" y1="27.94" x2="198.12" y2="27.94" width="0.1524" layer="91"/>
<label x="203.2" y="27.94" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$5" gate="G$1" pin="P$11"/>
<wire x1="251.46" y1="27.94" x2="231.14" y2="27.94" width="0.1524" layer="91"/>
<label x="236.22" y="27.94" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$6" gate="G$1" pin="P$11"/>
<wire x1="294.64" y1="27.94" x2="274.32" y2="27.94" width="0.1524" layer="91"/>
<label x="279.4" y="27.94" size="1.778" layer="95"/>
</segment>
</net>
<net name="ROW_SOUT" class="0">
<segment>
<pinref part="SHIFTY" gate="A" pin="QH*"/>
<wire x1="66.04" y1="182.88" x2="86.36" y2="182.88" width="0.1524" layer="91"/>
<label x="71.12" y="182.88" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="R12" gate="G$1" pin="2"/>
<wire x1="48.26" y1="162.56" x2="45.72" y2="162.56" width="0.1524" layer="91"/>
<label x="30.48" y="162.56" size="1.778" layer="95"/>
<pinref part="R13" gate="G$1" pin="2"/>
<wire x1="45.72" y1="162.56" x2="27.94" y2="162.56" width="0.1524" layer="91"/>
<wire x1="48.26" y1="157.48" x2="45.72" y2="157.48" width="0.1524" layer="91"/>
<wire x1="45.72" y1="157.48" x2="45.72" y2="162.56" width="0.1524" layer="91"/>
<junction x="45.72" y="162.56"/>
</segment>
</net>
<net name="ROW_!CLEAR" class="0">
<segment>
<pinref part="SHIFTY" gate="A" pin="SCL"/>
<wire x1="40.64" y1="198.12" x2="20.32" y2="198.12" width="0.1524" layer="91"/>
<label x="35.56" y="198.12" size="1.778" layer="95" rot="MR0"/>
</segment>
<segment>
<pinref part="U$1" gate="G$1" pin="P$9"/>
<wire x1="226.06" y1="101.6" x2="198.12" y2="101.6" width="0.1524" layer="91"/>
<label x="203.2" y="101.6" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$3" gate="G$1" pin="P$9"/>
<wire x1="281.94" y1="101.6" x2="254" y2="101.6" width="0.1524" layer="91"/>
<label x="259.08" y="101.6" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$4" gate="G$1" pin="P$9"/>
<wire x1="218.44" y1="33.02" x2="198.12" y2="33.02" width="0.1524" layer="91"/>
<label x="203.2" y="33.02" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$5" gate="G$1" pin="P$9"/>
<wire x1="251.46" y1="33.02" x2="231.14" y2="33.02" width="0.1524" layer="91"/>
<label x="236.22" y="33.02" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$6" gate="G$1" pin="P$9"/>
<wire x1="294.64" y1="33.02" x2="274.32" y2="33.02" width="0.1524" layer="91"/>
<label x="279.4" y="33.02" size="1.778" layer="95"/>
</segment>
</net>
<net name="3.3V" class="0">
<segment>
<pinref part="SHIFTY" gate="A" pin="VCC"/>
<pinref part="P+1" gate="G$1" pin="3.3V"/>
<wire x1="66.04" y1="205.74" x2="86.36" y2="205.74" width="0.1524" layer="91"/>
<wire x1="86.36" y1="205.74" x2="86.36" y2="226.06" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U$1" gate="G$1" pin="P$1"/>
<wire x1="198.12" y1="121.92" x2="226.06" y2="121.92" width="0.1524" layer="91"/>
<label x="203.2" y="121.92" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$1" gate="G$1" pin="P$2"/>
<wire x1="198.12" y1="119.38" x2="226.06" y2="119.38" width="0.1524" layer="91"/>
<label x="203.2" y="119.38" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$3" gate="G$1" pin="P$1"/>
<wire x1="254" y1="121.92" x2="281.94" y2="121.92" width="0.1524" layer="91"/>
<label x="259.08" y="121.92" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$3" gate="G$1" pin="P$2"/>
<wire x1="254" y1="119.38" x2="281.94" y2="119.38" width="0.1524" layer="91"/>
<label x="259.08" y="119.38" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="DRIVER" gate="G$1" pin="28.VCC"/>
<wire x1="35.56" y1="106.68" x2="15.24" y2="106.68" width="0.1524" layer="91"/>
<wire x1="15.24" y1="106.68" x2="15.24" y2="121.92" width="0.1524" layer="91"/>
<pinref part="P+2" gate="G$1" pin="3.3V"/>
</segment>
<segment>
<pinref part="C1" gate="G$1" pin="1"/>
<pinref part="P+3" gate="G$1" pin="3.3V"/>
<wire x1="302.26" y1="111.76" x2="302.26" y2="114.3" width="0.1524" layer="91"/>
<pinref part="C2" gate="G$1" pin="1"/>
<wire x1="302.26" y1="114.3" x2="302.26" y2="119.38" width="0.1524" layer="91"/>
<wire x1="302.26" y1="114.3" x2="307.34" y2="114.3" width="0.1524" layer="91"/>
<wire x1="307.34" y1="114.3" x2="307.34" y2="111.76" width="0.1524" layer="91"/>
<junction x="302.26" y="114.3"/>
<pinref part="C3" gate="G$1" pin="1"/>
<wire x1="307.34" y1="114.3" x2="312.42" y2="114.3" width="0.1524" layer="91"/>
<wire x1="312.42" y1="114.3" x2="312.42" y2="111.76" width="0.1524" layer="91"/>
<junction x="307.34" y="114.3"/>
<pinref part="C4" gate="G$1" pin="1"/>
<wire x1="312.42" y1="114.3" x2="317.5" y2="114.3" width="0.1524" layer="91"/>
<wire x1="317.5" y1="114.3" x2="317.5" y2="111.76" width="0.1524" layer="91"/>
<junction x="312.42" y="114.3"/>
<pinref part="C5" gate="G$1" pin="1"/>
<wire x1="317.5" y1="114.3" x2="322.58" y2="114.3" width="0.1524" layer="91"/>
<wire x1="322.58" y1="114.3" x2="322.58" y2="111.76" width="0.1524" layer="91"/>
<junction x="317.5" y="114.3"/>
<pinref part="C6" gate="G$1" pin="1"/>
<wire x1="322.58" y1="114.3" x2="327.66" y2="114.3" width="0.1524" layer="91"/>
<wire x1="327.66" y1="114.3" x2="327.66" y2="111.76" width="0.1524" layer="91"/>
<junction x="322.58" y="114.3"/>
</segment>
<segment>
<pinref part="U$4" gate="G$1" pin="P$1"/>
<wire x1="198.12" y1="53.34" x2="218.44" y2="53.34" width="0.1524" layer="91"/>
<label x="203.2" y="53.34" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$4" gate="G$1" pin="P$2"/>
<wire x1="198.12" y1="50.8" x2="218.44" y2="50.8" width="0.1524" layer="91"/>
<label x="203.2" y="50.8" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$5" gate="G$1" pin="P$1"/>
<wire x1="231.14" y1="53.34" x2="251.46" y2="53.34" width="0.1524" layer="91"/>
<label x="236.22" y="53.34" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$5" gate="G$1" pin="P$2"/>
<wire x1="231.14" y1="50.8" x2="251.46" y2="50.8" width="0.1524" layer="91"/>
<label x="236.22" y="50.8" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$6" gate="G$1" pin="P$1"/>
<wire x1="274.32" y1="53.34" x2="294.64" y2="53.34" width="0.1524" layer="91"/>
<label x="279.4" y="53.34" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$6" gate="G$1" pin="P$2"/>
<wire x1="274.32" y1="50.8" x2="294.64" y2="50.8" width="0.1524" layer="91"/>
<label x="279.4" y="50.8" size="1.778" layer="95"/>
</segment>
</net>
<net name="LED_VCC" class="0">
<segment>
<pinref part="Q1" gate="G$1" pin="S"/>
<wire x1="137.16" y1="218.44" x2="132.08" y2="218.44" width="0.1524" layer="91"/>
<wire x1="132.08" y1="218.44" x2="111.76" y2="218.44" width="0.1524" layer="91"/>
<wire x1="111.76" y1="218.44" x2="111.76" y2="226.06" width="0.1524" layer="91"/>
<label x="111.76" y="226.06" size="1.778" layer="95"/>
<pinref part="R2" gate="G$1" pin="2"/>
<wire x1="132.08" y1="215.9" x2="132.08" y2="218.44" width="0.1524" layer="91"/>
<junction x="132.08" y="218.44"/>
</segment>
<segment>
<wire x1="137.16" y1="177.8" x2="132.08" y2="177.8" width="0.1524" layer="91"/>
<wire x1="132.08" y1="177.8" x2="111.76" y2="177.8" width="0.1524" layer="91"/>
<wire x1="111.76" y1="177.8" x2="111.76" y2="185.42" width="0.1524" layer="91"/>
<label x="111.76" y="185.42" size="1.778" layer="95"/>
<pinref part="R3" gate="G$1" pin="2"/>
<wire x1="132.08" y1="175.26" x2="132.08" y2="177.8" width="0.1524" layer="91"/>
<junction x="132.08" y="177.8"/>
<pinref part="Q5" gate="G$1" pin="S"/>
</segment>
<segment>
<wire x1="200.66" y1="218.44" x2="195.58" y2="218.44" width="0.1524" layer="91"/>
<wire x1="195.58" y1="218.44" x2="175.26" y2="218.44" width="0.1524" layer="91"/>
<wire x1="175.26" y1="218.44" x2="175.26" y2="226.06" width="0.1524" layer="91"/>
<label x="175.26" y="226.06" size="1.778" layer="95"/>
<pinref part="R4" gate="G$1" pin="2"/>
<wire x1="195.58" y1="215.9" x2="195.58" y2="218.44" width="0.1524" layer="91"/>
<junction x="195.58" y="218.44"/>
<pinref part="Q2" gate="G$1" pin="S"/>
</segment>
<segment>
<wire x1="200.66" y1="177.8" x2="195.58" y2="177.8" width="0.1524" layer="91"/>
<wire x1="195.58" y1="177.8" x2="175.26" y2="177.8" width="0.1524" layer="91"/>
<wire x1="175.26" y1="177.8" x2="175.26" y2="185.42" width="0.1524" layer="91"/>
<label x="175.26" y="185.42" size="1.778" layer="95"/>
<pinref part="R5" gate="G$1" pin="2"/>
<wire x1="195.58" y1="175.26" x2="195.58" y2="177.8" width="0.1524" layer="91"/>
<junction x="195.58" y="177.8"/>
<pinref part="Q6" gate="G$1" pin="S"/>
</segment>
<segment>
<wire x1="264.16" y1="218.44" x2="259.08" y2="218.44" width="0.1524" layer="91"/>
<wire x1="259.08" y1="218.44" x2="238.76" y2="218.44" width="0.1524" layer="91"/>
<wire x1="238.76" y1="218.44" x2="238.76" y2="226.06" width="0.1524" layer="91"/>
<label x="238.76" y="226.06" size="1.778" layer="95"/>
<pinref part="R6" gate="G$1" pin="2"/>
<wire x1="259.08" y1="215.9" x2="259.08" y2="218.44" width="0.1524" layer="91"/>
<junction x="259.08" y="218.44"/>
<pinref part="Q3" gate="G$1" pin="S"/>
</segment>
<segment>
<wire x1="264.16" y1="177.8" x2="259.08" y2="177.8" width="0.1524" layer="91"/>
<wire x1="259.08" y1="177.8" x2="238.76" y2="177.8" width="0.1524" layer="91"/>
<wire x1="238.76" y1="177.8" x2="238.76" y2="185.42" width="0.1524" layer="91"/>
<label x="238.76" y="185.42" size="1.778" layer="95"/>
<pinref part="R7" gate="G$1" pin="2"/>
<wire x1="259.08" y1="175.26" x2="259.08" y2="177.8" width="0.1524" layer="91"/>
<junction x="259.08" y="177.8"/>
<pinref part="Q7" gate="G$1" pin="S"/>
</segment>
<segment>
<wire x1="327.66" y1="218.44" x2="322.58" y2="218.44" width="0.1524" layer="91"/>
<wire x1="322.58" y1="218.44" x2="302.26" y2="218.44" width="0.1524" layer="91"/>
<wire x1="302.26" y1="218.44" x2="302.26" y2="226.06" width="0.1524" layer="91"/>
<label x="302.26" y="226.06" size="1.778" layer="95"/>
<pinref part="R8" gate="G$1" pin="2"/>
<wire x1="322.58" y1="215.9" x2="322.58" y2="218.44" width="0.1524" layer="91"/>
<junction x="322.58" y="218.44"/>
<pinref part="Q4" gate="G$1" pin="S"/>
</segment>
<segment>
<wire x1="327.66" y1="177.8" x2="322.58" y2="177.8" width="0.1524" layer="91"/>
<wire x1="322.58" y1="177.8" x2="302.26" y2="177.8" width="0.1524" layer="91"/>
<wire x1="302.26" y1="177.8" x2="302.26" y2="185.42" width="0.1524" layer="91"/>
<label x="302.26" y="185.42" size="1.778" layer="95"/>
<pinref part="R9" gate="G$1" pin="2"/>
<wire x1="322.58" y1="175.26" x2="322.58" y2="177.8" width="0.1524" layer="91"/>
<junction x="322.58" y="177.8"/>
<pinref part="Q8" gate="G$1" pin="S"/>
</segment>
<segment>
<pinref part="U$1" gate="G$1" pin="P$13"/>
<wire x1="198.12" y1="91.44" x2="226.06" y2="91.44" width="0.1524" layer="91"/>
<label x="203.2" y="91.44" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$1" gate="G$1" pin="P$14"/>
<wire x1="226.06" y1="88.9" x2="198.12" y2="88.9" width="0.1524" layer="91"/>
<label x="203.2" y="88.9" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$3" gate="G$1" pin="P$13"/>
<wire x1="254" y1="91.44" x2="281.94" y2="91.44" width="0.1524" layer="91"/>
<label x="259.08" y="91.44" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$3" gate="G$1" pin="P$14"/>
<wire x1="281.94" y1="88.9" x2="254" y2="88.9" width="0.1524" layer="91"/>
<label x="259.08" y="88.9" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$4" gate="G$1" pin="P$13"/>
<wire x1="198.12" y1="22.86" x2="218.44" y2="22.86" width="0.1524" layer="91"/>
<label x="203.2" y="22.86" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$4" gate="G$1" pin="P$14"/>
<wire x1="218.44" y1="20.32" x2="198.12" y2="20.32" width="0.1524" layer="91"/>
<label x="203.2" y="20.32" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$5" gate="G$1" pin="P$13"/>
<wire x1="231.14" y1="22.86" x2="251.46" y2="22.86" width="0.1524" layer="91"/>
<label x="236.22" y="22.86" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$5" gate="G$1" pin="P$14"/>
<wire x1="251.46" y1="20.32" x2="231.14" y2="20.32" width="0.1524" layer="91"/>
<label x="236.22" y="20.32" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$6" gate="G$1" pin="P$13"/>
<wire x1="274.32" y1="22.86" x2="294.64" y2="22.86" width="0.1524" layer="91"/>
<label x="279.4" y="22.86" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$6" gate="G$1" pin="P$14"/>
<wire x1="294.64" y1="20.32" x2="274.32" y2="20.32" width="0.1524" layer="91"/>
<label x="279.4" y="20.32" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="C7" gate="G$1" pin="1"/>
<wire x1="337.82" y1="111.76" x2="337.82" y2="114.3" width="0.1524" layer="91"/>
<label x="337.82" y="119.38" size="1.778" layer="95"/>
<pinref part="C8" gate="G$1" pin="1"/>
<wire x1="337.82" y1="114.3" x2="337.82" y2="119.38" width="0.1524" layer="91"/>
<wire x1="337.82" y1="114.3" x2="342.9" y2="114.3" width="0.1524" layer="91"/>
<wire x1="342.9" y1="114.3" x2="342.9" y2="111.76" width="0.1524" layer="91"/>
<junction x="337.82" y="114.3"/>
<pinref part="C9" gate="G$1" pin="1"/>
<wire x1="342.9" y1="114.3" x2="347.98" y2="114.3" width="0.1524" layer="91"/>
<wire x1="347.98" y1="114.3" x2="347.98" y2="111.76" width="0.1524" layer="91"/>
<junction x="342.9" y="114.3"/>
<pinref part="C10" gate="G$1" pin="1"/>
<wire x1="347.98" y1="114.3" x2="353.06" y2="114.3" width="0.1524" layer="91"/>
<wire x1="353.06" y1="114.3" x2="353.06" y2="111.76" width="0.1524" layer="91"/>
<junction x="347.98" y="114.3"/>
</segment>
</net>
<net name="TOP_SIN" class="0">
<segment>
<pinref part="U$6" gate="G$1" pin="P$5"/>
<wire x1="274.32" y1="43.18" x2="294.64" y2="43.18" width="0.1524" layer="91"/>
<label x="279.4" y="43.18" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="R10" gate="G$1" pin="1"/>
<wire x1="55.88" y1="22.86" x2="81.28" y2="22.86" width="0.1524" layer="91"/>
<label x="60.96" y="22.86" size="1.778" layer="95"/>
</segment>
</net>
<net name="TOP_ROW_SIN" class="0">
<segment>
<pinref part="U$6" gate="G$1" pin="P$12"/>
<wire x1="274.32" y1="25.4" x2="294.64" y2="25.4" width="0.1524" layer="91"/>
<label x="279.4" y="25.4" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="R12" gate="G$1" pin="1"/>
<wire x1="58.42" y1="162.56" x2="86.36" y2="162.56" width="0.1524" layer="91"/>
<label x="63.5" y="162.56" size="1.778" layer="95"/>
</segment>
</net>
<net name="SIDE_SOUT" class="0">
<segment>
<pinref part="U$3" gate="G$1" pin="P$5"/>
<wire x1="254" y1="111.76" x2="281.94" y2="111.76" width="0.1524" layer="91"/>
<label x="259.08" y="111.76" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="R11" gate="G$1" pin="1"/>
<wire x1="55.88" y1="17.78" x2="81.28" y2="17.78" width="0.1524" layer="91"/>
<label x="60.96" y="17.78" size="1.778" layer="95"/>
</segment>
</net>
<net name="SIDE_ROW_SOUT" class="0">
<segment>
<pinref part="U$3" gate="G$1" pin="P$12"/>
<wire x1="254" y1="93.98" x2="281.94" y2="93.98" width="0.1524" layer="91"/>
<label x="259.08" y="93.98" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="R13" gate="G$1" pin="1"/>
<wire x1="58.42" y1="157.48" x2="86.36" y2="157.48" width="0.1524" layer="91"/>
<label x="63.5" y="157.48" size="1.778" layer="95"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
</eagle>

